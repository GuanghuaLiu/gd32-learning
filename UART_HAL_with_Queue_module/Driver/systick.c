#include "gd32f30x.h"
//#include "systick.h"

/**
 * @brief Systick初始化，产生中断时间为1ms
 * @param  
 */
void SystickInit(void)
{
    if (SysTick_Config(rcu_clock_freq_get(CK_AHB) / 1000))
    {
        while (1);
    }
    
}

static volatile uint64_t g_systemRunTime = 0;

/**
 * @brief systick中断处理函数，中断周期为1ms
 * @param  
 */
void SysTick_Handler(void)
{
    g_systemRunTime++;
}

/**
 * @brief 获取系统运行时间
 * @param  
 * @return 系统运行时间， 单位为1ms
 */
uint64_t GetSystemRunTime(void)
{
    return g_systemRunTime;
}
