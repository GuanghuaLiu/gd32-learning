#ifndef _CANBUS_DRV_H_
#define _CANBUS_DRV_H_

#include <stdbool.h>
#include <stdint.h>

void CanbusDrvInit(void);

/**
 * @brief canbus发送一帧数据
 * @param canbusId canbus设备id
 * @param dataLength 数据包长度
 * @param transmitBuffer 数据包地址
 * @return 
 */
bool CanbusSendMessage(uint32_t canbusId, uint8_t *transmitBuffer, uint8_t dataLength);

void CanbusDrvTest(void);
#endif
