// Includes for the Canfestival driver
#include "canfestival.h"
#include "timer.h"
#include "gd32f30x.h"

// Initializes the timer, turn on the interrupt and put the interrupt time to zero
void CanopenTimerDrvInit(void)
{
    rcu_periph_clock_enable(RCU_TIMER3);
    timer_deinit(TIMER3);
	
    timer_parameter_struct paramInitStruct;
    timer_struct_para_init(&paramInitStruct);
	
    paramInitStruct.alignedmode = TIMER_COUNTER_EDGE;
    paramInitStruct.prescaler = 1200 - 1;	// ���ֳ�Ա.clockdivision
    paramInitStruct.counterdirection = TIMER_COUNTER_UP;
    paramInitStruct.period = TIMEVAL_MAX;
    timer_init(TIMER3, &paramInitStruct);

    timer_interrupt_enable(TIMER3, TIMER_INT_UP);
    nvic_irq_enable(TIMER3_IRQn, 0, 0);
    timer_enable(TIMER3);
}

static TIMEVAL g_lastCounterVal = 0;
static TIMEVAL g_elapsedTime = 0;

// Set the timer for the next alarm.
void setTimer(TIMEVAL value)
{
	uint32_t timerCnt = timer_counter_read(TIMER3);
	
	g_elapsedTime += timerCnt - g_lastCounterVal;
	g_lastCounterVal = TIMEVAL_MAX - value;
	timer_counter_value_config(TIMER3, TIMEVAL_MAX - value);
}

// Return the elapsed time to tell the Stack how much time is spent since last call.
TIMEVAL getElapsedTime(void)
{
	uint32_t timer = timer_counter_read(TIMER3);
	if (timer < g_lastCounterVal)
	{
		timer += TIMEVAL_MAX;
	}
	TIMEVAL elapsed = timer - g_lastCounterVal + g_elapsedTime;
    return elapsed;
}

// This function handles Timer 3 interrupt request.
void TIMER3_IRQHandler(void)
{
    if (RESET != timer_interrupt_flag_get(TIMER3, TIMER_INT_FLAG_UP))
    {
        timer_interrupt_flag_clear(TIMER3, TIMER_INT_FLAG_UP);
		g_lastCounterVal = 0;
		g_elapsedTime = 0;
        TimeDispatch();
    }
}
