// Includes for the Canfestival driver
#include "canfestival.h"
#include "data.h"
#include "gd32f30x.h"

static CO_Data *g_coData = NULL;

static void GpioConfig(void)
{
    rcu_periph_clock_enable(RCU_GPIOA);
    gpio_init(GPIOA, GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_PIN_11);
    gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12);
}

static void ParamConfig(void)
{
    rcu_periph_clock_enable(RCU_CAN0);
	
    /* initialize CAN register */
    can_deinit(CAN0);

    /* initialize CAN */
    can_parameter_struct paramInitStruct;
    can_struct_para_init(CAN_INIT_STRUCT, &paramInitStruct);	
	
    paramInitStruct.working_mode = CAN_NORMAL_MODE;
    paramInitStruct.resync_jump_width = CAN_BT_SJW_1TQ;
    paramInitStruct.time_segment_1 = CAN_BT_BS1_7TQ;
    paramInitStruct.time_segment_2 = CAN_BT_BS2_2TQ;
    paramInitStruct.time_triggered = DISABLE;
    paramInitStruct.auto_bus_off_recovery = ENABLE;
    paramInitStruct.auto_wake_up = DISABLE;
    paramInitStruct.auto_retrans = ENABLE;
    paramInitStruct.rec_fifo_overwrite = DISABLE;
    paramInitStruct.trans_fifo_order = DISABLE;
    /* baudrate 1Mbps */
    paramInitStruct.prescaler = 6;
    can_init(CAN0, &paramInitStruct);

    /* initialize filter */
	can_filter_parameter_struct filterInitStruct;
    can_struct_para_init(CAN_FILTER_STRUCT, &filterInitStruct);

    filterInitStruct.filter_list_high = 0;
    filterInitStruct.filter_list_low = 0;
    filterInitStruct.filter_mask_high = 0;
    filterInitStruct.filter_mask_low = 0;
    filterInitStruct.filter_fifo_number = CAN_FIFO0;
    filterInitStruct.filter_number = 0;
    filterInitStruct.filter_mode = CAN_FILTERMODE_MASK;
    filterInitStruct.filter_bits = CAN_FILTERBITS_16BIT;
    filterInitStruct.filter_enable = ENABLE;

    can_filter_init(&filterInitStruct);

    /* enable CAN receive FIFO0 not empty interrupt */
    can_interrupt_enable(CAN0, CAN_INT_RFNE0);
    /* configure the nested vectored interrupt controller */
    nvic_irq_enable(USBD_LP_CAN0_RX0_IRQn, 0, 0);
}

static void CanbusDrvInit(void)
{
    GpioConfig();
    ParamConfig();
}

// Initialize the CAN hardware
void CanopenDrvInit(CO_Data *d)
{
    g_coData = d;
    CanbusDrvInit();
}

// The driver send a CAN message passed from the CANopen stack
unsigned char canSend(CAN_PORT notused, Message *m)
{
    can_trasnmit_message_struct transmitMessage;
    can_struct_para_init(CAN_TX_MESSAGE_STRUCT, &transmitMessage);
    transmitMessage.tx_sfid = m->cob_id;
    transmitMessage.tx_ff = CAN_FF_STANDARD;
    // transmitMessage.tx_ft = m->rtr << 1;
    if (m->rtr)
    {
        transmitMessage.tx_ft = CAN_FT_REMOTE;
    }
    else
    {
        transmitMessage.tx_ft = CAN_FT_DATA;
    }

    transmitMessage.tx_dlen = m->len;

    for (uint8_t i = 0; i < m->len; i++)
    {
        transmitMessage.tx_data[i] = m->data[i];
    }

    uint8_t txMail = 0;
    uint16_t timeout = 0xffff;
    txMail = can_message_transmit(CAN0, &transmitMessage);

    while ((can_transmit_states(CAN0, txMail) != CAN_TRANSMIT_OK) && (timeout != 0))
    {
        timeout--;
    }

    if (0 == timeout)
    {
        return 1;
    }
    return 0;
}

/**
 * @brief  This function handles CAN0 RX0 interrupt request.
 * @param  None
 * @retval None
 */
void USBD_LP_CAN0_RX0_IRQHandler(void)
{
    can_receive_message_struct recMessage;
    can_struct_para_init(CAN_RX_MESSAGE_STRUCT, &recMessage);

    can_message_receive(CAN0, CAN_FIFO0, &recMessage);

    if (recMessage.rx_ff == CAN_FF_EXTENDED)
    {
        return;
    }

    Message rxMessage = {0};

    rxMessage.cob_id = recMessage.rx_sfid;
    // rxMessage.rtr = recMessage.rx_ft >> 1;
    switch (recMessage.rx_ft)
    {
    case CAN_FT_DATA:
        rxMessage.rtr = 0;
        break;
    case CAN_FT_REMOTE:
        rxMessage.rtr = 1;
        break;
    default:
        break;
    }
    rxMessage.len = recMessage.rx_dlen;
    for (uint8_t i = 0; i < recMessage.rx_dlen; i++)
    {
        rxMessage.data[i] = recMessage.rx_data[i];
    }
    canDispatch(g_coData, &rxMessage);
}
