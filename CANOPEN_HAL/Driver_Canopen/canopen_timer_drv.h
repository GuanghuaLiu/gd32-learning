#ifndef _CANOPEN_TIMER_DRV_H_
#define _CANOPEN_TIMER_DRV_H_

// Initializes the timer, turn on the interrupt and put the interrupt time to zero
void CanopenTimerDrvInit(void);

#endif
