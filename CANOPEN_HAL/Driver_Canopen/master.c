
/* File generated by gen_cfile.py. Should not be modified. */

#include "master.h"

/**************************************************************************/
/* Declaration of mapped variables                                        */
/**************************************************************************/

/**************************************************************************/
/* Declaration of value range types                                       */
/**************************************************************************/

#define valueRange_EMC 0x9F /* Type for index 0x1003 subindex 0x00 (only set of value 0 is possible) */
UNS32 master_valueRangeTest (UNS8 typeValue, void * value)
{
  switch (typeValue) {
    case valueRange_EMC:
      if (*(UNS8*)value != (UNS8)0) return OD_VALUE_RANGE_EXCEEDED;
      break;
  }
  return 0;
}

/**************************************************************************/
/* The node id                                                            */
/**************************************************************************/
/* node_id default value.*/
UNS8 master_bDeviceNodeId = 0x00;

/**************************************************************************/
/* Array of message processing information */

const UNS8 master_iam_a_slave = 0;

TIMER_HANDLE master_heartBeatTimers[1] = {TIMER_NONE};

/*
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$

                               OBJECT DICTIONARY

$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
*/

/* index 0x1000 :   Device Type. */
                    UNS32 master_obj1000 = 0x0;	/* 0 */
                    subindex master_Index1000[] = 
                     {
                       { RO, uint32, sizeof (UNS32), (void*)&master_obj1000, NULL }
                     };

/* index 0x1001 :   Error Register. */
                    UNS8 master_obj1001 = 0x0;	/* 0 */
                    subindex master_Index1001[] = 
                     {
                       { RO, uint8, sizeof (UNS8), (void*)&master_obj1001, NULL }
                     };

/* index 0x1003 :   Pre-defined Error Field */
                    UNS8 master_highestSubIndex_obj1003 = 0; /* number of subindex - 1*/
                    UNS32 master_obj1003[] = 
                    {
                      0x0	/* 0 */
                    };
                    subindex master_Index1003[] = 
                     {
                       { RW, valueRange_EMC, sizeof (UNS8), (void*)&master_highestSubIndex_obj1003, NULL },
                       { RO, uint32, sizeof (UNS32), (void*)&master_obj1003[0], NULL }
                     };

/* index 0x1005 :   SYNC COB ID */
                    UNS32 master_obj1005 = 0x0;   /* 0 */

/* index 0x1006 :   Communication / Cycle Period */
                    UNS32 master_obj1006 = 0x0;   /* 0 */

/* index 0x100C :   Guard Time */ 
                    UNS16 master_obj100C = 0x0;   /* 0 */

/* index 0x100D :   Life Time Factor */ 
                    UNS8 master_obj100D = 0x0;   /* 0 */

/* index 0x1014 :   Emergency COB ID */
                    UNS32 master_obj1014 = 0x80 + 0x00;   /* 128 + NodeID */

/* index 0x1016 :   Consumer Heartbeat Time. */
                    UNS8 master_highestSubIndex_obj1016 = 1; /* number of subindex - 1*/
                    UNS32 master_obj1016[] = 
                    {
						0
						// 0x303E8	/* 197608 */
                    };
//                    subindex master_Index1016[] = 
//                     {
//                       { RO, uint8, sizeof (UNS8), (void*)&master_highestSubIndex_obj1016, NULL },
//                       { RW, uint32, sizeof (UNS32), (void*)&master_obj1016[0], NULL }
//                     };

/* index 0x1017 :   Producer Heartbeat Time */ 
                    UNS16 master_obj1017 = 0x0;   /* 0 */

/* index 0x1018 :   Identity. */
                    UNS8 master_highestSubIndex_obj1018 = 4; /* number of subindex - 1*/
                    UNS32 master_obj1018_Vendor_ID = 0x0;	/* 0 */
                    UNS32 master_obj1018_Product_Code = 0x0;	/* 0 */
                    UNS32 master_obj1018_Revision_Number = 0x0;	/* 0 */
                    UNS32 master_obj1018_Serial_Number = 0x0;	/* 0 */
                    subindex master_Index1018[] = 
                     {
                       { RO, uint8, sizeof (UNS8), (void*)&master_highestSubIndex_obj1018, NULL },
                       { RO, uint32, sizeof (UNS32), (void*)&master_obj1018_Vendor_ID, NULL },
                       { RO, uint32, sizeof (UNS32), (void*)&master_obj1018_Product_Code, NULL },
                       { RO, uint32, sizeof (UNS32), (void*)&master_obj1018_Revision_Number, NULL },
                       { RO, uint32, sizeof (UNS32), (void*)&master_obj1018_Serial_Number, NULL }
                     };

/* index 0x1280 :   Client SDO 1 Parameter. */
                    UNS8 master_highestSubIndex_obj1280 = 3; /* number of subindex - 1*/
                    UNS32 master_obj1280_COB_ID_Client_to_Server_Transmit_SDO = 0x603;	/* 1539 */
                    UNS32 master_obj1280_COB_ID_Server_to_Client_Receive_SDO = 0x583;	/* 1411 */
                    UNS8 master_obj1280_Node_ID_of_the_SDO_Server = 0x3;	/* 3 */
                    subindex master_Index1280[] = 
                     {
                       { RO, uint8, sizeof (UNS8), (void*)&master_highestSubIndex_obj1280, NULL },
                       { RW, uint32, sizeof (UNS32), (void*)&master_obj1280_COB_ID_Client_to_Server_Transmit_SDO, NULL },
                       { RW, uint32, sizeof (UNS32), (void*)&master_obj1280_COB_ID_Server_to_Client_Receive_SDO, NULL },
                       { RW, uint8, sizeof (UNS8), (void*)&master_obj1280_Node_ID_of_the_SDO_Server, NULL }
                     };

/**************************************************************************/
/* Declaration of pointed variables                                       */
/**************************************************************************/

const indextable master_objdict[] = 
{
  { (subindex*)master_Index1000,sizeof(master_Index1000)/sizeof(master_Index1000[0]), 0x1000},
  { (subindex*)master_Index1001,sizeof(master_Index1001)/sizeof(master_Index1001[0]), 0x1001},
  { (subindex*)master_Index1016,sizeof(master_Index1016)/sizeof(master_Index1016[0]), 0x1016},
  { (subindex*)master_Index1018,sizeof(master_Index1018)/sizeof(master_Index1018[0]), 0x1018},
  { (subindex*)master_Index1280,sizeof(master_Index1280)/sizeof(master_Index1280[0]), 0x1280},
};

const indextable * master_scanIndexOD (CO_Data *d, UNS16 wIndex, UNS32 * errorCode)
{
	(void)d;
	int i;
	switch(wIndex){
		case 0x1000: i = 0;break;
		case 0x1001: i = 1;break;
		case 0x1016: i = 2;break;
		case 0x1018: i = 3;break;
		case 0x1280: i = 4;break;
		default:
			*errorCode = OD_NO_SUCH_OBJECT;
			return NULL;
	}
	*errorCode = OD_SUCCESSFUL;
	return &master_objdict[i];
}

/* 
 * To count at which received SYNC a PDO must be sent.
 * Even if no pdoTransmit are defined, at least one entry is computed
 * for compilations issues.
 */
s_PDO_status master_PDO_status[1] = {s_PDO_status_Initializer};

const quick_index master_firstIndex = {
  0, /* SDO_SVR */
  4, /* SDO_CLT */
  0, /* PDO_RCV */
  0, /* PDO_RCV_MAP */
  0, /* PDO_TRS */
  0 /* PDO_TRS_MAP */
};

const quick_index master_lastIndex = {
  0, /* SDO_SVR */
  4, /* SDO_CLT */
  0, /* PDO_RCV */
  0, /* PDO_RCV_MAP */
  0, /* PDO_TRS */
  0 /* PDO_TRS_MAP */
};

const UNS16 master_ObjdictSize = sizeof(master_objdict)/sizeof(master_objdict[0]); 

CO_Data master_Data = CANOPEN_NODE_DATA_INITIALIZER(master);

