#include <stdint.h>
#include <stdio.h>
#include "systick.h"
#include "delay.h"
#include "usb2com_drv.h"
#include "canopen_app.h"
#include "key_drv.h"
#include "led_drv.h"
#include "sensor_drv.h"
#include "sensor_app.h"
typedef struct
{
	uint8_t run;
	uint16_t timCount;
	uint16_t timRload;
	void (*pTaskFuncCb)(void);
}TaskCompsInfo_t;

static TaskCompsInfo_t g_taskCompsInfo[] = {
	{0, 100, 100, SensorAppTask},
	{0, 10, 10, CanopenTask},
};

#define MAX_TASK_NUM	sizeof(g_taskCompsInfo)/sizeof(g_taskCompsInfo[0])
	
static void TaskHandler(void)
{
	for (uint8_t i = 0; i < MAX_TASK_NUM; i++)
	{
		if (g_taskCompsInfo[i].run)
		{
			g_taskCompsInfo[i].pTaskFuncCb();
			g_taskCompsInfo[i].run = 0;
		}
	}
}

static void TaskSchedulerCb(void)
{
	for (uint8_t i = 0; i < MAX_TASK_NUM; i++)
	{
		if (g_taskCompsInfo[i].timCount)
		{
			g_taskCompsInfo[i].timCount--;
			if (!g_taskCompsInfo[i].timCount)
			{
				g_taskCompsInfo[i].run = 1;
				g_taskCompsInfo[i].timCount = g_taskCompsInfo[i].timRload;
			}
		}
	}
}

static void DriverInit(void)
{
	DwtDelayInit();
	SystickInit();
	Usb2ComDrvInit();
	LedDrvInit();
	KeyDrvInit();
	SensorDrvInit();
}

static void AppInit(void)
{
	CanopenAppInit();
	TaskScheduleCbReg(TaskSchedulerCb);
}

int main(void)
{
	DriverInit();
	AppInit();

	while (1)
	{
		TaskHandler();
	}
}
