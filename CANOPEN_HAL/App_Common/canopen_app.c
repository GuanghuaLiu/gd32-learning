#include <stdint.h>
#include <stdio.h>
#include "canopen_can_drv.h"
#include "canopen_timer_drv.h"
#include "slaver.h"

#include "key_drv.h"
#include "led_drv.h"

#include "sensor_drv.h"

void CanopenAppInit(void)
{
    CanopenTimerDrvInit();
    CanopenDrvInit(&slaver_Data);

    volatile uint8_t nodeId = 0x05;
    setNodeId(&slaver_Data, nodeId);
    setState(&slaver_Data, Initialisation);
    setState(&slaver_Data, Operational);
}


void CanopenTask(void)
{
	
	SensorData_t sensorData;
	GetSensorData(&sensorData);
	printf("the temperature is %.1f celdegrees, and the humidity is %d %%.\r\n", sensorData.temperature, sensorData.humidity);
	canopenTemp = sensorData.temperature * 10;
	canopenHumi = sensorData.humidity;
#if 0
	uint8_t keyVal = GetKeyValue();
	
	uint8_t nodeId;
	uint16_t index; 
	uint16_t timeout;
	uint16_t data;
	uint32_t abortCode;
	uint32_t size;
	switch (keyVal)
	{
	case KEY1_SHORT_PRESS:
		TurnOnLed(LED1);
	
		nodeId = 0x03;
		index = 0x1017; 
		timeout = 0xffff;
	
		readNetworkDict(&master_Data, nodeId, index, 0, 2, 0);
		
		while (SDO_UPLOAD_IN_PROGRESS == getReadResultNetworkDict(&master_Data, nodeId, &data, &size, &abortCode) && (timeout != 0))
		{
			timeout--;
		}
		if (timeout == 0)
		{
			printf("read %#x error.\r\n", index);
			return;
		}
		printf("read %#x, data:%d, size:%d, abortCode:%d.\r\n",index, data, size, abortCode);
		break;
	case KEY1_LONG_PRESS:
		TurnOffLed(LED1);
		break;
	case KEY2_SHORT_PRESS:
		TurnOnLed(LED2);
	
		nodeId = 0x03;
		index = 0x1017; 
		timeout = 0xffff;
		data = 2000;
		size = sizeof(data);
	
		writeNetworkDict(&master_Data, nodeId, index, 0, 2, 2, &data, 0);
	
		while (SDO_DOWNLOAD_IN_PROGRESS == getWriteResultNetworkDict(&master_Data, nodeId, &abortCode) && (timeout != 0))
		{
			timeout--;
		}
		if (timeout == 0)
		{
			printf("write %#x error.\r\n", index);
			return;
		}
		printf("write %#x, data:%d, size:%d, abortCode:%d.\r\n",index, data, size, abortCode);
		break;
	case KEY2_LONG_PRESS:
		TurnOffLed(LED2);
		break;
	case KEY3_SHORT_PRESS:		
		TurnOnLed(LED3);
//		nodeId = 0x03;
//		masterSendNMTstateChange(&master_Data, nodeId, NMT_Reset_Comunication);
		break;
	case KEY3_LONG_PRESS:
		TurnOffLed(LED3);
		break;
	case KEY4_SHORT_PRESS:
		TurnOnLed(LED1);
		TurnOnLed(LED2);
		TurnOnLed(LED3);
		nodeId = 0x03;
		masterSendNMTstateChange(&master_Data, nodeId, NMT_Start_Node);
		break;
	case KEY4_LONG_PRESS:
		TurnOffLed(LED1);
		TurnOffLed(LED2);
		TurnOffLed(LED3);
		break;
	default:
		break;
	}
#endif
}