#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "rtc_drv.h"
#include "sensor_drv.h"
#include "key_drv.h"
#include "led_drv.h"
#include "fatfs_app.h"
/**
 * @brief 人机交互功能模块任务函数
 * @param
 */
void HmiTask(void)
{
	RtcTime_t time = {0};
	SensorData_t sensorData;
	uint8_t keyVal = GetKeyValue();

	switch (keyVal)
	{
	case KEY1_SHORT_PRESS:
		TurnOnLed(LED1);
		GetRtcTime(&time);
		printf("%d-%02d-%02d %02d:%02d:%02d\n", time.year, time.month, time.day,
			   time.hour, time.minute, time.second);
		break;
	case KEY1_LONG_PRESS:
		TurnOffLed(LED1);
		break;
	case KEY2_SHORT_PRESS:
		TurnOnLed(LED2);
		GetSensorData(&sensorData);
		printf("temperature = %.1f ℃, humidity = %d%%.\n", sensorData.temperature, sensorData.humidity);
		break;
	case KEY2_LONG_PRESS:
		TurnOffLed(LED2);
		break;
	case KEY3_SHORT_PRESS:
		TurnOnLed(LED3);
		break;
	case KEY3_LONG_PRESS:
		TurnOffLed(LED3);
		break;
	case KEY4_SHORT_PRESS:
		TurnOnLed(LED1);
		TurnOnLed(LED2);
		TurnOnLed(LED3);
		PrintFileData();
		break;
	case KEY4_LONG_PRESS:
		TurnOffLed(LED1);
		TurnOffLed(LED2);
		TurnOffLed(LED3);
		break;
	default:
		break;
	}
}
