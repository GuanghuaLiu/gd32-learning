#include <stdint.h>
#include "gd32f30x.h"
#include "delay.h"
#include "led_drv.h"

int main(void)
{
    // ��ʼ��DWT��ʱ
    DwtDelayInit();

    LedDrvInit();

    while (1)
    {
        TurnOffLed(LED2);
        TurnOnLed(LED1);
        DelayNms(1000);
        TurnOffLed(LED1);
        TurnOnLed(LED2);
        DelayNms(1000);
        TurnOffLed(LED2);
        TurnOnLed(LED3);
        DelayNms(1000);       
		TurnOffLed(LED3);
		TurnOnLed(LED2);
		DelayNms(1000);
    }
}
