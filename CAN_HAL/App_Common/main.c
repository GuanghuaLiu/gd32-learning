#include <stdint.h>
#include <stdio.h>
#include "systick.h"
#include "delay.h"
#include "usb2com_drv.h"
#include "canbus_drv.h"

static void DriverInit(void)
{
	DwtDelayInit();
	SystickInit();
	Usb2ComDrvInit();
	CanbusDrvInit();

}

//static void AppInit(void)
//{
//}

int main(void)
{
	DriverInit();
//	AppInit();

	while (1)
	{
		CanbusDrvTest();
	}
}
