#include <stdio.h>
#include "gd32f30x.h"

typedef struct
{
    uint32_t usart;
    rcu_periph_enum rcuUsart;
    uint32_t gpio;
    rcu_periph_enum rcuGpio;
    uint32_t txPin;
    uint32_t rxPin;
    uint8_t irq;
} Usb2ComHwInfo_t;

Usb2ComHwInfo_t g_Usb2ComInfo = {USART0, RCU_USART0, GPIOA, RCU_GPIOA, GPIO_PIN_9, GPIO_PIN_10, USART0_IRQn};

/**
 * @brief USB转串口GPIO口初始化
 * @param
 */
static void Usb2ComGpioInit(void)
{
    /* 使能GPIO时钟 */
    rcu_periph_clock_enable(g_Usb2ComInfo.rcuGpio);
    /* 配置TX对应管脚为推挽复用输出模式 */
    gpio_init(g_Usb2ComInfo.gpio, GPIO_MODE_AF_PP, GPIO_OSPEED_10MHZ, g_Usb2ComInfo.txPin);
    /* 配置RX对应管脚为浮空输入/上拉输入模式 */
    gpio_init(g_Usb2ComInfo.gpio, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, g_Usb2ComInfo.rxPin);
}

/**
 * @brief USB转串口UART初始化
 * @param baudRate 波特率
 */
static void Usb2ComUartInit(uint32_t baudRate)
{
    /* 使能UART时钟; */
    rcu_periph_clock_enable(g_Usb2ComInfo.rcuUsart);
    /* 复位UART; */
    usart_deinit(g_Usb2ComInfo.usart);
    /* 通过USART_CTLO寄存器的WL设置字长; */
    usart_word_length_set(g_Usb2ComInfo.usart, USART_WL_8BIT);
    /* 通过USART_CTLO寄存器的PCEN设置校验位; */
    usart_parity_config(g_Usb2ComInfo.usart, USART_PM_NONE);
    /* 在USART_CTL1寄存器中写STB[1:0]位来设置停止位的长度; */
    usart_stop_bit_set(g_Usb2ComInfo.usart, USART_STB_1BIT);
    /* 在USART_BAUD寄存器中设置波特率; */
    usart_baudrate_set(g_Usb2ComInfo.usart, baudRate);
    /* 在USART_CTLO寄存器中设置TEN位,使能发送功能; */
    usart_transmit_config(g_Usb2ComInfo.usart, USART_TRANSMIT_ENABLE);
    /* 在USART_CTLO寄存器中设置REN位,使能接收功能; */
    usart_receive_config(g_Usb2ComInfo.usart, USART_RECEIVE_ENABLE);
    /* 使能UART接收中断功能; */
    usart_interrupt_enable(g_Usb2ComInfo.usart, USART_INT_RBNE);
    /* 使能UART中断功能; */
    nvic_irq_enable(g_Usb2ComInfo.irq, 0, 0);
    /* 在USART_CTLO寄存器中置位UEN位,使能UART; */
    usart_enable(g_Usb2ComInfo.usart);
}

/**
 * @brief USB转串口硬件初始化
 * @param
 */
void Usb2ComDrvInit(void)
{
    Usb2ComGpioInit();
    Usb2ComUartInit(115200);
}

/**
 * @brief printf函数重定向至串口发送(printf函数默认打印输出到显示器，如果要输出到串口，
 *        必须重新实现fputc函数，将输出指向串口，称为重定向)
 * @param ch ASCII码
 * @param stream 文件输出流
 * @return 无符号字符
 */
int fputc(int ch, FILE *stream)
{
    /* 向USART_DATA寄存器写数据; */
    usart_data_transmit(g_Usb2ComInfo.usart, (uint8_t)ch);
    /* 等待TBE置位; */
    while (usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TBE) == RESET);

    //    /* 等待TC为1,表示全部发送完成; */
    //    while (usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TC) == RESET);
    //    /* 关闭发送功能。*/
    //    usart_transmit_config(g_Usb2ComInfo.usart, USART_TRANSMIT_DISABLE);
    return ch;
}

/**
 * @brief 创建全局函数指针变量
 */
static void (*g_pProcUartData)(uint8_t data);

/**
 * @brief 注册回调函数
 * @param pFunc 函数指针变量
 */
void RegUsb2ComCb(void (*pFunc)(uint8_t data))
{
    g_pProcUartData = pFunc;
}

void USART0_IRQHandler(void)
{
    if (usart_interrupt_flag_get(g_Usb2ComInfo.usart, USART_INT_FLAG_RBNE) != RESET)
    {
        usart_interrupt_flag_clear(g_Usb2ComInfo.usart, USART_INT_FLAG_RBNE);

        uint8_t uartData = (uint8_t)usart_data_receive(g_Usb2ComInfo.usart);
        g_pProcUartData(uartData);
    }
}

/**
  Put a character to the stderr

  \param[in]   ch  Character to output
  \return          The character written, or -1 on write error.
*/
int stderr_putchar(int ch)
{
    uint8_t buf[1];

    buf[0] = (uint8_t)ch;
    usart_data_transmit(g_Usb2ComInfo.usart, (uint8_t)buf[0]);
    while (RESET == usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TC));

    return (ch);
}
