#include <stdlib.h>
#include "modbus_slave.h"
#include "mb.h"
#include "sensor_drv.h"
#include "led_drv.h"

typedef struct
{
	uint8_t property;		// 读写的属性
	const uint16_t address; // 地址
	uint16_t minValue;		// 最小值
	uint16_t maxValue;		// 最大值
	void (*ReadCb)(uint16_t *value);
	void (*WriteCb)(uint16_t value);
} MbRegisterInstance_t;

#define R (1 << 0)
#define W (1 << 1)

static void ModbusGetTemp(uint16_t *value);
static void ModbusGetHumi(uint16_t *value);
static void ModbusSetLed1(uint16_t value);
static void ModbusSetLed2(uint16_t value);
static void ModbusSetLed3(uint16_t value);

static MbRegisterInstance_t g_regInstanceTab[] = {
	{
		.property = R,
		.address = 0x0000, // 温度 01 03 00 00 00 02
		.ReadCb = ModbusGetTemp,
	},
	{
		.property = R,
		.address = 0x0001, // 湿度
		.ReadCb = ModbusGetHumi,
	},
	{
		.property = R | W,
		.address = 0x0002, // LED1开关 01 06 00 02 00 01  ,LED1 LED2 01 10 00 02 00 02 04 00 01 00 01
		.minValue = 0,
		.maxValue = 1,
		.WriteCb = ModbusSetLed1,
	},
	{
		.property = R | W,
		.address = 0x0003, // LED2开关 01 06 00 03 00 01
		.minValue = 0,
		.maxValue = 1,
		.WriteCb = ModbusSetLed2,
	},
	{
		.property = R | W,
		.address = 0x0004, // LED2开关 01 06 00 04 00 01
		.minValue = 0,
		.maxValue = 1,
		.WriteCb = ModbusSetLed3,
	},
};

#define REG_TABLE_SIZE (sizeof(g_regInstanceTab) / sizeof(g_regInstanceTab[0]))

static eMBErrorCode WriteHoldingRegistersCb(uint8_t *pBuffer, uint16_t startAddr, uint16_t numReg)
{
	if (pBuffer == NULL)
	{
		return MB_EINVAL;
	}

	for (uint16_t i = 0; i < numReg; i++)
	{
		MbRegisterInstance_t *instance = NULL;
		for (uint8_t j = 0; j < REG_TABLE_SIZE; j++)
		{
			if (g_regInstanceTab[j].address != startAddr + i)
			{
				continue;
			}
			instance = &g_regInstanceTab[j];
			if (!(instance->property & W))
			{
				return MB_EINVAL;
			}

			uint16_t setVal = (pBuffer[2 * i] << 8 & 0XFF00) | (pBuffer[2 * i + 1] & 0x00ff);
			if (setVal < instance->minValue || setVal > instance->maxValue)
			{
				return MB_EINVAL;
			}

			if (instance->WriteCb != NULL)
			{
				instance->WriteCb(setVal);
			}
		}
		if (instance == NULL)
		{
			return MB_ENOREG;
		}
	}
	return MB_ENOERR;
}

static eMBErrorCode ReadHoldingRegistersCb(uint8_t *pBuffer, uint16_t startAddr, uint16_t numReg)
{
	if (pBuffer == NULL)
	{
		return MB_EINVAL;
	}

	for (uint16_t i = 0; i < numReg; i++)
	{
		MbRegisterInstance_t *instance = NULL;
		for (uint8_t j = 0; j < REG_TABLE_SIZE; j++)
		{
			if (g_regInstanceTab[j].address != startAddr + i)
			{
				continue;
			}
			instance = &g_regInstanceTab[j];
			if (!(instance->property & R))
			{
				return MB_EINVAL;
			}

			if (instance->ReadCb != NULL)
			{
				instance->ReadCb((uint16_t *)&pBuffer[2 * i]);
				uint16_t tmp;
				tmp = pBuffer[2 *i];
				pBuffer[2 * i] = pBuffer[2 * i + 1];
				pBuffer[2 * i + 1] = tmp;
			}
		}
		if (instance == NULL)
		{
			return MB_ENOREG;
		}
	}
	return MB_ENOERR;
}

void ModbusAppInit(void)
{
	ModbusSlaveInstance_t modbusSlaveInstance = {
		.baudRate = 9600,
		.slaveAddr = 0x01,
		.funcCb.WriteHoldingRegisters = WriteHoldingRegistersCb,
		.funcCb.ReadHoldingRegisters = ReadHoldingRegistersCb,
	};
	ModbusSlaveInit(&modbusSlaveInstance);
}

static void ModbusGetTemp(uint16_t *value)
{
	SensorData_t sensorData;
	GetSensorData(&sensorData);
	*value = (uint16_t)(sensorData.temperature * 10);
}

static void ModbusGetHumi(uint16_t *value)
{
	SensorData_t *sensorData = (SensorData_t *)malloc(sizeof(SensorData_t));
	GetSensorData(sensorData);
	*value = (uint16_t)(sensorData->humidity * 10);
	
	free(sensorData);
	sensorData = NULL;
}

static void ModbusSetLed1(uint16_t value)
{
	value == 0 ? TurnOffLed(LED1) : TurnOnLed(LED1);
}

static void ModbusSetLed2(uint16_t value)
{
	value == 0 ? TurnOffLed(LED2) : TurnOnLed(LED2);
}

static void ModbusSetLed3(uint16_t value)
{
	value == 0 ? TurnOffLed(LED3) : TurnOnLed(LED3);
}

void ModbusAppTask(void)
{
	(void)eMBPoll();
}
