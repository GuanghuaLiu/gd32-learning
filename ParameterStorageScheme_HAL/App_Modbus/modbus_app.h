#ifndef _MODBUS_APP_H_
#define _MODBUS_APP_H_

void ModbusAppInit(void);
void ModbusAppTask(void);

#endif
