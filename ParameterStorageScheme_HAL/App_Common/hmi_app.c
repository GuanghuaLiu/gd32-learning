#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "sensor_drv.h"
#include "key_drv.h"
#include "led_drv.h"
#include "store_app.h"

/**
 * @brief 人机交互功能模块任务函数
 * @param
 */
void HmiTask(void)
{
	// SensorData_t sensorData;
	// GetSensorData(&sensorData);
	// printf("temperature = %.1f ℃, humidity = %d%%.\n", sensorData.temperature, sensorData.humidity);
	uint8_t keyVal = GetKeyValue();
	switch (keyVal)
	{
		case KEY1_SHORT_PRESS:
			TurnOnLed(LED1);
			if (SetModbusParam(3))
			{
				printf("SetModbusParam sucess\n");
			}
			else
			{
				printf("SetModbusParam fail\n");
			}
			break;
		case KEY1_LONG_PRESS:
			TurnOffLed(LED1);
			break;
		case KEY2_SHORT_PRESS:
			TurnOnLed(LED2);
			break;
		case KEY2_LONG_PRESS:
			TurnOffLed(LED2);
			break;
		case KEY3_SHORT_PRESS:
			TurnOnLed(LED3);
			break;
		case KEY3_LONG_PRESS:
			TurnOffLed(LED3);
			break;
		default:
			break;
	}
}
