#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "eeprom_drv.h"
#include "norflash_drv.h"
#include "inflash_drv.h"
#include "delay.h"
#include "mb.h"
#include "store_app.h"

typedef struct
{
	uint16_t magicCode;

	uint8_t modbusAddress;

	uint8_t crcVal;
} SystemParameter_t;

#define MAGIC_CODE 0X5A5A
static const SystemParameter_t g_sysParamDefault =
{
		.magicCode = MAGIC_CODE,
		.modbusAddress = 0X01
};

static SystemParameter_t g_sysParamCurrent;

#if PARAM_SAVED_IN_EEPROM
#define SYSPARAM_MAX_SIZE		128
#define SYSPARAM_START_ADDR		0
#define BACKUP_START_ADDR		128
#endif
#if PARAM_SAVED_IN_NORFLASH
#define SYSPARAM_MAX_SIZE		4096
#define SYSPARAM_START_ADDR		0X00E000
#define BACKUP_START_ADDR		0X00F000
#endif
#if PARAM_SAVED_IN_INFLASH
#define SYSPARAM_MAX_SIZE		2048
#define SYSPARAM_START_ADDR		0x0807F000
#define BACKUP_START_ADDR		0x0807F800
#endif

static uint8_t CalcCrc8(uint8_t *pBuffer, uint32_t byteLen)
{
	uint8_t crc = 0xff; // CRC寄存器

	for (uint32_t num = 0; num < byteLen; num++)
	{
		crc ^= *pBuffer++; // 把数据与8位的CRC寄存器的8位相异或，结果存放于CRC寄存器。
		for (uint8_t x = 0; x < 8; x++)
		{ // 循环8次
			if (crc & 0x80)
			{				 // 判断最高位为：“1”
				crc <<= 1;	 // 先左移
				crc ^= 0X31; // 再与多项式0x31异或
			}
			else
			{			   // 判断最低位为：“0”
				crc <<= 1; // 右移
			}
		}
	}
	return crc; // 返回CRC校验值
}

#if PARAM_SAVED_IN_EEPROM
static bool ReadDataWithCheck(uint8_t readAddr, uint8_t *pBuffer, uint16_t numToRead)
#endif
#if PARAM_SAVED_IN_NORFLASH || PARAM_SAVED_IN_INFLASH
static bool ReadDataWithCheck(uint32_t readAddr, uint8_t *pBuffer, uint32_t numToRead)
#endif
{
#if PARAM_SAVED_IN_EEPROM
	// DelayNms(5);
	if (!ReadEepromData(readAddr, pBuffer, numToRead))
	{
		return false;
	}
#endif
#if PARAM_SAVED_IN_NORFLASH
	ReadNorflashData(readAddr, numToRead, pBuffer);
#endif
#if PARAM_SAVED_IN_INFLASH
	if (!ReadInflashData(readAddr, numToRead, pBuffer))
	{
		return false;
	}
#endif
	uint8_t crcVal = CalcCrc8(pBuffer, numToRead - 1);
	if (pBuffer[numToRead - 1] != crcVal)
	{
		return false;
	}
	return true;
}

static bool ReadSysParam(SystemParameter_t *sysParam)
{
	if (sysParam == NULL)
	{
		return false;
	}
	uint16_t sysParamLen = sizeof(SystemParameter_t);
	if (ReadDataWithCheck(SYSPARAM_START_ADDR, (uint8_t *)sysParam, sysParamLen))
	{
		return true;
	}
	if (ReadDataWithCheck(BACKUP_START_ADDR, (uint8_t *)sysParam, sysParamLen))
	{
		return true;
	}
	return false;
}

#if PARAM_SAVED_IN_EEPROM
static bool WriteDataWithCheck(uint8_t writeAddr, uint8_t *pBuffer, uint16_t numToWrite)
#endif
#if PARAM_SAVED_IN_NORFLASH || PARAM_SAVED_IN_INFLASH
static bool WriteDataWithCheck(uint32_t writeAddr, uint8_t *pBuffer, uint32_t numToWrite)
#endif
{
	pBuffer[numToWrite - 1] = CalcCrc8(pBuffer, numToWrite - 1);
#if PARAM_SAVED_IN_EEPROM
	if (!WriteEepromData(writeAddr, pBuffer, numToWrite))
	{
		return false;
	}
	DelayNms(5);
#endif
#if PARAM_SAVED_IN_NORFLASH
	EraseNorflashSectorForWrite(writeAddr, numToWrite);
	WriteNorflashData(writeAddr, numToWrite, pBuffer);
	
#endif
#if PARAM_SAVED_IN_INFLASH
	if (!EraseInflashForWrite(writeAddr, numToWrite))
	{
		return false;
	}	
	if (!WriteInflashData(writeAddr, numToWrite, pBuffer))
	{
		return false;
	}	
#endif
	return true;
}


static bool WriteSysParam(SystemParameter_t *sysParam)
{
	if (sysParam == NULL)
	{
		return false;
	}
	uint16_t sysParamLen = sizeof(SystemParameter_t);
	if (sysParamLen > SYSPARAM_MAX_SIZE)
	{
		return false;
	}

	if (!WriteDataWithCheck(SYSPARAM_START_ADDR, (uint8_t *)sysParam, sysParamLen))
	{
		return false;
	}

	WriteDataWithCheck(BACKUP_START_ADDR, (uint8_t *)sysParam, sysParamLen);
	return true;
}

void InitSysParam(void)
{
	SystemParameter_t sysParam;
	if (ReadSysParam(&sysParam) && sysParam.magicCode == MAGIC_CODE)
	{
		g_sysParamCurrent = sysParam;
		eMBSetSlaveAddr(g_sysParamCurrent.modbusAddress);
		return;
	}
	g_sysParamCurrent = g_sysParamDefault;
}

bool SetModbusParam(uint8_t addr)
{
	if (addr == g_sysParamCurrent.modbusAddress)
	{
		return true;
	}

	SystemParameter_t sysParam = g_sysParamCurrent;
	sysParam.modbusAddress = addr;

	if (eMBSetSlaveAddr(addr) != MB_ENOERR)
	{
		return false;
	}

	if (!WriteSysParam(&sysParam))
	{
		eMBSetSlaveAddr(g_sysParamCurrent.modbusAddress);
		return false;
	}

	g_sysParamCurrent = sysParam;
	return true;
}
