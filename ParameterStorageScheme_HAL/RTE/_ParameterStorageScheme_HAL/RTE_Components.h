
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'ParameterStorageScheme_HAL' 
 * Target:  'ParameterStorageScheme_HAL' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "gd32f30x.h"

/* Keil::Compiler&ARM Compiler:I/O:STDERR&ITM@1.2.0 */
#define RTE_Compiler_IO_STDERR          /* Compiler I/O: STDERR */
          #define RTE_Compiler_IO_STDERR_ITM      /* Compiler I/O: STDERR ITM */


#endif /* RTE_COMPONENTS_H */
