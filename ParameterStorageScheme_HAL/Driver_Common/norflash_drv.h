#ifndef _NORFLASH_DRV_H_
#define _NORFLASH_DRV_H_

#include <stdint.h>

void NorflashDrvInit(void);

/**
 * @brief 指定地址开始读出指定个数的数据
 * @param readAdder 读取数据的地址
 * @param len 读取数据的个数
 * @param pBuffer 指定数据保存的地址
 */
void ReadNorflashData(uint32_t readAddr, uint32_t len, uint8_t *pBuffer);

/**
 * @brief 指定地址开始写入指定个数的数据，调用前需要擦除flash
 * @param writeAddr 写入地址
 * @param pBuffer 数组首地址
 * @param len 要写入的数据个数
 */
void WriteNorflashData(uint32_t writeAddr, uint32_t len, uint8_t *pBuffer);

/**
 * @brief 擦除从eraseAddr开始到eraseAddr + len所对应的扇区
 * @param eraseAddr 所需擦除扇区的地址
 * @param len 数据的个数
 */
void EraseNorflashSectorForWrite(uint32_t eraseAddr, uint32_t len);

void NorflashDrvTest(void);

#endif
