/*-----------------------------------------------------------------------*/
/* Low level disk I/O module SKELETON for FatFs     (C)ChaN, 2019        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#include "ff.h"		/* Obtains integer types */
#include "diskio.h" /* Declarations of disk functions */
#include "norflash_drv.h"
#include "rtc_drv.h"

/* Definitions of physical drive number for each drive */
#define DEV_NORFLASH 0 /* Example: Map Norflash to physical drive 0 */
#define DEV_RAM 1	   /* Example: Map Ramdisk to physical drive 1 */
#define DEV_MMC 2	   /* Example: Map MMC/SD card to physical drive 2 */
#define DEV_USB 3	   /* Example: Map USB MSD to physical drive 3 */

/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(
	BYTE pdrv /* Physical drive nmuber to identify the drive */
)
{
	uint8_t mId;
	uint16_t dId;
	switch (pdrv)
	{
	case DEV_NORFLASH:

		ReadNorflashID(&mId, &dId);
		if (((uint32_t)mId << 16 | dId) == NORFLASH_ID)
		{
			return RES_OK;
		}
		break;
	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(
	BYTE pdrv /* Physical drive nmuber to identify the drive */
)
{
	uint8_t mId;
	uint16_t dId;
	switch (pdrv)
	{
	case DEV_NORFLASH:
		NorflashDrvInit();
		ReadNorflashID(&mId, &dId);
		if (((uint32_t)mId << 16 | dId) == NORFLASH_ID)
		{
			return RES_OK;
		}
		break;
	}
	return STA_NOINIT;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(
	BYTE pdrv,	  /* Physical drive nmuber to identify the drive */
	BYTE *buff,	  /* Data buffer to store read data */
	LBA_t sector, /* Start sector in LBA */
	UINT count	  /* Number of sectors to read */
)
{
	switch (pdrv)
	{
	case DEV_NORFLASH:
		ReadNorflashData(sector * NORFLASH_SECTOR_SIZE, count * NORFLASH_SECTOR_SIZE, buff);
		return RES_OK;
	}

	return RES_PARERR;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if FF_FS_READONLY == 0

DRESULT disk_write(
	BYTE pdrv,		  /* Physical drive nmuber to identify the drive */
	const BYTE *buff, /* Data to be written */
	LBA_t sector,	  /* Start sector in LBA */
	UINT count		  /* Number of sectors to write */
)
{
	switch (pdrv)
	{
	case DEV_NORFLASH:
		EraseNorflashSectorForWrite(sector * NORFLASH_SECTOR_SIZE, count * NORFLASH_SECTOR_SIZE);
		WriteNorflashData(sector * NORFLASH_SECTOR_SIZE, count * NORFLASH_SECTOR_SIZE, (BYTE *)buff);
		return RES_OK;
	}
	return RES_PARERR;
}

#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl(
	BYTE pdrv, /* Physical drive nmuber (0..) */
	BYTE cmd,  /* Control code */
	void *buff /* Buffer to send/receive control data */
)
{
	switch (pdrv)
	{
	case DEV_NORFLASH:

		switch (cmd)
		{
		case CTRL_SYNC:
			return RES_OK;
		case GET_SECTOR_COUNT:
			*(LBA_t *)buff = NORFLASH_SECTOR_COUNT;
			return RES_OK;
		case GET_SECTOR_SIZE:
			*(WORD *)buff = NORFLASH_SECTOR_SIZE;
			return RES_OK;
		default:
			break;
		}
		break;
	}

	return RES_PARERR;
}

/**
 * @brief Get any valid time even if the system does not support a real time clock.
 * @param
 * @return Currnet local time shall be returned as bit-fields packed into a DWORD value.
 */
DWORD get_fattime(void)
{
	RtcTime_t RtcTime;
	GetRtcTime(&RtcTime);
	return (DWORD)(RtcTime.year - 1980) << 25 | (DWORD)RtcTime.month << 21 | (DWORD)RtcTime.day << 16 | (DWORD)RtcTime.hour << 11 | (DWORD)RtcTime.second >> 1;
	
}
