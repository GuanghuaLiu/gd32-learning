#ifndef _RS485_DRV_H_
#define _RS485_DRV_H_

#include <stdbool.h>
#include <stdint.h>

void RS485DrvInit(void);
bool ReceiveByteTimeout(uint8_t *byte, uint32_t timeout);
bool GetKeyPressed(uint8_t *key);
void TransmitByte(uint8_t byte);

#endif
