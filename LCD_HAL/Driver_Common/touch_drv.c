#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "gd32f30x.h"
#include "systick.h"
#include "delay.h"
#include "touch_drv.h"

#define GET_I2C_SDA() gpio_input_bit_get(GPIOF, GPIO_PIN_9)
#define SET_I2C_SDA() gpio_bit_set(GPIOF, GPIO_PIN_9)
#define CLEAR_I2C_SDA() gpio_bit_reset(GPIOF, GPIO_PIN_9)

#define SET_I2C_SCL() gpio_bit_set(GPIOF, GPIO_PIN_10)
#define CLEAR_I2C_SCL() gpio_bit_reset(GPIOF, GPIO_PIN_10)

static void GpioInit(void)
{
    rcu_periph_clock_enable(RCU_GPIOB);

    gpio_init(GPIOF, GPIO_MODE_OUT_OD, GPIO_OSPEED_10MHZ, GPIO_PIN_9 | GPIO_PIN_10);

    rcu_periph_clock_enable(RCU_GPIOB);
    gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, GPIO_PIN_9 | GPIO_PIN_12); // RST | INT
}

/**
 * @brief 产生IIC起始时序，准备发送或接收数据前必须由起始序列开始
 * @note  SCL为高电平时，SDA由高电平向低电平跳变，开始传输数据
 *           生成下图所示的波形图，即为起始时序
 *                 1 2    3     4
 *                    __________
 *           SCL : __/          \_____
 *                 ________
 *           SDA :         \___________
 */
static void I2CStart(void)
{
    SET_I2C_SDA();
    SET_I2C_SCL();
    DelayNus(4);
    CLEAR_I2C_SDA();
    DelayNus(4);
    CLEAR_I2C_SCL();
    DelayNus(4);
}

/**
 * @brief   产生IIC停止时序
 * @note    SCL为高电平时，SDA由低电平向高电平跳变，结束传输数据
 *          生成下图所示的波形图，即为停止时序
 *                1 2   3  4
 *                       _______________
 *          SCL : ______/
 *                __        ____________
 *          SDA:    \______/
 */
static void I2CStop(void)
{
    CLEAR_I2C_SDA();
    DelayNus(4);
    SET_I2C_SCL();
    DelayNus(4);
    SET_I2C_SDA();
}

/**
 * @brief 等待接收端的应答信号
 * @return true or false
 * @note 当SDA拉低后，表示接收到ACK信号，然后，拉低SCL，
 *           此处表示发送端收到接收端的ACK
 *                _______|____
 *           SCL:        |    \_________
 *                _______|
 *           SDA:         \_____________
 */
static bool I2CWaitAck(void)
{
    SET_I2C_SDA();
    DelayNus(4);
    SET_I2C_SCL();
    DelayNus(4);

    while (GET_I2C_SDA())
    {
        static uint8_t errTimes = 0;
        errTimes++;
        if (errTimes > 200)
        {
            errTimes = 0;
            I2CStop();
            return false;
        }
    }
    CLEAR_I2C_SCL();
    DelayNus(4);

    return true;
}

/**
 * @brief 发送应答信号
 * @note  下面是具体的时序图
 *                 1 2     3      4      5
 *                         ______
 *           SCL: ________/      \____________
 *                __                     ______
 *           SDA:   \___________________/
 */
static void I2CSendAck(void)
{
    CLEAR_I2C_SDA();
    DelayNus(4);
    SET_I2C_SCL();
    DelayNus(4);
    CLEAR_I2C_SCL();
    DelayNus(4);

    SET_I2C_SDA();
}

/**
 * @brief 发送非应答信号
 * @note  下面是具体的时序图
 *               1 2     3      4
 *                        ______
 *          SCL: ________/      \______
 *               __ ___________________
 *          SDA: __/
 */
static void I2CSendNack(void)
{
    SET_I2C_SDA();
    DelayNus(4);
    SET_I2C_SCL();
    DelayNus(4);
    CLEAR_I2C_SCL();
    DelayNus(4);
}

/**
 * @brief 发送一字节，数据从高位开始发送出去
 * @param byte 待发送的字节
 * @note  下面是具体的时序图
 *                1 2     3      4
 *                         ______
 *           SCL: ________/      \______
 *                ______________________
 *           SDA: \\\___________________
 */
static void I2CWriteByte(uint8_t byte)
{
    for (uint8_t i = 0; i < 8; i++)
    {
        if (byte & 0x80)
        {
            SET_I2C_SDA();
        }
        else
        {
            CLEAR_I2C_SDA();
        }
        byte <<= 1;

        DelayNus(4);
        SET_I2C_SCL();
        DelayNus(4);
        CLEAR_I2C_SCL();
        DelayNus(4);
    }
}

/**
 * @brief  读取一字节数据
 * @return 待读取的字节
 * @note   下面是具体的时序图
 *                       ______
 *           SCL: ______/      \___
 *                ____________________
 *           SDA: \\\\______________\\\
 */
static uint8_t I2CReadByte(void)
{
    uint8_t byte = 0;
    SET_I2C_SDA();
    // DelayNus(4);
    for (uint8_t i = 0; i < 8; i++)
    {
        SET_I2C_SCL();
        DelayNus(4);

        byte <<= 1;
        byte |= (uint8_t)GET_I2C_SDA();

        CLEAR_I2C_SCL();
        DelayNus(4);
    }
    return byte;
}

#define TOUCH_I2C_WR 0 // 写控制bit
#define TOUCH_I2C_RD 1 // 读控制bit

/* GT911 部分寄存器定义 */
#define TOUCH_DEV_ADDR 0x28
#define GT911_CTRL_REG 0x8040 // GT911控制寄存器
#define GT911_CFGS_REG 0x8050 // GT911配置起始地址寄存器
#define GT911_PID_REG 0x8140  // GT911产品ID寄存器

#define GT911_STATUS_REG 0x814E // GT911当前检测到的触摸情况
#define GT911_TP1_REG 0x8150    // 第一个触摸点数据地址
#define GT911_TP2_REG 0x8158    // 第二个触摸点数据地址
#define GT911_TP3_REG 0x8160    // 第三个触摸点数据地址
#define GT911_TP4_REG 0x8168    // 第四个触摸点数据地址
#define GT911_TP5_REG 0x8170    // 第五个触摸点数据地址

#define DETECT_INTERVAL_TIME 20 // GT911检测间隔时间要求10ms以上

/**
 * @brief 配置触摸芯片的设备地址 0x28/0x29
 * @param
 */
static void CfgDevAddr(void)
{
    gpio_bit_reset(GPIOB, GPIO_PIN_9);

    gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, GPIO_PIN_12);
    gpio_bit_reset(GPIOB, GPIO_PIN_12);
    DelayNms(1);
    gpio_bit_set(GPIOB, GPIO_PIN_12);
    DelayNms(1);
    gpio_bit_set(GPIOB, GPIO_PIN_9);
    DelayNms(10);
    gpio_init(GPIOB, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_2MHZ, GPIO_PIN_12);
}

/**
 * @brief
 * @param regAddr
 * @param pBuffer
 * @param numToWrite
 * @return
 */
static bool WriteTouchRegisters(uint16_t regAddr, uint8_t *pBuffer, uint8_t numToWrite)
{
    I2CStart();
    I2CWriteByte(TOUCH_DEV_ADDR | TOUCH_I2C_WR);
    if (!I2CWaitAck())
    {
        goto write_err;
    }
    I2CWriteByte(regAddr >> 8 & 0xff);
    if (!I2CWaitAck())
    {
        goto write_err;
    }
    I2CWriteByte(regAddr & 0xff);
    if (!I2CWaitAck())
    {
        goto write_err;
    }

    for (uint8_t i = 0; i < numToWrite; i++)
    {
        I2CWriteByte(pBuffer[i]);
        if (!I2CWaitAck())
        {
            goto write_err;
        }    
    }

    I2CStop();
    return true;

write_err:
    I2CStop();
    return false;
}

static bool ReadTouchRegister(uint16_t regAddr, uint8_t *pBuffer, uint8_t numToRead)
{
    I2CStart();
    I2CWriteByte(TOUCH_DEV_ADDR | TOUCH_I2C_WR);
    if (!I2CWaitAck())
    {
        goto read_err;
    }

    I2CWriteByte(regAddr >> 8 & 0xff);
    if (!I2CWaitAck())
    {
        goto read_err;
    }    
    I2CWriteByte(regAddr & 0xff);
    if (!I2CWaitAck())
    {
        goto read_err;
    }

    I2CStart();
    I2CWriteByte(TOUCH_DEV_ADDR | TOUCH_I2C_RD);
    if (!I2CWaitAck())
    {
        goto read_err;
    }    

    for (uint8_t i = 0; i < numToRead - 1; i++)
    {
        *pBuffer++ = I2CReadByte();
        I2CSendAck();
    }
    *pBuffer = I2CReadByte();
    I2CSendNack();

    I2CStop();
    return true;

read_err:
    I2CStop();
    return false;
}

void TouchDrvInit(void)
{
    GpioInit();
    CfgDevAddr();

    uint8_t devId[5] = {0};
    if (ReadTouchRegister(GT911_PID_REG, devId, 4))
    {
        devId[4] = '\0';
        printf("devId = %s.\n", devId);
        return;
    }
    printf("Touch init failed.\n");

}


void TouchScan(TouchInfo_t *touchInfo)
{
    static uint64_t lastSysTime = 0;
    static TouchInfo_t lastTouchInfo = {UP};
    if (GetSystemRunTime() - lastSysTime < DETECT_INTERVAL_TIME)
    {
        *touchInfo = lastTouchInfo;
        return;
    }
    lastSysTime = GetSystemRunTime();

    uint8_t stateRegVal;
    uint8_t posBuffer[6];

    if (!ReadTouchRegister(GT911_STATUS_REG, &stateRegVal, 1))
    {
        printf("read state register error!\n");
        touchInfo->state = UP;
        lastTouchInfo = *touchInfo;
        return;
    }
    if (!(stateRegVal & 0x80))
    {
//        printf("It has no touched point.\n");
        touchInfo->state = UP;
        lastTouchInfo = *touchInfo;
        return;
    }
    uint8_t touchNums = stateRegVal & 0x0f;
    printf("touch nums = %d\n", touchNums);

    stateRegVal = 0;
    WriteTouchRegisters(GT911_STATUS_REG, &stateRegVal, 1);

    if (touchNums == 0 || touchNums > TOUCH_POINT_MAX)
    {
        touchInfo->state = UP;
        lastTouchInfo = *touchInfo;
        return;
    }

    ReadTouchRegister(GT911_TP1_REG, posBuffer, 6);
    touchInfo->point.x = (uint16_t)posBuffer[1] << 8 | posBuffer[0];
    touchInfo->point.y = (uint16_t)posBuffer[3] << 8 | posBuffer[2];
    touchInfo->point.size = (uint16_t)posBuffer[5] << 8 | posBuffer[4];

	printf("point[%d].x = %d, point[%d].y = %d, point[%d].size = %d\n", \
			0, touchInfo->point.x, 0, touchInfo->point.y, 0, touchInfo->point.size);

    touchInfo->state = DOWN;
    lastTouchInfo = *touchInfo;
    return;
}
