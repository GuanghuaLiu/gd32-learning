#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "gd32f30x.h"
#include "delay.h"

#define INDEX_TO_TEMP(index) ((int8_t)index)
static float g_temp;

#define MAX_BUF_SIZE 10
static int32_t g_temp10MulBuf[MAX_BUF_SIZE];

static const uint16_t g_ntcAdcTable[] =
	{
		3123, 3089, 3051, 3013, 2973, 2933, 2893, 2852, 2810, 2767, // 0 ~ 9 ℃
		2720, 2681, 2637, 2593, 2548, 2503, 2458, 2412, 2367, 2321, // 10 ~ 19 ℃
		2275, 2230, 2184, 2138, 2093, 2048, 2002, 1958, 1913, 1869, // 20 ~ 29 ℃
		1825, 1782, 1739, 1697, 1655, 1614, 1573, 1533, 1494, 1455, // 30 ~ 39 ℃
		1417, 1380, 1343, 1307, 1272, 1237, 1203, 1170, 1138, 1106, // 40 ~ 49 ℃
		1081, 1045, 1016, 987, 959, 932, 905, 879, 854, 829,		// 50 ~ 59 ℃
		806, 782, 760, 738, 716, 696, 675, 656, 637, 618,			// 60 ~ 69 ℃
		600, 583, 566, 550, 534, 518, 503, 489, 475, 461,			// 70 ~ 79 ℃
		448, 435, 422, 410, 398, 387, 376, 365, 355, 345,			// 80 ~ 89 ℃
		335, 326, 316, 308, 299, 290, 283, 274, 267, 259			// 90 ~ 99 ℃
};

#define NTC_TABLE_SIZE (sizeof(g_ntcAdcTable) / sizeof(g_ntcAdcTable[0]))

static void GpioInit(void)
{
	rcu_periph_clock_enable(RCU_GPIOC);
	gpio_init(GPIOC, GPIO_MODE_AIN, GPIO_OSPEED_10MHZ, GPIO_PIN_3);
}

static void AdcInit(void)
{
	/* 使能ADC时钟 */
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV6);
	/* 1. 确保ADC_CTL0寄存器的DISRC和SM位以及ADC_CTL1寄存器的CTN位为0；*/
	adc_deinit(ADC0);
	/* 2. 用模拟通道编号来配置RSQ0，配置 分辨率、独立模式、连续模式、数据对齐方式、通道个数；*/
	adc_resolution_config(ADC0, ADC_RESOLUTION_12B);
	adc_mode_config(ADC_MODE_FREE);
	adc_special_function_config(ADC0, ADC_CONTINUOUS_MODE, ENABLE);
	adc_data_alignment_config(ADC0, ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0, ADC_REGULAR_CHANNEL, 1);
	/* 3. 配置ADC_SAMPTx寄存器；*/
	adc_regular_channel_config(ADC0, 0, ADC_CHANNEL_13, ADC_SAMPLETIME_239POINT5);
	/* 4. 如果有需要，可以配置ADC_CTL1寄存器的ETERC和ETSRC位；*/
	adc_external_trigger_source_config(ADC0, ADC_REGULAR_CHANNEL, ADC0_1_2_EXTTRIG_REGULAR_NONE);
	adc_external_trigger_config(ADC0, ADC_REGULAR_CHANNEL, ENABLE);
	/* ADC硬件滤波 */
	adc_oversample_mode_config(ADC0, ADC_OVERSAMPLING_ALL_CONVERT, ADC_OVERSAMPLING_SHIFT_4B, ADC_OVERSAMPLING_RATIO_MUL16);
	adc_oversample_mode_enable(ADC0);
	/* 时间校准 */
	/* 1. 确保ADCON=1；*/
	adc_enable(ADC0);
	/* 2. 延迟14个CK_ADC以等待ADC稳定；*/
	DelayNus(20);
	/* 3. 设置RSTCLB (可选的)；*/
	/* 4. 设置CLB=1；*/
	/* 5. 等待直到CLB=0。*/
	adc_calibration_enable(ADC0);

	/* 5. 设置SWRCST位，或者为常规序列产生一个外部触发信号；*/
	adc_software_trigger_enable(ADC0, ADC_REGULAR_CHANNEL);
}

void TempDrvInit(void)
{
	GpioInit();
	AdcInit();
}

/**
 * @brief 算术平均值滤波算法
 * @param arr 目标数组
 * @param len 数组元素个数
 * @return 数组的平均数
 */
static int32_t MeanValueFilter(int32_t *arr, uint8_t len)
{
	int32_t sum = 0;
	for (uint8_t i = 0; i < len; i++)
	{
		sum += arr[i];
	}

	return sum / len;
}

static int Compare(const void *pa, const void *pb)
{
	return *(uint16_t *)pa - *(uint16_t *)pb;
}

/**
 * @brief 中位平均滤波算法
 * @param arr 目标数组
 * @param len 数组元素个数
 * @return 数组去掉最大最小值的平均数
 */
static int32_t MidianMeanFilter(int32_t *arr, uint8_t len)
{
	qsort(arr, len, sizeof(int32_t), Compare);

	return MeanValueFilter(&arr[1], len - 2);
}

/**
 * @brief 递增式二分查找目标值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t AscBinarySearch(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] > key)
		{
			right = middle - 1;
		}
		else if (arr[middle] < key)
		{
			left = middle + 1;
		}
		else
		{
			return middle;
		}
	}
	return -1;
}

/**
 * @brief 递减式二分查找目标值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t DesBinarySearch(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] < key)
		{
			right = middle - 1;
		}
		else if (arr[middle] > key)
		{
			left = middle + 1;
		}
		else
		{
			return middle;
		}
	}
	return -1;
}

/**
 * @brief 递增式二分查找接近值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t AscBinaryNear(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	int8_t index = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] < key)
		{
			left = middle + 1;
		}
		else
		{
			right = middle - 1;
			index = middle;
		}
	}
	return index;
}

/**
 * @brief 递减式二分查找接近值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t DesBinaryNear(const uint16_t *arr, uint8_t len, uint16_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	int8_t index = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] > key)
		{
			left = middle + 1;
		}
		else
		{
			right = middle - 1;
			index = middle;
		}
	}
	return index;
}

/**
 * @brief 获取温度传感器ADC值
 * @param
 * @return 温度传感器ADC数值
 */
static uint16_t GetAdcVal(void)
{
	/* 6. 等到EOC置1；*/
	while (!adc_flag_get(ADC0, ADC_FLAG_EOC))
		;
	/* 7. 从ADC_RDATA寄存器中读ADC转换结果；*/
	uint16_t adcVal = adc_regular_data_read(ADC0);
	/* 8. 写0清除EOC标志位。*/
	adc_flag_clear(ADC0, ADC_FLAG_EOC);
	return adcVal;
}

static int32_t Adc2Temp10Mul(uint16_t adcVal)
{
	int8_t index = DesBinaryNear(g_ntcAdcTable, NTC_TABLE_SIZE, adcVal);

	if (index == 0)
	{
		return 0;
	}
	int32_t temp10Mul = INDEX_TO_TEMP(index - 1) * 10 + (g_ntcAdcTable[index - 1] - adcVal) * 10 / (g_ntcAdcTable[index - 1] - g_ntcAdcTable[index]);
	return temp10Mul;
}

static void PushTemp10MulToBuf(int16_t temp10Mul)
{
	static uint8_t s_index = 0;
	g_temp10MulBuf[s_index] = temp10Mul;
	s_index++;
	s_index %= MAX_BUF_SIZE;
}

/**
 * @brief 触发驱动转换温度传感器数据
 * @param
 */
void TempSensorProc(void)
{
	static uint8_t s_convertNum = 0;
	uint16_t adcVal = GetAdcVal();
	int32_t temp10Mul = Adc2Temp10Mul(adcVal);

	PushTemp10MulToBuf(temp10Mul);
	s_convertNum++;
	if (s_convertNum < 3)
	{
		g_temp = g_temp10MulBuf[0] / 10.0f;
		return;
	}
	if (s_convertNum > MAX_BUF_SIZE)
	{
		s_convertNum = MAX_BUF_SIZE;
	}
	g_temp = MidianMeanFilter(g_temp10MulBuf, s_convertNum) / 10.0f;
}

/**
 * @brief 获取温度数据
 * @param
 * @return 温度数据，浮点数
 */
float GetTempData(void)
{
	return g_temp;
}
