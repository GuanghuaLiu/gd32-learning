#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "sensor_drv.h"

/**
 * @brief 人机交互功能模块任务函数
 * @param
 */
void HmiTask(void)
{
	SensorData_t *sensorData = (SensorData_t *)malloc(sizeof(SensorData_t));
	GetSensorData(sensorData);
//    printf("temperature = %.1f ℃.\n", sensorData->temperature);
	printf("temperature = %.1f ℃, humidity = %d%%.\n", sensorData->temperature, sensorData->humidity);
	
	free(sensorData);
	sensorData = NULL;
}
