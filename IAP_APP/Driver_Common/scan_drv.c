#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "gd32f30x.h"
#include "delay.h"

#define BUF_MAX_SIZE 10
static uint16_t g_adcVal[BUF_MAX_SIZE];
#define RDATA (ADC0 + 0X4C)

static void GpioInit(void)
{
	rcu_periph_clock_enable(RCU_GPIOC);
	gpio_init(GPIOC, GPIO_MODE_AIN, GPIO_OSPEED_10MHZ, GPIO_PIN_2);
}

static void AdcInit(void)
{
	/* 使能ADC时钟 */
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV6);
	/* 1. 确保ADC_CTL0寄存器的DISRC和SM位以及ADC_CTL1寄存器的CTN位为0；*/
	adc_deinit(ADC0);
	/* 2. 用模拟通道编号来配置RSQ0，配置 分辨率、独立模式、扫描模式、连续模式、数据对齐方式、通道个数；*/
	adc_resolution_config(ADC0, ADC_RESOLUTION_12B);
	adc_mode_config(ADC_MODE_FREE);
	adc_special_function_config(ADC0, ADC_SCAN_MODE, ENABLE);
	adc_special_function_config(ADC0, ADC_CONTINUOUS_MODE, ENABLE);
	adc_data_alignment_config(ADC0, ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0, ADC_REGULAR_CHANNEL, 1);
	/* 3. 配置ADC_SAMPTx寄存器；*/
	adc_regular_channel_config(ADC0, 0, ADC_CHANNEL_12, ADC_SAMPLETIME_239POINT5);
	/* 4. 如果有需要，可以配置ADC_CTL1寄存器的ETERC和ETSRC位；*/
	adc_external_trigger_source_config(ADC0, ADC_REGULAR_CHANNEL, ADC0_1_2_EXTTRIG_REGULAR_NONE);
	adc_external_trigger_config(ADC0, ADC_REGULAR_CHANNEL, ENABLE);

	adc_dma_mode_enable(ADC0);
	/* 时间校准 */
	/* 1. 确保ADCON=1；*/
	adc_enable(ADC0);
	/* 2. 延迟14个CK_ADC以等待ADC稳定；*/
	DelayNus(20);
	/* 3. 设置RSTCLB (可选的)；*/
	/* 4. 设置CLB=1；*/
	/* 5. 等待直到CLB=0。*/
	adc_calibration_enable(ADC0);

	/* 5. 设置SWRCST位，或者为常规序列产生一个外部触发信号；*/
	adc_software_trigger_enable(ADC0, ADC_REGULAR_CHANNEL);
}

static void DmaInit(void)
{
	rcu_periph_clock_enable(RCU_DMA0);
	/* 1. 读取 CHEN 位，如果为1(通道已使能)，清零该位。当CHEN为0时，请按照下列步骤配置 DMA 开始新的传输; */
	dma_deinit(DMA0, DMA_CH0);

	dma_parameter_struct dmaStruct;
	dma_struct_para_init(&dmaStruct);
	/* 2. 配置 DMA CHXCTL 寄存器的 M2M 及 DIR 位，选择传输模式; */
	dmaStruct.direction = DMA_PERIPHERAL_TO_MEMORY;
	/* 4. 配置 DMA CHXCTL 寄存器的 PRIO 位域，选择该通道的软件优先级; */
	dmaStruct.priority = DMA_PRIORITY_HIGH;
	/* 5. 通过 DMA CHXCTL 寄存器配置存储器和外设的传输宽度以及存储器和外设地址生成算法; */
	dmaStruct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;
	dmaStruct.memory_width = DMA_MEMORY_WIDTH_16BIT;
	dmaStruct.periph_inc = DMA_MEMORY_INCREASE_DISABLE;
	dmaStruct.periph_width = DMA_PERIPHERAL_WIDTH_16BIT;
	/* 6. 通过 DMA CHXCTL 寄存器配置传输完成中断，半传输完成中断，传输错误中断的使能位: */
	/* 7. 通过 DMA CHXPADDR 寄存器配置外设基地址; */
	dmaStruct.periph_addr = RDATA;
	/* 8. 通过 DMA CHXMADDR 寄存器配置存储器基地址; */
	dmaStruct.memory_addr = (uint32_t)g_adcVal;
	/* 9. 通过 DMA CHXCNT 寄存器配置数据传输总量; */
	dmaStruct.number = BUF_MAX_SIZE;
	dma_init(DMA0, DMA_CH0, &dmaStruct);
	/* 3. 配置 DMA CHXCTL 寄存器的 CMEN 位，选择是否使能循环模式; */
	dma_circulation_enable(DMA0, DMA_CH0);
	/* 10. 将 DMA CHXCTL 寄存器的 CHEN 位置 1，使能 DMA 通道。*/
	dma_channel_enable(DMA0, DMA_CH0);
}

void ScanDrvInit(void)
{
	GpioInit();
	AdcInit();
	DmaInit();
}

/**
 * @brief 算术平均值滤波算法
 * @param arr 目标数组
 * @param len 数组元素个数
 * @return 数组的平均数
 */
static uint16_t MeanValueFilter(uint16_t *arr, uint8_t len)
{
	uint16_t sum = 0;
	for (uint8_t i = 0; i < len; i++)
	{
		sum += arr[i];
	}

	return sum / len;
}

static int Compare(const void *pa, const void *pb)
{
	return (*(uint16_t *)pa - *(uint16_t *)pb);
}

/**
 * @brief 中位平均滤波算法
 * @param arr 目标数组
 * @param len 数组元素个数
 * @return 数组去掉最大最小值的平均数
 */
static uint16_t MidianMeanFilter(uint16_t *arr, uint8_t len)
{
	qsort(arr, len, sizeof(uint16_t), Compare);
	printf("After qsort.\n");
	for (uint8_t i = 0; i < len; i++)
	{
		printf("arr[%d] = %d.\n", i, arr[i]);
	}

	return MeanValueFilter(&arr[1], len - 2);
}

/**
 * @brief 递增式二分查找目标值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t AscBinarySearch(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] > key)
		{
			right = middle - 1;
		}
		else if (arr[middle] < key)
		{
			left = middle + 1;
		}
		else
		{
			return middle;
		}
	}
	return -1;
}

/**
 * @brief 递减式二分查找目标值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t DesBinarySearch(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] < key)
		{
			right = middle - 1;
		}
		else if (arr[middle] > key)
		{
			left = middle + 1;
		}
		else
		{
			return middle;
		}
	}
	return -1;
}

/**
 * @brief 递增式二分查找接近值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t AscBinaryNear(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	int8_t index = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] < key)
		{
			left = middle + 1;
		}
		else
		{
			right = middle - 1;
			index = middle;
		}
	}
	return index;
}

/**
 * @brief 递减式二分查找接近值
 * @param arr 目标数组
 * @param len 数组元素个数
 * @param key 目标值
 * @return 元素下标
 */
int8_t DesBinaryNear(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	int8_t index = 0;
	while (left <= right)
	{
		middle = left + (right - left) / 2;
		if (arr[middle] > key)
		{
			left = middle + 1;
		}
		else
		{
			right = middle - 1;
			index = middle;
		}
	}
	return index;
}

void ScanDrvTest(void)
{
	uint16_t adcVal[BUF_MAX_SIZE];
	memcpy(adcVal, g_adcVal, BUF_MAX_SIZE * sizeof(uint16_t));

	for (uint8_t i = 0; i < BUF_MAX_SIZE; i++)
	{
		printf("adcVal[%d] = %d.\n", i, adcVal[i]);
	}

	uint16_t res = MidianMeanFilter(adcVal, BUF_MAX_SIZE);
	int8_t index = AscBinaryNear(adcVal, BUF_MAX_SIZE, res + 1);
	printf("res = %d.\n", res);
	printf("index = %d.\n", index);
}
