#ifndef _FWDGT_DRV_H_
#define _FWDGT_DRV_H_

#include <stdint.h>

/**
 * @brief 独立看门狗硬件初始化
 * @param
 */
void FwdgtDrvInit(void);

/**
 * @brief 喂狗
 * @param  
 */
void FeedDog(void);

#endif
