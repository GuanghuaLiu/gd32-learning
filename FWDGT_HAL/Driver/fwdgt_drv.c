#include <stdint.h>
#include "gd32f30x.h"

/**
 * @brief 独立看门狗硬件初始化
 * @param
 */
void FwdgtDrvInit(void)
{
    /* 关闭FWDGT PSC和FWDGT RLD的写保护 */
    /* 配置预分频器和重装载寄存器 */
    fwdgt_config(40 * 2000 / 32, FWDGT_PSC_DIV32);
    /* 开启开门狗 */
    fwdgt_enable();
}

/**
 * @brief 喂狗
 * @param
 */
void FeedDog(void)
{
    /* 按照FWDGT_RLD寄存器的值重装载IWDG计数器 */
    fwdgt_counter_reload();
}
