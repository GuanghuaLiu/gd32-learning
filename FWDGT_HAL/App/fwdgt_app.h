#ifndef _FWDGT_APP_H_
#define _FWDGT_APP_H_

#include <stdint.h>

/**
 * @brief 独立看门狗任务处理函数
 * @param  
 */
void FwdgtTask(void);

#endif
