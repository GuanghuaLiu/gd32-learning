#include <stdint.h>
#include <stdio.h>
#include "ir_drv.h"
#include "led_drv.h"
#include "rtc_drv.h"

/**
 * @brief 人机交互功能模块任务函数
 * @param
 */
void HmiTask(void)
{
    RtcTime_t time = {0};
    GetRtcTime(&time);
    printf("%d-%02d-%02d %02d:%02d:%02d\n", time.year, time.month, time.day, 
                                  time.hour, time.minute, time.second);
}
