#ifndef _USB2COM_DRV_H_
#define _USB2COM_DRV_H_

#include <stdint.h>

/**
 * @brief USB转串口硬件初始化
 * @param
 */
void Usb2ComDrvInit(void);

/**
 * @brief 注册回调函数
 * @param pFunc 函数指针变量
 */
void RegUsb2ComCb(void (*pFunc)(uint8_t data));

#endif
