#include <stdint.h>
#include "gd32f30x.h"
#include "systick.h"

typedef struct
{
    rcu_periph_enum rcu;
    uint32_t gpio;
    uint32_t pin;
} Key_GPIO_t;

static Key_GPIO_t g_GpioList[] = {
    {RCU_GPIOA, GPIOA, GPIO_PIN_0},
    {RCU_GPIOG, GPIOG, GPIO_PIN_13},
    {RCU_GPIOG, GPIOG, GPIO_PIN_14},
    {RCU_GPIOG, GPIOG, GPIO_PIN_15}};

#define KEY_NUM_MAX (sizeof(g_GpioList) / sizeof(g_GpioList[0]))

/**
 * @brief 按键硬件初始化
 * @param
 */
void KeyDrvInit(void)
{
    for (uint8_t i = 0; i < KEY_NUM_MAX; i++)
    {
        rcu_periph_clock_enable(g_GpioList[i].rcu);
        gpio_init(g_GpioList[i].gpio, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_2MHZ, g_GpioList[i].pin);
    }
}

typedef enum
{
    KEY_RELEASE = 0,
    KEY_CONFIRM,
    KEY_SHORT_PRESS,
    KEY_LONG_PRESS
} KEY_STATE;

#define CONFIRM_TIME 10
#define LONGPRESS_TIME 1000

typedef struct
{
    KEY_STATE keyState;
    uint64_t firstSystemRunTime;
} Key_Info_t;

static Key_Info_t g_keyInfoList[KEY_NUM_MAX];

/**
 * @brief 使用状态机进行按键扫描，返回按键码值
 * @param keyNum 按键标号
 * @return 四个按键码值，短按0x01 0x02 0x03 0x04，长按0x81 0x82 0x83 0x84，没有按下为0
 */
static uint8_t KeyScan(uint8_t keyNum)
{
    uint64_t currentSystemRunTime = 0;
    uint8_t keyPress = gpio_input_bit_get(g_GpioList[keyNum].gpio, g_GpioList[keyNum].pin);

    switch (g_keyInfoList[keyNum].keyState)
    {
    case KEY_RELEASE:
        if (!keyPress)
        {
            g_keyInfoList[keyNum].keyState = KEY_CONFIRM;
            g_keyInfoList[keyNum].firstSystemRunTime = GetSystemRunTime();
        }
        break;
    case KEY_CONFIRM:
        if (!keyPress)
        {
            currentSystemRunTime = GetSystemRunTime();
            if (currentSystemRunTime - g_keyInfoList[keyNum].firstSystemRunTime > CONFIRM_TIME)
            {
                g_keyInfoList[keyNum].keyState = KEY_SHORT_PRESS;
            }
        }
        else
        {
            g_keyInfoList[keyNum].keyState = KEY_RELEASE;
        }
        break;
    case KEY_SHORT_PRESS:
        if (!keyPress)
        {
            currentSystemRunTime = GetSystemRunTime();
            if (currentSystemRunTime - g_keyInfoList[keyNum].firstSystemRunTime > LONGPRESS_TIME)
            {
                g_keyInfoList[keyNum].keyState = KEY_LONG_PRESS;
            }
        }
        else
        {
            g_keyInfoList[keyNum].keyState = KEY_RELEASE;
            return keyNum + 1;
        }
        break;
    case KEY_LONG_PRESS:
        if (keyPress)
        {
            g_keyInfoList[keyNum].keyState = KEY_RELEASE;
            return keyNum + 0x81;
        }
        break;
    default:
        break;
    }
    return 0;
}

/**
 * @brief 获取按键码值
 * @param
 * @return 四个按键码值，短按0x01 0x02 0x03 0x04，长按0x81 0x82 0x83 0x84，没有按下为0
 */
uint8_t GetKeyValue(void)
{
    uint8_t res = 0;
    for (uint8_t i = 0; i < KEY_NUM_MAX; i++)
    {
        res = KeyScan(i);
        if (res != 0)
        {
            break;
        }
    }
    return res;
}
