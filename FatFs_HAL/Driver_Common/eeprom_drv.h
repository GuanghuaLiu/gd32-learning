#ifndef _EEPROM_DRV_H_
#define _EEPROM_DRV_H_

#include <stdbool.h>
#include <stdint.h>

void EepromDrvInit(void);

void EepromDrvTest(void);

bool WriteEepromData(uint8_t writeAddr, uint8_t *pBuffer, uint16_t numToWrite);

bool ReadEepromData(uint8_t readAddr, uint8_t *pBuffer, uint16_t numToRead);
#endif
