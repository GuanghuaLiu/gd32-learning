#ifndef _SYSTICK_H_
#define _SYSTICK_H_

#include <stdint.h>

/**
 * @brief Systick初始化，产生中断时间为1ms
 * @param  
 */
void SystickInit(void);

/**
 * @brief 获取系统运行时间
 * @param
 * @return 系统运行时间， 单位为1ms
 */
uint64_t GetSystemRunTime(void);

#endif
