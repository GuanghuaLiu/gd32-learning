#include "gd32f30x.h"
#include "key_drv.h"
#include "led_drv.h"
#include "systick.h"

int main(void)
{
    uint8_t keyVal = 0;

    SystickInit();
    LedDrvInit();
    KeyDrvInit();

    while (1)
    {
        keyVal = GetKeyValue();
        switch (keyVal)
        {
        case KEY1_SHORT_PRESS:
            TurnOnLed(LED1);
            break;
        case KEY1_LONG_PRESS:
            TurnOffLed(LED1);
            break;
        case KEY2_SHORT_PRESS:
            TurnOnLed(LED2);
            break;
        case KEY2_LONG_PRESS:
            TurnOffLed(LED2);
            break;
        case KEY3_SHORT_PRESS:
            TurnOnLed(LED3);
            break;
        case KEY3_LONG_PRESS:
            TurnOffLed(LED3);
            break;
        case KEY4_SHORT_PRESS:
            TurnOnLed(LED1);
            TurnOnLed(LED2);
            TurnOnLed(LED3);
            break;
        case KEY4_LONG_PRESS:
            TurnOffLed(LED1);
            TurnOffLed(LED2);
            TurnOffLed(LED3);
            break;
        default:
            break;
        }
    }
}
