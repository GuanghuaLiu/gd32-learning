#include <stdint.h>
#include <stdio.h>
#include "rs485_drv.h"
#include "systick.h"
#include "delay.h"
#include "update.h"
#include "gd32f30x.h"

typedef void (*pFunction_t)(void);

static void DriverInit(void)
{
	RS485DrvInit();
	SystickInit();
	DwtDelayInit();
}

#define SRAM_START_ADDR 			0X20000000
#define SRAM_SIZE 					0X10000

static void BootToApp(void)
{
	uint32_t stackTopAddr = *(volatile uint32_t *)APP_ADDR_IN_FLASH;

	if ((stackTopAddr > SRAM_START_ADDR) && (stackTopAddr < SRAM_START_ADDR + SRAM_SIZE))
	{
		__disable_irq();

		__set_MSP(stackTopAddr);
		uint32_t resetHandlerAddr = *(volatile uint32_t *)(APP_ADDR_IN_FLASH + 4);
		pFunction_t UserApplication = (pFunction_t)resetHandlerAddr;
		UserApplication();
	}
	NVIC_SystemReset();
}

#define BOOT_DELAY_COUNT 			20000 // ��λms

#define DOWNLOAD_KEY_VALUE 			0X31
#define EXECUTE_KEY_VALUE 			0X32
static void MainMenuCmd(void)
{
	printf("\rHit any key to stop autoboot:     ");

	uint8_t keyVal;
	uint32_t timCount = (uint32_t)GetSystemRunTime();
	uint8_t bootDelayNow = 0;
	uint8_t bootDelayLast = 0;

	while ((timCount < BOOT_DELAY_COUNT) && !GetKeyPressed(&keyVal))
	{
		timCount = (uint32_t)GetSystemRunTime();
		bootDelayNow = (BOOT_DELAY_COUNT - timCount) / 1000;
		if (bootDelayLast != bootDelayNow)
		{
			printf("\b\b\b\b%02d s", bootDelayNow);
			bootDelayLast = bootDelayNow;
		}
	}

	if (timCount >= BOOT_DELAY_COUNT)
	{
		BootToApp();
	}

	while (1)
	{
		printf("\r\n\n**************************Main Menu**************************\r\n\n");
		printf("************[1] Download Image To Internal Flash*************\r\n\n");
		printf("************[2] Execute The App******************************\r\n\n");
		printf("*************************************************************\r\n\n");

		while (!GetKeyPressed(&keyVal))
			;

		if (keyVal == DOWNLOAD_KEY_VALUE)
		{
			UpdateApp();
		}
		if (keyVal == EXECUTE_KEY_VALUE)
		{
			BootToApp();
		}
	}
}

int main(void)
{
	DriverInit();

	while (1)
	{
		MainMenuCmd();
	}
}
