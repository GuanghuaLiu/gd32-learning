#ifndef _FATFS_APP_H_
#define _FATFS_APP_H_

void FatFsInit(void);
void FatfsTask(void);
void PrintFileData(void);

#endif
