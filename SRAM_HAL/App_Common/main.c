#include <stdint.h>
#include <stdio.h>

#include "systick.h"
#include "delay.h"
#include "led_drv.h"
#include "key_drv.h"
#include "usb2com_drv.h"
#include "sensor_drv.h"
#include "norflash_drv.h"
#include "rtc_drv.h"
#include "exsram_drv.h"

#include "hmi_app.h"
#include "sensor_app.h"
#include "fatfs_app.h"

//typedef struct
//{
//	uint8_t run;
//	uint16_t timCount;	// 设置多长时间执行函数任务，根据函数设置实际数据
//	uint16_t timRload;
//	void (*pTaskFunc)(void);
//} TaskComps_t;

//static TaskComps_t g_taskComps[] = {
//	{0, 5, 5, HmiTask},
//	{0, 1000, 1000, SensorAppTask},
//	{0, 10000, 10000, FatfsTask}
//	/* 添加业务功能模块*/
//};

//#define TASK_NUM_MAX (sizeof(g_taskComps) / sizeof(g_taskComps[0]))

///**
// * @brief 业务功能模块任务处理函数
// * @param
// */
//static void TaskHandler(void)
//{
//	for (uint8_t i = 0; i < TASK_NUM_MAX; i++)
//	{
//		if (g_taskComps[i].run)
//		{
//			g_taskComps[i].run = 0;
//			g_taskComps[i].pTaskFunc();
//		}
//	}
//}

///**
// * @brief 业务模块任务时间块处理函数
// * 在定时器中断服务函数中被间接调用，设置时间片标记，需要定时器1ms产生1次中断
// * @param
// */
//static void TaskSchedule(void)
//{
//	for (uint8_t i = 0; i < TASK_NUM_MAX; i++)
//	{
//		if (g_taskComps[i].timCount)
//		{
//			g_taskComps[i].timCount--;
//			if (0 == g_taskComps[i].timCount)
//			{
//				g_taskComps[i].run = 1;
//				g_taskComps[i].timCount = g_taskComps[i].timRload;
//			}
//		}
//	}
//}

static void DriverInit(void)
{
	DwtDelayInit();
	SystickInit();
	Usb2ComDrvInit();
	ExsramDrvInit();
}

static void AppInit(void)
{
//	TaskScheduleCbReg(TaskSchedule);
}

int main(void)
{
	DriverInit();
	AppInit();

	ExmcSramDrvTest();
	
	while (1)
	{
//		TaskHandler();
	}
}
