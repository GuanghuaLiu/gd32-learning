#include <stdint.h>

// #define GPIOA_BASE (uint32_t *)(0x40010800)   // GPIOA基地址
#define GPIOA_CTL1 (*((uint32_t *)(0x40010800 + 0X04)))
#define GPIOA_OCTL (*((uint32_t *)(0x40010800 + 0X0C)))
#define RCU_APB2EN (*((uint32_t *)(0x40021000 + 0X18)))
#define GPIOA_CTL0 (*((uint32_t *)(0x40010800 + 0X00)))
#define GPIOA_ISTAT (*((uint32_t *)(0x40010800 + 0X08)))

static void Delay(uint32_t count) // 误差 0us
{
    while (count--);
}

int main(void)
{
    // 时钟树，开启PA口时钟树
    RCU_APB2EN |= 0x01 << 2;
    // 端口控制寄存器，打开PA8，设置为推挽输出，最大速率2MHz
    GPIOA_CTL1 = 0x44444442;
    // 端口控制寄存器，打开PA0，设置为浮空输入
    GPIOA_CTL0 = 0X44444444;
    while (1)
    {
        while (0 == (GPIOA_ISTAT & 1 << 0))
        {
            // 端口输出控制寄存器, 输出高电平
            GPIOA_OCTL |= 0x01 << 8;
        }
        // 端口输出控制寄存器, 输出低电平
        GPIOA_OCTL &= ~(0x01 << 8);
    }
}
