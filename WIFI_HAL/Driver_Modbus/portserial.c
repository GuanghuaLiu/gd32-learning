/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */
/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include "gd32f30x.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- static functions ---------------------------------*/
static void prvvUARTTxReadyISR(void);
static void prvvUARTRxISR(void);
static void GpioInit(void);
static void UartInit(uint32_t ulBaudRate);
static void SwitchInit(void);
/* ----------------------- Start implementation -----------------------------*/
typedef struct
{
	rcu_periph_enum rcuGpio;
	uint32_t periphGpio;
	uint32_t rxPin;
	uint32_t txPin;
	rcu_periph_enum rcuUart;
	uint32_t periphUart;
	uint8_t irq;
	rcu_periph_enum rcuSwitchGpio;
	uint32_t switchGpio;
	uint32_t switchPin;
} UartHwInfo_t;

static UartHwInfo_t g_uartHwInfo = {RCU_GPIOA, GPIOA, GPIO_PIN_3, GPIO_PIN_2, RCU_USART1, USART1, USART1_IRQn, RCU_GPIOC, GPIOC, GPIO_PIN_5};

void vMBPortSerialEnable(BOOL xRxEnable, BOOL xTxEnable)
{
	/* If xRXEnable enable serial receive interrupts. If xTxENable enable
	 * transmitter empty interrupts.
	 */
	if (xRxEnable)
	{
		usart_interrupt_enable(g_uartHwInfo.periphUart, USART_INT_RBNE);
		gpio_bit_reset(g_uartHwInfo.switchGpio, g_uartHwInfo.switchPin);
	}
	else
	{
		usart_interrupt_disable(g_uartHwInfo.periphUart, USART_INT_RBNE);
		gpio_bit_set(g_uartHwInfo.switchGpio, g_uartHwInfo.switchPin);
	}

	if (xTxEnable)
	{
		usart_interrupt_enable(g_uartHwInfo.periphUart, USART_INT_TC);
		gpio_bit_set(g_uartHwInfo.switchGpio, g_uartHwInfo.switchPin);
	}
	else
	{
		usart_interrupt_disable(g_uartHwInfo.periphUart, USART_INT_TC);
		gpio_bit_reset(g_uartHwInfo.switchGpio, g_uartHwInfo.switchPin);
	}
}

static void GpioInit(void)
{
	rcu_periph_clock_enable(g_uartHwInfo.rcuGpio);
	gpio_init(g_uartHwInfo.periphGpio, GPIO_MODE_IN_FLOATING, GPIO_OSPEED_10MHZ, g_uartHwInfo.rxPin);
	gpio_init(g_uartHwInfo.periphGpio, GPIO_MODE_AF_PP, GPIO_OSPEED_10MHZ, g_uartHwInfo.txPin);
}

static void UartInit(uint32_t baudRate)
{
	rcu_periph_clock_enable(g_uartHwInfo.rcuUart);
	usart_deinit(g_uartHwInfo.periphUart);
	usart_word_length_set(g_uartHwInfo.periphUart, USART_WL_8BIT);
	usart_parity_config(g_uartHwInfo.periphUart, USART_PM_NONE);
	usart_stop_bit_set(g_uartHwInfo.periphUart, USART_STB_1BIT);
	usart_baudrate_set(g_uartHwInfo.periphUart, baudRate);
	usart_data_first_config(g_uartHwInfo.periphUart, USART_MSBF_LSB);
	usart_transmit_config(g_uartHwInfo.periphUart, USART_TRANSMIT_ENABLE);
	usart_receive_config(g_uartHwInfo.periphUart, USART_RECEIVE_ENABLE);
	nvic_irq_enable(g_uartHwInfo.irq, 0, 0);
	usart_enable(g_uartHwInfo.periphUart);
}

static void SwitchInit(void)
{
	rcu_periph_clock_enable(g_uartHwInfo.rcuSwitchGpio);
	gpio_init(g_uartHwInfo.switchGpio, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, g_uartHwInfo.switchPin);
}

BOOL xMBPortSerialInit(UCHAR ucPORT, ULONG ulBaudRate, UCHAR ucDataBits, eMBParity eParity)
{
	(void)ucPORT;
	(void)ucDataBits;
	(void)eParity;
	GpioInit();
	UartInit(ulBaudRate);
	SwitchInit();
	return TRUE;
}

BOOL xMBPortSerialPutByte(CHAR ucByte)
{
	/* Put a byte in the UARTs transmit buffer. This function is called
	 * by the protocol stack if pxMBFrameCBTransmitterEmpty( ) has been
	 * called. */
	usart_data_transmit(g_uartHwInfo.periphUart, ucByte);
	return TRUE;
}

BOOL xMBPortSerialGetByte(CHAR *pucByte)
{
	/* Return the byte in the UARTs receive buffer. This function is called
	 * by the protocol stack after pxMBFrameCBByteReceived( ) has been called.
	 */
	*pucByte = usart_data_receive(g_uartHwInfo.periphUart);
	return TRUE;
}

/* Create an interrupt handler for the transmit buffer empty interrupt
 * (or an equivalent) for your target processor. This function should then
 * call pxMBFrameCBTransmitterEmpty( ) which tells the protocol stack that
 * a new character can be sent. The protocol stack will then call
 * xMBPortSerialPutByte( ) to send the character.
 */
static void prvvUARTTxReadyISR(void)
{
	pxMBFrameCBTransmitterEmpty();
}

/* Create an interrupt handler for the receive interrupt for your target
 * processor. This function should then call pxMBFrameCBByteReceived( ). The
 * protocol stack will then call xMBPortSerialGetByte( ) to retrieve the
 * character.
 */
static void prvvUARTRxISR(void)
{
	pxMBFrameCBByteReceived();
}

void USART1_IRQHandler(void)
{
	if (usart_interrupt_flag_get(g_uartHwInfo.periphUart, USART_INT_FLAG_TC))
	{
		usart_interrupt_flag_clear(g_uartHwInfo.periphUart, USART_INT_FLAG_TC);
		prvvUARTTxReadyISR();
	}
	if (usart_interrupt_flag_get(g_uartHwInfo.periphUart, USART_INT_FLAG_RBNE))
	{
		usart_interrupt_flag_clear(g_uartHwInfo.periphUart, USART_INT_FLAG_RBNE);
		prvvUARTRxISR();
	}
}
