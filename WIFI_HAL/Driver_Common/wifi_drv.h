#ifndef _WIFI_DRV_H_
#define _WIFI_DRV_H_

#include <stdint.h>

void HwResetWifiModule(void);

void WifiDrvInit(void);

void SendWifiModuleStr(char *sendStr);

char *RecvWifiModuleStr(void);

void ClearRecvStr(void);

#endif
