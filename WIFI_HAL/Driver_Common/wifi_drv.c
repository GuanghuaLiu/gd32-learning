#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "gd32f30x.h"
#include "delay.h"

#define MAX_BUFFER_SIZE 256
static uint8_t g_recvBuffer[MAX_BUFFER_SIZE];

#define USART2_DATA_ADDR (uint32_t)(USART2 + 0X04)

typedef struct
{
    rcu_periph_enum comGpioRcu;
    rcu_periph_enum resetGpioRcu;
    rcu_periph_enum uartRcu;
    rcu_periph_enum dmaRcu;

    uint32_t comGpio;
    uint32_t comTxPin;
    uint32_t comRxPin;

    uint8_t uartIrq;
    uint32_t uartPort;

    uint32_t dmaPort;
    dma_channel_enum dmaCh;

    uint32_t resetGpio;
    uint32_t resetPin;
} WifiModuleHwInfo_t;

static WifiModuleHwInfo_t g_wifiModuleInfo = {
    RCU_GPIOB,
    RCU_GPIOG,
    RCU_USART2,
    RCU_DMA0,
    GPIOB,
    GPIO_PIN_10,
    GPIO_PIN_11,
    USART2_IRQn,
    USART2,
    DMA0,
    DMA_CH2,
    GPIOG,
    GPIO_PIN_7,
};

static void GpioInit(void)
{
    rcu_periph_clock_enable(g_wifiModuleInfo.comGpioRcu);
    rcu_periph_clock_enable(g_wifiModuleInfo.resetGpioRcu);

    gpio_init(g_wifiModuleInfo.comGpio, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, g_wifiModuleInfo.comTxPin);
    gpio_init(g_wifiModuleInfo.comGpio, GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, g_wifiModuleInfo.comRxPin);

    gpio_init(g_wifiModuleInfo.resetGpio, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, g_wifiModuleInfo.resetPin);
}

static void UartInit(uint32_t baudRate)
{
    rcu_periph_clock_enable(g_wifiModuleInfo.uartRcu);

    usart_deinit(g_wifiModuleInfo.uartPort);

    usart_word_length_set(g_wifiModuleInfo.uartPort, USART_WL_8BIT);
    usart_parity_config(g_wifiModuleInfo.uartPort, USART_PM_NONE);
    usart_stop_bit_set(g_wifiModuleInfo.uartPort, USART_STB_1BIT);
    usart_baudrate_set(g_wifiModuleInfo.uartPort, baudRate);
    usart_transmit_config(g_wifiModuleInfo.uartPort, USART_TRANSMIT_ENABLE);
    usart_receive_config(g_wifiModuleInfo.uartPort, USART_RECEIVE_ENABLE);
    usart_dma_receive_config(g_wifiModuleInfo.uartPort, USART_RECEIVE_DMA_ENABLE);

    usart_interrupt_enable(g_wifiModuleInfo.uartPort, USART_INT_IDLE);
    nvic_irq_enable(g_wifiModuleInfo.uartIrq, 0, 0);

    usart_enable(g_wifiModuleInfo.uartPort);
}

static void DmaInit()
{
    rcu_periph_clock_enable(g_wifiModuleInfo.dmaRcu);
    dma_deinit(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh);

    dma_parameter_struct paramInitStruct;
    dma_struct_para_init(&paramInitStruct);

    paramInitStruct.direction = DMA_PERIPHERAL_TO_MEMORY;
    paramInitStruct.periph_addr = USART2_DATA_ADDR;
    paramInitStruct.periph_inc = DMA_PERIPH_INCREASE_DISABLE;
    paramInitStruct.periph_width = DMA_PERIPHERAL_WIDTH_8BIT;

    paramInitStruct.memory_addr = (uint32_t)g_recvBuffer;
    paramInitStruct.memory_inc = DMA_MEMORY_INCREASE_ENABLE;
    paramInitStruct.memory_width = DMA_MEMORY_WIDTH_8BIT;

    paramInitStruct.number = MAX_BUFFER_SIZE;
    paramInitStruct.priority = DMA_PRIORITY_HIGH;
    dma_init(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh, &paramInitStruct);

    dma_circulation_disable(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh);
    dma_channel_enable(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh);
}

void HwResetWifiModule(void)
{
    printf("WIFI module, now perform a hardware reset on it!\r\n");
    gpio_bit_reset(g_wifiModuleInfo.resetGpio, g_wifiModuleInfo.resetPin);
    DelayNms(100);
    gpio_bit_set(g_wifiModuleInfo.resetGpio, g_wifiModuleInfo.resetPin);
    // DelayNms(100);
}

void WifiDrvInit(void)
{
    GpioInit();
    DmaInit();
    UartInit(115200);
    HwResetWifiModule();
}

void USART2_IRQHandler(void)
{
    if (RESET != usart_interrupt_flag_get(g_wifiModuleInfo.uartPort, USART_INT_FLAG_IDLE))
    {
        usart_interrupt_flag_clear(g_wifiModuleInfo.uartPort, USART_INT_FLAG_IDLE);
        usart_data_receive(g_wifiModuleInfo.uartPort);

        printf("The string received: %s.\r\n", g_recvBuffer);
        dma_channel_disable(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh);
        dma_transfer_number_config(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh, MAX_BUFFER_SIZE);
        dma_channel_enable(g_wifiModuleInfo.dmaPort, g_wifiModuleInfo.dmaCh);
    }
}

void SendWifiModuleStr(char *sendStr)
{
    printf("The string sended: %s.\r\n", sendStr);

    while (*sendStr != '\0')
    {
        usart_data_transmit(g_wifiModuleInfo.uartPort, *sendStr);
        while (RESET == usart_flag_get(g_wifiModuleInfo.uartPort, USART_FLAG_TC))
        {
        }
        sendStr++;
    }
}

char *RecvWifiModuleStr(void)
{
    return (char *)g_recvBuffer;
}

void ClearRecvStr(void)
{
    memset(g_recvBuffer, 0, MAX_BUFFER_SIZE);
}
