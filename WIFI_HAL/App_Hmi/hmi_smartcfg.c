#include <stdio.h>
#include "lvgl.h"
#include "wifi_app.h"

LV_FONT_DECLARE(lv_font_zihun_26);
LV_FONT_DECLARE(lv_font_zihun_28);
LV_FONT_DECLARE(lv_font_zihun_40);

static void CreateTitleArea(lv_obj_t *parent, const char *titleStr)
{
    lv_obj_t *label = lv_label_create(parent);
    lv_label_set_text(label, titleStr);
    lv_obj_set_style_text_font(label, &lv_font_zihun_28, 0);
    lv_obj_set_style_text_color(label, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_CENTER, 0);
    //    lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);
}

static uint8_t g_timerCount;
static void CfgWifiBtnEvent(lv_event_t *event)
{
    lv_obj_t *btn = lv_event_get_target(event);
    StartSmartConfigure();
    g_timerCount = SMARTCFG_TIMEOUT_MAX / 1000;
}

static void RefreshTimerCount(lv_timer_t *timer)
{
    lv_obj_t *label = timer->user_data;
    WifiConnectState_t wifiConnectState = GetWifiConnectState();
    char strBuffer[50];

    switch (wifiConnectState)
    {
    case WIFI_CONNECT_ING:
        lv_label_set_text(label, "Wifi Connecting");
        break;
    case WIFI_CONNECT_SUCCESS:
        lv_label_set_text(label, "Wifi Connected");
        break;
    case WIFI_CONNECT_FAIL:
        lv_label_set_text_static(label, "Wifi Disconnected");
        break;
    case WIFI_SMART_CONFIGURE:
        sprintf(strBuffer, "%02d", g_timerCount--);
        lv_label_set_text(label, strBuffer);
        break;
    default:
        break;
    }
}

static void CreateMainArea(lv_obj_t *parent)
{
    static lv_style_t style;
    lv_style_init(&style);

    lv_style_set_width(&style, 280);
    lv_style_set_height(&style, 280);
    lv_style_set_radius(&style, 140);
    lv_style_set_align(&style, LV_ALIGN_CENTER);
    lv_style_set_bg_color(&style, lv_color_make(35, 52, 240));
    lv_style_set_bg_opa(&style, LV_OPA_100);
    lv_style_set_border_width(&style, 0);

    lv_obj_t *btn = lv_btn_create(parent);
    lv_obj_remove_style_all(btn);
    lv_obj_add_style(btn, &style, 0);
    lv_obj_add_event_cb(btn, CfgWifiBtnEvent, LV_EVENT_LONG_PRESSED, NULL);

    lv_obj_t *label = lv_label_create(btn);
    lv_label_set_text(label, "Network Disconnect");
    lv_obj_set_width(label, 200);
    lv_label_set_long_mode(label, LV_LABEL_LONG_WRAP);
    lv_obj_set_style_text_align(label, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_set_style_text_font(label, &lv_font_zihun_40, 0);
    lv_obj_set_style_text_color(label, lv_color_make(240, 135, 132), 0);
    lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);

    lv_timer_create(RefreshTimerCount, 1000, label);
}

void SmarCfgNetworkInit(void)
{
    /* 创建smartCfgNetworkScr屏幕 */
    lv_obj_t *smartCfgNetworkScr = lv_obj_create(lv_scr_act());
    lv_obj_remove_style_all(smartCfgNetworkScr);
    lv_obj_set_size(smartCfgNetworkScr, lv_pct(100), lv_pct(100));
    lv_obj_set_style_bg_color(smartCfgNetworkScr, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_bg_opa(smartCfgNetworkScr, LV_OPA_100, 0);
    lv_obj_set_style_radius(smartCfgNetworkScr, 0, 0);
    lv_obj_set_style_border_width(smartCfgNetworkScr, 0, 0);

    /* 创建smartCfgNetworkScr页面Title控件 */
    lv_obj_t *titleCont = lv_obj_create(smartCfgNetworkScr);

    lv_obj_set_size(titleCont, lv_pct(100), lv_pct(10));
    lv_obj_set_style_bg_color(titleCont, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_radius(titleCont, 0, 0);
    lv_obj_set_style_border_width(titleCont, 0, 0);
    lv_obj_set_flex_flow(titleCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_flex_align(titleCont, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
    lv_obj_set_scroll_dir(titleCont, LV_DIR_NONE);
    CreateTitleArea(titleCont, "smartConfig Network");

    /* 创建smartCfgNetworkScr页面Line控件 */
    static lv_point_t linePoints[2];
    linePoints[0].x = 10;
    linePoints[0].y = LV_VER_RES / 10 + 10; // LV_VER_RES/10对应titleCont的lv_pct(10)
    linePoints[1].x = LV_HOR_RES - 10;
    linePoints[1].y = LV_VER_RES / 10 + 10;
    lv_obj_t *line = lv_line_create(smartCfgNetworkScr);
    lv_obj_set_style_line_width(line, 1, 0);
    lv_obj_set_style_line_color(line, lv_color_make(0xFF, 0xFF, 0xFF), 0);
    lv_obj_set_style_line_opa(line, LV_OPA_20, 0);
    lv_line_set_points(line, linePoints, 2);

    /* 创建smartCfgNetworkScr页面内容主体，obj控件 */
    lv_obj_t *mainCont = lv_obj_create(smartCfgNetworkScr);
    lv_obj_center(mainCont);
    lv_obj_set_size(mainCont, lv_pct(100), lv_pct(41));
    lv_obj_set_style_bg_color(mainCont, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_radius(mainCont, 0, 0);
    lv_obj_set_style_border_width(mainCont, 0, 0);
    //    lv_obj_set_flex_flow(mainCont, LV_FLEX_FLOW_ROW_WRAP);
    //    lv_obj_set_flex_align(mainCont, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_SPACE_EVENLY);
    lv_obj_set_scroll_dir(mainCont, LV_DIR_NONE);
    CreateMainArea(mainCont);
}