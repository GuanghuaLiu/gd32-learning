#include <stdio.h>
#include "lvgl.h"
#include "sensor_drv.h"

LV_FONT_DECLARE(lv_font_zihun_26);
LV_FONT_DECLARE(lv_font_zihun_28);
LV_FONT_DECLARE(lv_font_zihun_40);
LV_IMG_DECLARE(g_tempImg);
LV_IMG_DECLARE(g_humiImg);

static lv_obj_t * g_tempValLabel;
static lv_obj_t * g_humiValLabel;

static void CreateTitleArea(lv_obj_t *parent, const char *titleStr)
{
    lv_obj_t *label = lv_label_create(parent);
    lv_label_set_text(label, titleStr);
    lv_obj_set_style_text_font(label, &lv_font_zihun_28, 0);
    lv_obj_set_style_text_color(label, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    //lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);

}

static void RefreshSensorData(lv_timer_t * timer)
{
	SensorData_t sensorData;
	GetSensorData(&sensorData);
	
	char buffer[5] = { 0 };
	sprintf(buffer, "%02d", (uint8_t)sensorData.temperature);
	lv_label_set_text(g_tempValLabel, buffer);

	sprintf(buffer, "%02d", (uint8_t)sensorData.humidity);
	lv_label_set_text(g_humiValLabel, buffer);	
}

static void CreateMainArea(lv_obj_t *parent)
{
    lv_obj_t *imgObj = lv_img_create(parent);
    lv_img_set_src(imgObj, &g_tempImg);
    lv_obj_align(imgObj, LV_ALIGN_TOP_LEFT, 0, 0);

    lv_obj_t *imgName = lv_label_create(parent);
    lv_label_set_text(imgName, "温度(℃)");
    lv_obj_set_style_text_font(imgName, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(imgName, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align_to(imgName, imgObj, LV_ALIGN_OUT_RIGHT_MID, 20, 0);

    g_tempValLabel = lv_label_create(parent);
    lv_label_set_text(g_tempValLabel, "0");
    lv_obj_set_style_text_font(g_tempValLabel, &lv_font_zihun_40, 0);
    lv_obj_set_style_text_color(g_tempValLabel, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align_to(g_tempValLabel, imgName, LV_ALIGN_OUT_RIGHT_MID, 50, 0);

    imgObj = lv_img_create(parent);
    lv_img_set_src(imgObj, &g_humiImg);
    lv_obj_align(imgObj, LV_ALIGN_BOTTOM_LEFT, 0, 0);

    imgName = lv_label_create(parent);
    lv_label_set_text(imgName, "湿度(%)");
    lv_obj_set_style_text_font(imgName, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(imgName, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align_to(imgName, imgObj, LV_ALIGN_OUT_RIGHT_MID, 20, 0);

    g_humiValLabel = lv_label_create(parent);
    lv_label_set_text(g_humiValLabel, "0");
    lv_obj_set_style_text_font(g_humiValLabel, &lv_font_zihun_40, 0);
    lv_obj_set_style_text_color(g_humiValLabel, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align_to(g_humiValLabel, imgName, LV_ALIGN_OUT_RIGHT_MID, 50, 0);
	
	lv_timer_t *timer = lv_timer_create(RefreshSensorData, 3000, 0);
}

void TempHumiScrInit(void)
{
    /* 创建TempHumiScr页面 */
    lv_obj_t *TempHumiScr = lv_obj_create(lv_scr_act());
    lv_obj_remove_style_all(TempHumiScr);
	lv_obj_set_size(TempHumiScr, lv_pct(100), lv_pct(100));
    lv_obj_set_style_bg_color(TempHumiScr, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_bg_opa(TempHumiScr, LV_OPA_100, 0);
    lv_obj_set_style_radius(TempHumiScr, 0, 0);
    lv_obj_set_style_border_width(TempHumiScr, 0, 0);

    /* 创建TempHumiScr页面Title控件 */
    lv_obj_t *titleCont = lv_obj_create(TempHumiScr);

    lv_obj_set_size(titleCont, lv_pct(100), lv_pct(10));
    lv_obj_set_style_bg_color(titleCont, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_radius(titleCont, 0, 0);
    lv_obj_set_style_border_width(titleCont, 0, 0);
    lv_obj_set_flex_flow(titleCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_flex_align(titleCont, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
    lv_obj_set_scroll_dir(titleCont, LV_DIR_NONE);
    CreateTitleArea(titleCont, "温度湿度");


    /* 创建TempHumiScr页面Line控件 */
    static lv_point_t linePoints[2];
    linePoints[0].x = 10;
    linePoints[0].y = LV_VER_RES / 10 + 10;
    linePoints[1].x = LV_HOR_RES - 10;
    linePoints[1].y = LV_VER_RES / 10 + 10;

    lv_obj_t *line = lv_line_create(TempHumiScr);
    lv_obj_set_style_line_color(line, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_set_style_line_opa(line, LV_OPA_10, 0);
    lv_obj_set_style_line_width(line, 1, 0);
    lv_line_set_points(line, linePoints, 2);

    /* 创建TempHumiScr页面内容主体，obj控件 */
    lv_obj_t *mainCont = lv_obj_create(TempHumiScr);
    lv_obj_center(mainCont);
    lv_obj_set_size(mainCont, lv_pct(100), lv_pct(41));
    lv_obj_set_style_bg_color(mainCont, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_radius(mainCont, 0, 0);
    lv_obj_set_style_border_width(mainCont, 0, 0);
//    lv_obj_set_flex_flow(mainCont, LV_FLEX_FLOW_ROW_WRAP);
//    lv_obj_set_flex_align(mainCont, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_SPACE_EVENLY);
    lv_obj_set_scroll_dir(mainCont, LV_DIR_NONE);
    CreateMainArea(mainCont);
}