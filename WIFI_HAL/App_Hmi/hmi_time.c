#include <stdio.h>
#include "lvgl.h"
#include "rtc_drv.h"

LV_FONT_DECLARE(lv_font_zihun_26);
LV_FONT_DECLARE(lv_font_zihun_28);
LV_FONT_DECLARE(lv_font_zihun_40);

static lv_obj_t * g_dateTa;
static lv_obj_t * g_hourDd;
static lv_obj_t * g_minuteDd;
static RtcTime_t g_rtcTime;

static void CreateTitleArea(lv_obj_t *parent, const char *titleStr)
{
    lv_obj_t *label = lv_label_create(parent);
    lv_label_set_text(label, titleStr);
    lv_obj_set_style_text_font(label, &lv_font_zihun_28, 0);
    lv_obj_set_style_text_color(label, lv_color_make(0XFF, 0XFF, 0XFF), 0);
//    lv_obj_align(label, LV_ALIGN_CENTER, 0, 0);

}

static void CalendarEventCb(lv_event_t * event)
{

    lv_event_code_t code = lv_event_get_code(event);
    lv_obj_t * ta = lv_event_get_user_data(event);
    lv_obj_t * obj = lv_event_get_current_target(event);
    if(code == LV_EVENT_VALUE_CHANGED) 
	{
        char buf[32] = {0};
        lv_calendar_date_t date;
        lv_calendar_get_pressed_date(obj, &date);

//        lv_snprintf(buf, sizeof(buf), "%d-%02d-%02d", date.day, date.month, date.year);
        sprintf(buf, "%d-%02d-%02d", date.year, date.month, date.day);
        lv_textarea_set_text(ta, buf);
		g_rtcTime.year = date.year;
		g_rtcTime.month = date.month;
		g_rtcTime.day = date.day;
    }
    lv_obj_del(obj);
}

void EditorDateEventCb(lv_event_t * event)
{
    lv_obj_t * ta = lv_event_get_target(event);

    lv_obj_t *calendar = lv_calendar_create(lv_layer_top());
    lv_obj_set_size(calendar, lv_pct(100), lv_pct(41));
    lv_obj_align(calendar, LV_ALIGN_CENTER, 0, 27);
    lv_obj_add_event_cb(calendar, CalendarEventCb, LV_EVENT_VALUE_CHANGED, ta);

    lv_calendar_set_today_date(calendar, g_rtcTime.year, g_rtcTime.month, g_rtcTime.day);
    lv_calendar_set_showed_date(calendar, g_rtcTime.year, g_rtcTime.month);
    lv_calendar_header_dropdown_create(calendar);
}
static void DateInit(lv_obj_t *parent)
{
    char buffer[32] = {0};
    lv_obj_t *dateLabel = lv_label_create(parent);
    lv_label_set_text(dateLabel, "日期");
    lv_obj_set_style_text_font(dateLabel, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(dateLabel, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align(dateLabel, LV_ALIGN_TOP_LEFT, 0, 0);

    g_dateTa = lv_textarea_create(parent);
    lv_obj_remove_style_all(g_dateTa);
    lv_obj_set_style_bg_opa(g_dateTa, LV_OPA_0, 0);
    lv_obj_set_style_border_width(g_dateTa, 10, 0);

    lv_obj_set_style_text_font(g_dateTa, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(g_dateTa, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_textarea_set_one_line(g_dateTa, true);
    sprintf(buffer, "%d-%02d-%02d", g_rtcTime.year, g_rtcTime.month, g_rtcTime.day);
    lv_textarea_set_text(g_dateTa, buffer);
    lv_obj_align_to(g_dateTa, dateLabel, LV_ALIGN_OUT_RIGHT_MID, 50, 0);
    lv_obj_add_event_cb(g_dateTa, EditorDateEventCb, LV_EVENT_CLICKED, NULL);
}
static void TimeInit(lv_obj_t *parent)
{
    lv_obj_t *timeLabel = lv_label_create(parent);
    lv_label_set_text(timeLabel, "时间");
    lv_obj_set_style_text_font(timeLabel, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(timeLabel, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align(timeLabel,  LV_ALIGN_BOTTOM_LEFT, 0, 0);

    static const char *s_hourOpts = "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n10\n11\n"
                                  "12\n13\n14\n15\n16\n17\n18\n19\n20\n21\n22\n23";
    g_hourDd = lv_dropdown_create(parent);
    lv_obj_remove_style_all(g_hourDd);
    lv_dropdown_set_options_static(g_hourDd, s_hourOpts);
    lv_dropdown_set_selected(g_hourDd, g_rtcTime.hour);
    lv_dropdown_set_dir(g_hourDd, LV_DIR_RIGHT);
    lv_dropdown_set_symbol(g_hourDd, NULL);

    lv_obj_set_style_bg_opa(g_hourDd, LV_OPA_0, 0);
    lv_obj_set_style_text_color(g_hourDd, lv_color_make(0xff, 0xff, 0xff),0);
    lv_obj_set_style_text_font(g_hourDd, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_align(g_hourDd, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_set_style_border_width(g_hourDd, 0, 0);
    lv_obj_align_to(g_hourDd, timeLabel, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

    lv_obj_t *label = lv_label_create(parent);
    lv_label_set_text(label, ":");
    lv_obj_set_style_text_font(label, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(label, lv_color_make(0XFF, 0XFF, 0XFF), 0);
    lv_obj_align_to(label, g_hourDd, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

    static const char *s_minuteOpts = "00\n01\n02\n03\n04\n05\n06\n07\n08\n09\n"
									"10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n"
									"20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n"
									"30\n31\n32\n33\n34\n35\n36\n37\n38\n39\n"
									"40\n41\n42\n43\n44\n45\n46\n47\n48\n49\n"
									"50\n51\n52\n53\n54\n55\n56\n57\n58\n59";

    g_minuteDd = lv_dropdown_create(parent);
    lv_obj_remove_style_all(g_minuteDd);
    lv_dropdown_set_options_static(g_minuteDd, s_minuteOpts);
    lv_dropdown_set_selected(g_minuteDd, g_rtcTime.minute);
    lv_dropdown_set_dir(g_minuteDd, LV_DIR_LEFT);
    lv_dropdown_set_symbol(g_minuteDd, NULL);

    lv_obj_set_style_bg_opa(g_minuteDd, LV_OPA_0, 0);
    lv_obj_set_style_text_color(g_minuteDd, lv_color_make(0xff, 0xff, 0xff),0);
    lv_obj_set_style_text_font(g_minuteDd, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_align(g_minuteDd, LV_TEXT_ALIGN_CENTER, 0);
    lv_obj_set_style_border_width(g_minuteDd, 0, 0);
    lv_obj_align_to(g_minuteDd, label, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

}

static void CreateMainArea(lv_obj_t *parent)
{
    DateInit(parent);
    TimeInit(parent);
}

static void SaveBtnClickedCb(lv_event_t *event)
{
	g_rtcTime.hour = lv_dropdown_get_selected(g_hourDd);
	g_rtcTime.minute = lv_dropdown_get_selected(g_minuteDd);
	SetRtcTime(&g_rtcTime);
}

static void CreateSaveBtn(lv_obj_t *parent)
{
    lv_obj_t *saveBtn = lv_btn_create(parent);
    lv_obj_set_size(saveBtn, 100, 62);
    lv_obj_align(saveBtn, LV_ALIGN_BOTTOM_MID, 0, -30);
    lv_obj_set_style_bg_color(saveBtn, lv_color_make(255, 253, 85), 0);
    lv_obj_add_event_cb(saveBtn, SaveBtnClickedCb, LV_EVENT_CLICKED, 0);

    lv_obj_t *saveLabel = lv_label_create(saveBtn);
    lv_label_set_text(saveLabel, "SAVE");
    lv_obj_set_style_text_font(saveLabel, &lv_font_zihun_26, 0);
    lv_obj_set_style_text_color(saveLabel, lv_color_make(0, 0, 0), 0);
    lv_obj_align(saveLabel, LV_ALIGN_CENTER, 0, 0);
}

void HmiSetTimeInit(void)
{	
	GetRtcTime(&g_rtcTime);
	
    /* 创建TempHumiScr页面 */
    lv_obj_t *setTimeScr = lv_obj_create(lv_scr_act());
    lv_obj_remove_style_all(setTimeScr);
    lv_obj_set_size(setTimeScr, lv_pct(100), lv_pct(100));
    lv_obj_set_style_bg_color(setTimeScr, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_bg_opa(setTimeScr, LV_OPA_100, 0);
    lv_obj_set_style_radius(setTimeScr, 0, 0);
    lv_obj_set_style_border_width(setTimeScr, 0, 0);

    /* 创建TempHumiScr页面Title控件 */
    lv_obj_t *titleCont = lv_obj_create(setTimeScr);

    lv_obj_set_size(titleCont, lv_pct(100), lv_pct(10));
    lv_obj_set_style_bg_color(titleCont, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_radius(titleCont, 0, 0);
    lv_obj_set_style_border_width(titleCont, 0, 0);
    lv_obj_set_flex_flow(titleCont, LV_FLEX_FLOW_ROW);
    lv_obj_set_flex_align(titleCont, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
    lv_obj_set_scroll_dir(titleCont, LV_DIR_NONE);
    CreateTitleArea(titleCont, "设置时间");


    /* 创建TempHumiScr页面Line控件 */
	static lv_point_t linePoints[2];
    linePoints[0].x = 10;
    linePoints[0].y = LV_VER_RES / 10 + 10;  // LV_VER_RES/10对应titleCont的lv_pct(10)
    linePoints[1].x = LV_HOR_RES - 10;
    linePoints[1].y = LV_VER_RES / 10 + 10;
    lv_obj_t *line = lv_line_create(setTimeScr);
    lv_obj_set_style_line_width(line, 1, 0);
    lv_obj_set_style_line_color(line, lv_color_make(0xFF, 0xFF, 0xFF), 0);
    lv_obj_set_style_line_opa(line, LV_OPA_20, 0);
    lv_line_set_points(line, linePoints, 2);

    /* 创建TempHumiScr页面内容主体，obj控件 */
    lv_obj_t *mainCont = lv_obj_create(setTimeScr);
    lv_obj_center(mainCont);
    lv_obj_set_size(mainCont, lv_pct(100), lv_pct(41));
    lv_obj_set_style_bg_color(mainCont, lv_color_make(0, 0, 0), 0);
    lv_obj_set_style_radius(mainCont, 0, 0);
    lv_obj_set_style_border_width(mainCont, 0, 0);
//    lv_obj_set_flex_flow(mainCont, LV_FLEX_FLOW_ROW_WRAP);
//    lv_obj_set_flex_align(mainCont, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_SPACE_EVENLY);
    lv_obj_set_scroll_dir(mainCont, LV_DIR_NONE);
    CreateMainArea(mainCont);


    CreateSaveBtn(setTimeScr);
}