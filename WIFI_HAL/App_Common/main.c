#include <stdint.h>
#include <stdio.h>
#include "systick.h"
#include "delay.h"
#include "usb2com_drv.h"
#include "wifi_drv.h"
#include "wifi_app.h"
#include "sensor_drv.h"
#include "sensor_app.h"
#include "hmi_app.h"
#include "exsram_drv.h"

typedef struct 
{
	uint8_t run;
	uint16_t timCount;
	uint16_t timRLoad;
	void (*pTaskFuncionCb)(void);
} TaskComponts_t;

static TaskComponts_t g_taskCompsTable[] = {
	{0, 1, 1, HmiTask},
	{0, 100, 100, WifiModuleTask},
	{0, 1000, 1000, SensorAppTask},
};

#define MAX_TASK_NUM	sizeof(g_taskCompsTable) / sizeof(g_taskCompsTable[0])

static void TaskHandler(void)
{
	for (uint8_t i = 0; i < MAX_TASK_NUM; i++)
	{
		if (g_taskCompsTable[i].run)
		{
			g_taskCompsTable[i].run = 0;
			g_taskCompsTable[i].pTaskFuncionCb();
		}		
	}
	
}

static void TaskSchduler(void)
{
	for (uint8_t i = 0; i < MAX_TASK_NUM; i++)
	{
		if (g_taskCompsTable[i].timCount)
		{
			g_taskCompsTable[i].timCount--;
			if (g_taskCompsTable[i].timCount == 0)
			{
				g_taskCompsTable[i].run = 1;
				g_taskCompsTable[i].timCount = g_taskCompsTable[i].timRLoad;
			}			
		}		
	}	
}

static void DriverInit(void)
{
	DwtDelayInit();
	SystickInit();
	Usb2ComDrvInit();
	WifiDrvInit();
	SensorDrvInit();
	ExsramDrvInit();
}

static void AppInit(void)
{	
	TaskScheduleCbReg(TaskSchduler);
	HmiInit();
}

int main(void)
{
	DriverInit();
	AppInit();
	DelayNms(1000);
	
	while (1)
	{
		TaskHandler();
	}
}
