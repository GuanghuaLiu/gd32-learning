#include <stdint.h>
#include "canopen_can_drv.h"
#include "canopen_timer_drv.h"
#include "slaver.h"

void CanopenAppInit(void)
{
    CanopenTimerDrvInit();
    CanopenDrvInit(&slaver_Data);

    volatile uint8_t nodeId = 0x02;
    setNodeId(&slaver_Data, nodeId);
    setState(&slaver_Data, Initialisation);
    setState(&slaver_Data, Operational);
}
