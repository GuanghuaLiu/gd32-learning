#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "wifi_drv.h"
#include "systick.h"
#include "delay.h"
#include "sensor_drv.h"
#include "wifi_app.h"

typedef enum
{
    WIFI_COMM_WAIT = 0,
    WIFI_COMM_OK,
    WIFI_COMM_FAIL
} WifiCommState_t;

static WifiCommState_t WifiAtCmdHandler(char *cmd, char *rsp, uint32_t timeoutMs)
{
    static WifiCommState_t s_commState = WIFI_COMM_OK;
    static uint64_t s_sendCmdTime;
    char *recvStrBuffer = NULL;

    if (s_commState != WIFI_COMM_WAIT)
    {
        if (cmd != NULL)
        {
            SendWifiModuleStr(cmd);
        }
        s_commState = WIFI_COMM_WAIT;
        s_sendCmdTime = GetSystemRunTime();
    }
    else
    {
        if (GetSystemRunTime() - s_sendCmdTime < timeoutMs)
        {

            recvStrBuffer = RecvWifiModuleStr();
            if (strstr(recvStrBuffer, rsp) != NULL)
            {
                s_commState = WIFI_COMM_OK;
                ClearRecvStr();
            }
        }
        else
        {
            s_commState = WIFI_COMM_FAIL;
        }
    }
    return s_commState;
}

typedef struct
{
    /* 要发送的AT命令 */
    char *cmd;
    /* 期望的应答数据，默认处理匹配到该字符串认为命令执行成功 */
    char *rsp;
    /* 得到应答的超时时间，达到超时时间为执行失败，单位ms*/
    uint32_t timeoutMs;
} WifiCmdInfo_t;

static WifiCmdInfo_t g_wifiModuleCmdTable[] = {
    {
        .cmd = "AT+RST\r\n",
        .rsp = "OK",
        .timeoutMs = 3000,
    },
    {
        .cmd = NULL,
        .rsp = "XXXXXX",
        .timeoutMs = 1000,
    },
    {
        .cmd = "ATE0\r\n",
        .rsp = "OK",
        .timeoutMs = 1000,
    },
    {
        .cmd = "AT+CWMODE=1\r\n",
        .rsp = "OK",
        .timeoutMs = 1000,
    },
};

typedef enum
{
    AT_RST,
    AT_RST_DELAY,
    AT_E0,
    AT_CWMODE_1,
} AtCmdTableIndex_t;

static WifiCommState_t CheckWifiMuduleWork(void)
{
    WifiCommState_t commState = WIFI_COMM_OK;
    static AtCmdTableIndex_t s_cmdIndex = AT_E0;
    static uint8_t s_retryTimes = 0;

    switch (s_cmdIndex)
    {
    case AT_RST:
        commState = WifiAtCmdHandler(g_wifiModuleCmdTable[AT_RST].cmd, g_wifiModuleCmdTable[AT_RST].rsp, g_wifiModuleCmdTable[AT_RST].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_retryTimes = 0;
            s_cmdIndex = AT_RST_DELAY;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_retryTimes++;
            if (s_retryTimes == 3)
            {
                s_retryTimes = 0;
                s_cmdIndex = AT_RST;
                return WIFI_COMM_FAIL;
            }
                }
        break;

    case AT_RST_DELAY:
        commState = WifiAtCmdHandler(g_wifiModuleCmdTable[AT_RST_DELAY].cmd, g_wifiModuleCmdTable[AT_RST_DELAY].rsp, g_wifiModuleCmdTable[AT_RST_DELAY].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_cmdIndex = AT_E0;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_cmdIndex = AT_E0;
        }
        break;

    case AT_E0:
        commState = WifiAtCmdHandler(g_wifiModuleCmdTable[AT_E0].cmd,
                                     g_wifiModuleCmdTable[AT_E0].rsp,
                                     g_wifiModuleCmdTable[AT_E0].timeoutMs);

        if (commState == WIFI_COMM_OK)
        {
            s_retryTimes = 0;
            s_cmdIndex = AT_CWMODE_1;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_retryTimes++;
            if (s_retryTimes == 3)
            {
                s_retryTimes = 0;
                // s_cmdIndex = AT_RST;
                return WIFI_COMM_FAIL;
            }
        }
        break;

    case AT_CWMODE_1:

        commState = WifiAtCmdHandler(g_wifiModuleCmdTable[AT_CWMODE_1].cmd,
                                     g_wifiModuleCmdTable[AT_CWMODE_1].rsp,
                                     g_wifiModuleCmdTable[AT_CWMODE_1].timeoutMs);

        if (commState == WIFI_COMM_OK)
        {
            s_cmdIndex = AT_RST;
            return WIFI_COMM_OK;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            // s_cmdIndex = AT_RST;
            return WIFI_COMM_FAIL;
        }
        break;
    default:
        break;
    }
    return WIFI_COMM_WAIT;
}

#define DYNAMIC_CONNCECTT_WIFI 1

#if DYNAMIC_CONNCECTT_WIFI == 1
static WifiCmdInfo_t g_checkConnectCmdTable[] = {
    {     
        .cmd = "AT+CWSTATE?\r\n",
        .rsp = "+CWSTATE:2",
        .timeoutMs = 5000,
    },
};
typedef enum
{
    AT_CWSTATE,
} CheckConnectCmdIndex_t;

static WifiCommState_t CheckWifiConnect(void)
{
    WifiCommState_t commState = WIFI_COMM_OK;
    static CheckConnectCmdIndex_t s_connctCmdIndex = AT_CWSTATE;
    static uint8_t s_retryTimes = 0;

    switch (s_connctCmdIndex)
    {
    case AT_CWSTATE:
        commState = WifiAtCmdHandler(g_checkConnectCmdTable[AT_CWSTATE].cmd,
                                     g_checkConnectCmdTable[AT_CWSTATE].rsp,
                                     g_checkConnectCmdTable[AT_CWSTATE].timeoutMs);

        if (commState == WIFI_COMM_OK)
        {
            s_retryTimes = 0;
            return WIFI_COMM_OK;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_retryTimes++;
            if (s_retryTimes == 3)
            {
                s_retryTimes = 0;
                return WIFI_COMM_FAIL;
            }
        }
        break;
    default:
        break;
    }
    return WIFI_COMM_WAIT;
}

#else
static WifiCmdInfo_t g_ConectApCmdTable[] = {
    {
        /**************WiFi名称  密码*********/
        .cmd = "AT+CWJAP=\"%s\",\"%s\"\r\n",
        .rsp = "GOT IP",
        .timeoutMs = 15000,
    },
};

static char g_ssid[20] = "YR&LY";
static char g_pwd[20] = "jingjing.";

typedef enum
{
    AT_CWJAP_SSID_PWD,
} ConectApCmdIndex_t;


static WifiCommState_t CheckWifiConnect(void)
{
    WifiCommState_t commState = WIFI_COMM_OK;
    static ConectApCmdIndex_t s_connctCmdIndex = AT_CWJAP_SSID_PWD;
    char fmtCmd[256];
    static uint8_t retryTimes = 0;

    switch (s_connctCmdIndex)
    {
    case AT_CWJAP_SSID_PWD:

        sprintf(fmtCmd, g_ConectApCmdTable[AT_CWJAP_SSID_PWD].cmd, g_ssid, g_pwd);
        commState = WifiAtCmdHandler(fmtCmd,
                                     g_ConectApCmdTable[AT_CWJAP_SSID_PWD].rsp,
                                     g_ConectApCmdTable[AT_CWJAP_SSID_PWD].timeoutMs);

        if (commState == WIFI_COMM_OK)
        {
            retryTimes = 0;
            return WIFI_COMM_OK;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            retryTimes++;
            if (retryTimes == 3)
            {
                retryTimes = 0;
                return WIFI_COMM_FAIL;
            }
        }
        break;
    default:
        break;
    }
    return WIFI_COMM_WAIT;
}
#endif
/**
 * {"clientId":"k180y91Grfs.board1|securemode=2,signmethod=hmacsha256,timestamp=1714995946755|",
 * "username":"board1&k180y91Grfs",
 * "mqttHostUrl":"iot-06z00bemgdm1488.mqtt.iothub.aliyuncs.com",
 * "passwd":"1c9034475e1f5b597ccf9eae309f7fcac05562417ef151466f7dc3c0fc1aaa1c",
 * "port":1883}
 */

static const char g_clientId[] = "k180y91Grfs.board1|securemode=2\\,signmethod=hmacsha256\\,timestamp=1714995946755|";
static const char g_username[] = "board1&k180y91Grfs";
static const char g_mqttHostUrl[] = "iot-06z00bemgdm1488.mqtt.iothub.aliyuncs.com";
static const char g_passwd[] = "1c9034475e1f5b597ccf9eae309f7fcac05562417ef151466f7dc3c0fc1aaa1c";
// static const uint16_t g_port = 1883;

static WifiCmdInfo_t g_AtMqttConnCmdTable[] = {
    {
        .cmd = "AT+MQTTUSERCFG=0,1,\"%s\",\"%s\",\"%s\",0,0,\"\"\r\n",
        .rsp = "OK",
        .timeoutMs = 300,
    },
    {
        .cmd = "AT+MQTTCONN=0,\"%s\",1883,1\r\n",
        .rsp = "OK",
        .timeoutMs = 2000,

    },
};

typedef enum
{
    AT_MQTTUSERCFG,
    AT_MQTTCONN,
} AtMqttConnCmdIndex_t;

WifiCommState_t ConnMqttServer(void)
{
    WifiCommState_t commState = WIFI_COMM_OK;
    static AtMqttConnCmdIndex_t s_cmdIndex = AT_MQTTUSERCFG;
    static uint8_t retryTimes = 0;
    char cmdBuffer[256];
    switch (s_cmdIndex)
    {
    case AT_MQTTUSERCFG:
        sprintf(cmdBuffer, g_AtMqttConnCmdTable[AT_MQTTUSERCFG].cmd, g_clientId, g_username, g_passwd);
        commState = WifiAtCmdHandler(cmdBuffer,
                                     g_AtMqttConnCmdTable[AT_MQTTUSERCFG].rsp,
                                     g_AtMqttConnCmdTable[AT_MQTTUSERCFG].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_cmdIndex = AT_MQTTCONN;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            return WIFI_COMM_FAIL;
        }

        break;
    case AT_MQTTCONN:
        sprintf(cmdBuffer, g_AtMqttConnCmdTable[AT_MQTTCONN].cmd, g_mqttHostUrl);
        commState = WifiAtCmdHandler(cmdBuffer,
                                     g_AtMqttConnCmdTable[AT_MQTTCONN].rsp,
                                     g_AtMqttConnCmdTable[AT_MQTTCONN].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            retryTimes = 0;
            s_cmdIndex = AT_MQTTUSERCFG;
            return WIFI_COMM_OK;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            retryTimes++;
            if (retryTimes == 3)
            {
                retryTimes = 0;
                s_cmdIndex = AT_MQTTUSERCFG;
                return WIFI_COMM_FAIL;
            }
        }
        break;
    default:
        break;
    }
    return WIFI_COMM_WAIT;
}

static WifiCmdInfo_t g_AtCommMqttCmdTable[] = {
    {
        .cmd = "AT+MQTTPUB=0,\"%s\",\"{\\\"params\\\": {\\\"temperature\\\": %.1f\\, \\\"Humidity\\\": %d}}\",0,0\r\n",
        /**
         *
         * {
                "params": {
                    "Power": 5.0
                }
            }
        */
        .rsp = "OK",
        .timeoutMs = 500,
    },

};

typedef enum
{
    AT_MQTTPUB_SENSOR_TEMP,
} AtCommMqttCmdIndex_t;

static const char g_topic[] = "/sys/k180y91Grfs/board1/thing/event/property/post";

#define MQTT_PUB_PERIOD (uint32_t)30000

static WifiCommState_t CommMqttServer(void)
{
    static WifiCommState_t commState = WIFI_COMM_OK;
    static AtCommMqttCmdIndex_t s_cmdIndex = AT_MQTTPUB_SENSOR_TEMP;
    static uint8_t retryTimes = 0;
    static uint64_t lastPubTime = 0;

    SensorData_t sensorData;
    char cmdBuffer[256];

    switch (s_cmdIndex)
    {
    case AT_MQTTPUB_SENSOR_TEMP:
        if (commState != WIFI_COMM_WAIT)
        {
            if (GetSystemRunTime() - lastPubTime < MQTT_PUB_PERIOD)
            {
                break;
            }
            else
            {
                lastPubTime = GetSystemRunTime();
            }
        }

        GetSensorData(&sensorData);
        sprintf(cmdBuffer, g_AtCommMqttCmdTable[AT_MQTTPUB_SENSOR_TEMP].cmd, g_topic, sensorData.temperature, sensorData.humidity);
        commState = WifiAtCmdHandler(cmdBuffer,
                                     g_AtCommMqttCmdTable[AT_MQTTPUB_SENSOR_TEMP].rsp,
                                     g_AtCommMqttCmdTable[AT_MQTTPUB_SENSOR_TEMP].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            retryTimes = 0;
            s_cmdIndex = AT_MQTTPUB_SENSOR_TEMP;
            return WIFI_COMM_OK;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            retryTimes++;
            if (retryTimes == 3)
            {
                retryTimes = 0;
                s_cmdIndex = AT_MQTTPUB_SENSOR_TEMP;
                return WIFI_COMM_FAIL;
            }
        }
        break;

    default:
        break;
    }
    return WIFI_COMM_WAIT;
}

static WifiCmdInfo_t g_smartCfgWifiCmdTable[] = {
    {
        .cmd = "AT+CWSTARTSMART\r\n",
        .rsp = "ssid",
        .timeoutMs = SMARTCFG_SEND_CMD_TIMEOUT,
    },
    {
        .cmd = NULL,
        .rsp = "GOT IP",
        .timeoutMs = SMARTCFG_CONNECTED_TIMEOUT,
    },
    {
        .cmd = NULL,
        .rsp = "XXXXXX",
        .timeoutMs = SMARTCFG_DELAY_TIMEOUT,
    },
    {
        .cmd = "AT+CWSTOPSMART\r\n",
        .rsp = "OK",
        .timeoutMs = 1000,
    }
};

typedef enum
{
    SMARTCFG_SEND_CMD,
    SMARTCFG_CONNECTED,
    SMARTCFG_DELAY,
    SMARTCFG_STOP,
} AtSmartcfgCmdIndex_t;

static WifiCommState_t SmartCfgWifi(void)
{
    WifiCommState_t commState;
    static AtSmartcfgCmdIndex_t s_smartCfgCmdIndex = SMARTCFG_SEND_CMD;
    static uint8_t s_retryTimes = 0;

    switch (s_smartCfgCmdIndex)
    {
    case SMARTCFG_SEND_CMD:
        commState = WifiAtCmdHandler(g_smartCfgWifiCmdTable[SMARTCFG_SEND_CMD].cmd, g_smartCfgWifiCmdTable[SMARTCFG_SEND_CMD].rsp, g_smartCfgWifiCmdTable[SMARTCFG_SEND_CMD].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_smartCfgCmdIndex = SMARTCFG_CONNECTED;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_smartCfgCmdIndex = SMARTCFG_DELAY;
        }
        break;
    case SMARTCFG_CONNECTED:
        commState = WifiAtCmdHandler(g_smartCfgWifiCmdTable[SMARTCFG_CONNECTED].cmd, g_smartCfgWifiCmdTable[SMARTCFG_CONNECTED].rsp, g_smartCfgWifiCmdTable[SMARTCFG_CONNECTED].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_smartCfgCmdIndex = SMARTCFG_DELAY;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_smartCfgCmdIndex = SMARTCFG_DELAY;
        }
        break;
    case SMARTCFG_DELAY:
        commState = WifiAtCmdHandler(g_smartCfgWifiCmdTable[SMARTCFG_DELAY].cmd, g_smartCfgWifiCmdTable[SMARTCFG_DELAY].rsp, g_smartCfgWifiCmdTable[SMARTCFG_DELAY].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_smartCfgCmdIndex = SMARTCFG_STOP;
        }
        else if (s_smartCfgCmdIndex == WIFI_COMM_FAIL)
        {
            s_smartCfgCmdIndex = SMARTCFG_STOP;
        }
        break;
    case SMARTCFG_STOP:
        commState = WifiAtCmdHandler(g_smartCfgWifiCmdTable[SMARTCFG_STOP].cmd, g_smartCfgWifiCmdTable[SMARTCFG_STOP].rsp, g_smartCfgWifiCmdTable[SMARTCFG_STOP].timeoutMs);
        if (commState == WIFI_COMM_OK)
        {
            s_retryTimes = 0;
            s_smartCfgCmdIndex = SMARTCFG_SEND_CMD;
            return WIFI_COMM_OK;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_retryTimes++;
            if (s_retryTimes == 3)
            {
                s_retryTimes = 0;
                s_smartCfgCmdIndex = SMARTCFG_SEND_CMD;
                return WIFI_COMM_FAIL;
            }
        }
        break;
    default:
        break;
    }
    return WIFI_COMM_WAIT;
}

static bool g_needCfgWifi = false;
void StartSmartConfigure(void)
{
    g_needCfgWifi = true;
}

static WifiConnectState_t g_wifiConnectState = WIFI_CONNECT_ING;
WifiConnectState_t GetWifiConnectState(void)
{
    return g_wifiConnectState;
}

typedef enum
{
    CHECK_WIFI_MODULE,
    CHECK_WIFI_CONNECT,
    CONNECT_MQTT_SERVER,
    COMM_MQTT_SERVER,
    SMARTCONFIG_WIFI,
    HARD_RESET_WIFI_MODULE,
    WIFI_MODULE_ERROR,
} WifiWorkState_t;

void WifiModuleTask(void)
{
    WifiCommState_t commState;
    static WifiWorkState_t s_workState = CHECK_WIFI_MODULE;
    static uint8_t s_hwResetTimes = 0;

    switch (s_workState)
    {
    case CHECK_WIFI_MODULE:
        g_wifiConnectState = WIFI_CONNECT_ING;
        commState = CheckWifiMuduleWork();
        if (WIFI_COMM_OK == commState)
        {
            s_workState = CHECK_WIFI_CONNECT;
        }
        else if (WIFI_COMM_FAIL == commState)
        {
            s_workState = HARD_RESET_WIFI_MODULE;
        }
        break;

    case CHECK_WIFI_CONNECT:
        commState = CheckWifiConnect();
        if (commState != WIFI_COMM_WAIT && g_needCfgWifi)
        {
            s_workState = SMARTCONFIG_WIFI;
            break;
        }

        if (commState == WIFI_COMM_OK)
        {
            s_workState = CONNECT_MQTT_SERVER;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_workState = CHECK_WIFI_CONNECT;
        }
        break;

    case CONNECT_MQTT_SERVER:
        commState = ConnMqttServer();
        if (commState == WIFI_COMM_OK)
        {
            s_workState = COMM_MQTT_SERVER;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_workState = CHECK_WIFI_MODULE;
        }
        break;

    case COMM_MQTT_SERVER:
        commState = CommMqttServer();
        if (commState != WIFI_COMM_WAIT && g_needCfgWifi)
        {
            s_workState = SMARTCONFIG_WIFI;
            break;
        }

        if (commState == WIFI_COMM_OK)
        {
            g_wifiConnectState = WIFI_CONNECT_SUCCESS;
            s_workState = COMM_MQTT_SERVER;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            g_wifiConnectState = WIFI_CONNECT_FAIL;
            s_workState = CHECK_WIFI_MODULE;
        }
        break;

    case SMARTCONFIG_WIFI:
        g_wifiConnectState = WIFI_SMART_CONFIGURE;
        g_needCfgWifi = false;
        commState = SmartCfgWifi();
        if (commState == WIFI_COMM_OK)
        {
            s_workState = CONNECT_MQTT_SERVER;
        }
        else if (commState == WIFI_COMM_FAIL)
        {
            s_workState = CHECK_WIFI_MODULE;
        }
        break;

    case HARD_RESET_WIFI_MODULE:
        if (s_hwResetTimes < 1)
        {
            s_hwResetTimes++;
            HwResetWifiModule();
            DelayNms(1000);
            s_workState = CHECK_WIFI_MODULE;
        }
        else
        {
            printf("Wifi module error!\r\n");
            s_workState = WIFI_MODULE_ERROR;
        }
        break;

    default:
        break;
    }
}
