#ifndef _WIFI_APP_H_
#define _WIFI_APP_H_

#define SMARTCFG_SEND_CMD_TIMEOUT (30000)
#define SMARTCFG_CONNECTED_TIMEOUT (4000)
#define SMARTCFG_DELAY_TIMEOUT (6000)
#define SMARTCFG_TIMEOUT_MAX (SMARTCFG_SEND_CMD_TIMEOUT +  \
                              SMARTCFG_CONNECTED_TIMEOUT + \
                              SMARTCFG_DELAY_TIMEOUT)

typedef enum
{
    WIFI_CONNECT_ING,
    WIFI_CONNECT_SUCCESS,
    WIFI_CONNECT_FAIL,
    WIFI_SMART_CONFIGURE,
} WifiConnectState_t;

WifiConnectState_t GetWifiConnectState(void);
void StartSmartConfigure(void);
void WifiModuleTask(void);

#endif
