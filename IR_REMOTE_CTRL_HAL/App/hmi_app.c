#include <stdint.h>
#include <stdio.h>
#include "ir_drv.h"
#include "led_drv.h"

/**
 * @brief 人机交互功能模块任务函数
 * @param
 */
void HmiTask(void)
{
    uint8_t keyVal;

    if (!GetIrCode(&keyVal))
    {
        return;
    }
    printf("The infrared received code is %#02x.\n", keyVal);
    switch (keyVal)
    {
    case KEY1_CODE:
        TurnOnLed(LED1);
        break;
    case KEY2_CODE:
        TurnOffLed(LED1);
        break;
    case KEY3_CODE:
        TurnOnLed(LED2);
        break;
    case KEY4_CODE:
        TurnOffLed(LED2);
        break;
    case KEY5_CODE:
        TurnOnLed(LED2);
        break;
    case KEY6_CODE:
        TurnOffLed(LED2);
        break;
    case KEY7_CODE:
        TurnOnLed(LED3);
        break;
    case KEY8_CODE:
        TurnOffLed(LED3);
        break;

    default:
        break;
    }
}
