#include <stdio.h>
#include "gd32f30x.h"
#include "led_drv.h"
#include "usb2com_drv.h"

int main(void)
{    
    LedDrvInit();
    Usb2ComDrvInit();

	// Usb2ComTest();
    // printf("Welcome to the Embedded World.\n");

    while (1)
    {
        Usb2ComTask();
    }
}
