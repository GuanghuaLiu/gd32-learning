#ifndef _USB2COM_DRV_H_
#define _USB2COM_DRV_H_

#include <stdint.h>

/**
 * @brief USB转串口硬件初始化
 * @param
 */
void Usb2ComDrvInit(void);

//void Usb2ComTest(void);

/**
 * @brief USB转串口接收任务
 * @param  
 */
void Usb2ComTask(void);

#endif
