#include <stdio.h>
#include <stdbool.h>
#include "gd32f30x.h"
#include "led_drv.h"

typedef struct
{
    uint32_t usart;
    rcu_periph_enum rcuUsart;
    uint32_t gpio;
    rcu_periph_enum rcuGpio;
    uint32_t txPin;
    uint32_t rxPin;
    uint8_t irq;
} Usb2ComHwInfo_t;

Usb2ComHwInfo_t g_Usb2ComInfo = {USART0, RCU_USART0, GPIOA, RCU_GPIOA, GPIO_PIN_9, GPIO_PIN_10, USART0_IRQn};

/**
 * @brief USB转串口GPIO口初始化
 * @param
 */
static void Usb2ComGpioInit(void)
{
    /* 使能GPIO时钟 */
    rcu_periph_clock_enable(g_Usb2ComInfo.rcuGpio);
    /* 配置TX对应管脚为推挽复用输出模式 */
    gpio_init(g_Usb2ComInfo.gpio, GPIO_MODE_AF_PP, GPIO_OSPEED_10MHZ, g_Usb2ComInfo.txPin);
    /* 配置RX对应管脚为浮空输入/上拉输入模式 */
    gpio_init(g_Usb2ComInfo.gpio, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, g_Usb2ComInfo.rxPin);
}

/**
 * @brief USB转串口UART初始化
 * @param baudRate 波特率
 */
static void Usb2ComUartInit(uint32_t baudRate)
{
    /* 使能UART时钟; */
    rcu_periph_clock_enable(g_Usb2ComInfo.rcuUsart);
    /* 复位UART; */
    usart_deinit(g_Usb2ComInfo.usart);
    /* 通过USART_CTLO寄存器的WL设置字长; */
    usart_word_length_set(g_Usb2ComInfo.usart, USART_WL_8BIT);
    /* 通过USART_CTLO寄存器的PCEN设置校验位; */
    usart_parity_config(g_Usb2ComInfo.usart, USART_PM_NONE);
    /* 在USART_CTL1寄存器中写STB[1:0]位来设置停止位的长度; */
    usart_stop_bit_set(g_Usb2ComInfo.usart, USART_STB_1BIT);
    /* 在USART_BAUD寄存器中设置波特率; */
    usart_baudrate_set(g_Usb2ComInfo.usart, baudRate);
    /* 在USART_CTLO寄存器中设置TEN位,使能发送功能; */
    usart_transmit_config(g_Usb2ComInfo.usart, USART_TRANSMIT_ENABLE);
    /* 在USART_CTLO寄存器中设置REN位,使能接收功能; */
    usart_receive_config(g_Usb2ComInfo.usart, USART_RECEIVE_ENABLE);
    /* 使能UART接收中断功能; */
    usart_interrupt_enable(g_Usb2ComInfo.usart, USART_INT_RBNE);
    /* 使能UART中断功能; */
    nvic_irq_enable(g_Usb2ComInfo.irq, 0, 0);
    /* 在USART_CTLO寄存器中置位UEN位,使能UART; */
    usart_enable(g_Usb2ComInfo.usart);
}

/**
 * @brief USB转串口硬件初始化
 * @param
 */
void Usb2ComDrvInit(void)
{
    Usb2ComGpioInit();
    Usb2ComUartInit(115200);
}

/**
 * @brief Usb2Com发送器测试程序
 * @param  
 */
void Usb2ComTest(void)
{
    for (uint8_t i = 0; i < 255; i++)
    {
        /* 向USART_DATA寄存器写数据; */
        usart_data_transmit(g_Usb2ComInfo.usart, i);
        /* 等待TBE置位; */
        while (usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TBE) == RESET);
    }
    /* 等待TC为1,表示全部发送完成; */
    while (usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TC) == RESET);
    /* 关闭发送功能。*/
    usart_transmit_config(g_Usb2ComInfo.usart, USART_TRANSMIT_DISABLE);
}

/**
 * @brief printf函数重定向至串口发送(printf函数默认打印输出到显示器，如果要输出到串口，
 *        必须重新实现fputc函数，将输出指向串口，称为重定向)
 * @param ch ASCII码
 * @param stream 文件输出流
 * @return 无符号字符
 */
int fputc(int ch, FILE *stream)
{
    /* 向USART_DATA寄存器写数据; */
    usart_data_transmit(g_Usb2ComInfo.usart, (uint8_t)ch);
    /* 等待TBE置位; */
    while (usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TBE) == RESET);
    /* 等待TC为1,表示全部发送完成; */
    while (usart_flag_get(g_Usb2ComInfo.usart, USART_FLAG_TC) == RESET);
    /* 关闭发送功能。*/
    usart_transmit_config(g_Usb2ComInfo.usart, USART_TRANSMIT_DISABLE);
    return ch;
}

/**
***********************************************************************
包格式：帧头0   帧头1   数据长度  功能字  LED编号  亮/灭   异或校验数据
        0x55   0xAA    0x03     0x06    0x00    0x01    0xFB
***********************************************************************
*/
#define FRAME_HEAD_0 0x55
#define FRAME_HEAD_1 0xAA
#define CTRL_DATA_LEN 0x03
#define FUNC_WORD_IDX 0x03
#define LED_CTRL_CODE 0x06

#define PACKED_DATA_LEN (CTRL_DATA_LEN + 4)

#define MAX_BUF_SIZE 20
static uint8_t g_uartBuf[MAX_BUF_SIZE];
static bool g_rcvFlag = false;

/**
 * @brief 串口数据处理函数
 * @param data 
 */
static void ProcUartData(uint8_t data)
{
    static uint8_t index = 0;
    g_uartBuf[index++] = data;

    switch (index)
    {
    case 1:
        if (g_uartBuf[0] != FRAME_HEAD_0)
        {
            index = 0;
        }
        break;
    case 2:
        if (g_uartBuf[1] != FRAME_HEAD_1)
        {
            index = 0;
        }
        break;
    case PACKED_DATA_LEN:
        g_rcvFlag = true;
        index = 0;
        break;
    default:
        break;
    }
}

void USART0_IRQHandler(void)
{
    if (usart_interrupt_flag_get(g_Usb2ComInfo.usart, USART_INT_FLAG_RBNE) != RESET)
    {
        usart_interrupt_flag_clear(g_Usb2ComInfo.usart, USART_INT_FLAG_RBNE);

        uint8_t uartData = (uint8_t)usart_data_receive(g_Usb2ComInfo.usart);
        ProcUartData(uartData);
    }
}

/**
* @brief 对数据进行异或运算
* @param pBuf, 存储数组的首地址
* @param len, 要计算的元素的个数
* @return 异或运算结果
 */
static uint8_t CalXorSum(uint8_t *pBuf, uint8_t len)
{
    uint8_t xorSum = 0;
    for (uint8_t i = 0; i < len; i++)
    {
        xorSum ^= pBuf[i];
    }
    return xorSum;
}

typedef struct
{
    uint8_t ledNo;
    uint8_t ledState;
} CtrlLedInfo_t;

/**
* @brief LED控制处理函数
* @param ctrlData 结构体指针，传入LED的编号和状态
 */
static void CtrlLed(CtrlLedInfo_t *ctrlData)
{
    ctrlData->ledState != 0 ? TurnOnLed(ctrlData->ledNo) : TurnOffLed(ctrlData->ledNo);
}

/**
 * @brief USB转串口接收任务处理函数
 * @param
 */
void Usb2ComTask(void)
{
    if (!g_rcvFlag)
    {
        return;
    }
    g_rcvFlag = false;

    if (CalXorSum(g_uartBuf, PACKED_DATA_LEN - 1) != g_uartBuf[PACKED_DATA_LEN - 1])
    {
        return;
    }

    if (g_uartBuf[FUNC_WORD_IDX] == LED_CTRL_CODE)
    {
        CtrlLed((CtrlLedInfo_t *)&g_uartBuf[FUNC_WORD_IDX + 1]);
    }
}
