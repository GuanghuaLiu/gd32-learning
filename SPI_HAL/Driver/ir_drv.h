#ifndef _IR_DRV_H_
#define _IR_DRV_H_

#include <stdbool.h>
#include <stdint.h>

#define KEY1_CODE		0X45
#define KEY2_CODE		0X46
#define KEY3_CODE		0X47
#define KEY4_CODE		0X44
#define KEY5_CODE		0X40
#define KEY6_CODE		0X43
#define KEY7_CODE		0X07
#define KEY8_CODE		0X15
#define KEY9_CODE		0X09
#define KEY10_CODE		0X16
#define KEY11_CODE		0X19
#define KEY12_CODE		0X0D
#define KEY13_CODE		0X0C
#define KEY14_CODE		0X18
#define KEY15_CODE		0X5E
#define KEY16_CODE		0X08
#define KEY17_CODE		0X1C
#define KEY18_CODE		0X5A
#define KEY19_CODE		0X42
#define KEY20_CODE		0X52
#define KEY21_CODE		0X4A



void IrDrvInit(void);
bool GetIrCode(uint8_t *code);

#endif
