#ifndef _EEPROM_DRV_H_
#define _EEPROM_DRV_H_

#include <stdbool.h>
#include <stdint.h>

void EepromDrvInit(void);

void EepromDrvTest(void);

#endif
