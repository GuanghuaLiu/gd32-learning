#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "inflash_drv.h"
#include "rs485_drv.h"
#include "delay.h"
#include "update.h"

/**
 * @brief  Convert an Integer to a string
 * @param  pStr: The string output pointer
 * @param  intNum: The integer to be converted
 * @retval None
 */
void Int2Str(uint8_t *pStr, uint32_t intNum)
{
    uint32_t i, divider = 1000000000, pos = 0, status = 0;

    for (i = 0; i < 10; i++)
    {
        pStr[pos++] = (intNum / divider) + 48;

        intNum = intNum % divider;
        divider /= 10;
        if ((pStr[pos - 1] == '0') & (status == 0))
        {
            pos = 0;
        }
        else
        {
            status++;
        }
    }
}

#define IS_CAP_LETTER(c)            (((c) >= 'A') && ((c) <= 'F'))
#define IS_LC_LETTER(c)             (((c) >= 'a') && ((c) <= 'f'))
#define IS_09(c)                    (((c) >= '0') && ((c) <= '9'))
#define ISVALIDHEX(c)               (IS_CAP_LETTER(c) || IS_LC_LETTER(c) || IS_09(c))

#define ISVALIDDEC(c)               IS_09(c)
#define CONVERTDEC(c)               (c - '0')

#define CONVERTHEX_ALPHA(c)         (IS_CAP_LETTER(c) ? ((c) - 'A' + 10) : ((c) - 'a' + 10))
#define CONVERTHEX(c)               (IS_09(c) ? ((c) - '0') : CONVERTHEX_ALPHA(c))

/**
 * @brief  Convert a string to an integer
 * @param  pInputStr: The string to be converted
 * @param  pIntNum: The integer value
 * @retval 1: Correct
 *         0: Error
 */
uint32_t Str2Int(uint8_t *inputstr, uint32_t *intnum)
{
    uint32_t i = 0, res = 0;
    uint32_t val = 0;

    if (inputstr[0] == '0' && (inputstr[1] == 'x' || inputstr[1] == 'X'))
    {
        if (inputstr[2] == '\0')
        {
            return 0;
        }
        for (i = 2; i < 11; i++)
        {
            if (inputstr[i] == '\0')
            {
                *intnum = val;
                /* return 1; */
                res = 1;
                break;
            }
            if (ISVALIDHEX(inputstr[i]))
            {
                val = (val << 4) + CONVERTHEX(inputstr[i]);
            }
            else
            {
                /* return 0, Invalid input */
                res = 0;
                break;
            }
        }
        /* over 8 digit hex --invalid */
        if (i >= 11)
        {
            res = 0;
        }
    }
    else /* max 10-digit decimal input */
    {
        for (i = 0; i < 11; i++)
        {
            if (inputstr[i] == '\0')
            {
                *intnum = val;
                /* return 1 */
                res = 1;
                break;
            }
            else if ((inputstr[i] == 'k' || inputstr[i] == 'K') && (i > 0))
            {
                val = val << 10;
                *intnum = val;
                res = 1;
                break;
            }
            else if ((inputstr[i] == 'm' || inputstr[i] == 'M') && (i > 0))
            {
                val = val << 20;
                *intnum = val;
                res = 1;
                break;
            }
            else if (ISVALIDDEC(inputstr[i]))
            {
                val = val * 10 + CONVERTDEC(inputstr[i]);
            }
            else
            {
                /* return 0, Invalid input */
                res = 0;
                break;
            }
        }
        /* Over 10 digit decimal --invalid */
        if (i >= 11)
        {
            res = 0;
        }
    }

    return res;
}

uint16_t CalCrc16Ymodem(uint8_t *pArr, uint32_t size)
{
    uint16_t crc = 0;
    while (size--)
    {
        crc ^= (uint16_t)(*pArr++) << 8;

        for (uint16_t i = 0; i < 8; i++)
        {
            if (crc & 0x8000)
            {
                crc <<= 1;
                crc ^= 0x1021;
            }
            else
            {
                crc <<= 1;
            }
        }
    }
    return crc;
}

/* Packet structure defines */
#define PACKET_HEADER_SIZE          ((uint32_t)3)
#define PACKET_DATA_INDEX           ((uint32_t)3)
#define PACKET_START_INDEX          ((uint32_t)0)
#define PACKET_NUMBER_INDEX         ((uint32_t)1)
#define PACKET_CNUMBER_INDEX        ((uint32_t)2)
#define PACKET_TRAILER_SIZE         ((uint32_t)2)
#define PACKET_OVERHEAD_SIZE        (PACKET_HEADER_SIZE + PACKET_TRAILER_SIZE)
#define PACKET_SIZE                 ((uint32_t)128)
#define PACKET_1K_SIZE              ((uint32_t)1024)

/**
 * /-------- Packet in IAP memory ------------------------------------------\
 * | 0      |   1    |  2   |    3    | ... |   n+3   | n+4  | n+5  |
 * |------------------------------------------------------------------------|
 * | start  | number | !num | data[0] | ... | data[n] | CRCH | CRCL |
 * \------------------------------------------------------------------------/
 */
#define SOH                         ((uint8_t)0x01)   /* start of 128-byte data packet */
#define STX                         ((uint8_t)0x02)   /* start of 1024-byte data packet */
#define EOT                         ((uint8_t)0x04)   /* end of transmission */
#define ACK                         ((uint8_t)0x06)   /* acknowledge */
#define NAK                         ((uint8_t)0x15)   /* negative acknowledge */
#define CA                          ((uint8_t)0x18)    /* two of these in succession aborts transfer */
#define CRC16                       ((uint8_t)0x43) /* 'C' == 0x43, request 16-bit CRC */
#define NEGATIVE_BYTE               ((uint8_t)0xFF)

#define ABORT1                      ((uint8_t)0x41) /* 'A' == 0x41, abort by user */
#define ABORT2                      ((uint8_t)0x61) /* 'a' == 0x61, abort by user */

#define NAK_TIMEOUT                 ((uint32_t)0x100000)
#define MAX_ERRORS                  ((uint32_t)5)

#define YMODEM_PACKET_SIZE          (PACKET_1K_SIZE)
static uint8_t g_packetBuffer[YMODEM_PACKET_SIZE]; // 元素个数

#define FILE_NAME_LENGTH            ((uint32_t)256)
#define FILE_SIZE_LENGTH            ((uint32_t)16)
static char g_fileName[FILE_NAME_LENGTH];

typedef enum
{
    STATUS_OK = 0U,
    STATUS_ERROR,
    STATUS_ABORT,
    STATUS_DATA,
    STATUS_LIMIT
} StatusType_t;

/**
 * @brief Receive a packet from sender
 * @param pData
 * @param pLength
 *      0: end of transmission
 *      2: abort by sender
 *     >0: packet length
 * @param timeout
 * @return result of reception/programming
 */
static StatusType_t ReceivePacket(uint8_t *pData, uint32_t *pLength, uint32_t timeout)
{
    uint8_t ch;
    uint32_t packetSize = 0;
    *pLength = 0;

    if (!ReceiveByteTimeout(&ch, timeout))
    {
        return STATUS_ERROR;
    }

    switch (ch)
    {
    case SOH:
        packetSize = PACKET_SIZE;
        break;
    case STX:
        packetSize = PACKET_1K_SIZE;
        break;
    case EOT:
        return STATUS_OK;
    case CA:
        if (ReceiveByteTimeout(&ch, timeout) && (ch == CA))
        {
            *pLength = 2;
            return STATUS_OK;
        }
        else
        {
            return STATUS_ERROR;
        }
    case ABORT1:
    case ABORT2:
        return STATUS_ABORT;
    default:
        return STATUS_ERROR;
    }
    *pData = ch;
    for (uint16_t i = 1; i < (packetSize + PACKET_OVERHEAD_SIZE); i++)
    {
        if (!ReceiveByteTimeout(pData + i, timeout))
        {
            return STATUS_ERROR;
        }
    }
    if ((pData[PACKET_NUMBER_INDEX] | pData[PACKET_CNUMBER_INDEX]) != 0XFF)
    {
        return STATUS_ERROR;
    }
    uint16_t crc = CalCrc16Ymodem(pData + PACKET_DATA_INDEX, packetSize);
    uint16_t rawCrc = (uint16_t)((pData[packetSize + PACKET_OVERHEAD_SIZE - 2] << 8) | pData[packetSize + PACKET_OVERHEAD_SIZE - 1]);
    if (crc != rawCrc)
    {
        return STATUS_ERROR;
    }
    *pLength = packetSize;
    return STATUS_OK;
}

/**
 * @brief Receive a file using the ymodem protocol with CRC16.
 * @param pBuffer The address of the file
 * @param pSize The size of the file
 * @return result of reception/programming
 */
static StatusType_t YmodemReceive(uint8_t *pBuffer, uint32_t *pSize)
{
    bool sessionDone = false, fileDone = false, sessionBegin = false;
    uint8_t packetData[PACKET_1K_SIZE + PACKET_OVERHEAD_SIZE] = {0};
    uint8_t fileSize[FILE_SIZE_LENGTH] = {0};
    uint8_t *pBufferPtr = pBuffer;
    uint8_t *filePtr = NULL;
    uint32_t i = 0, packetLength = 0, packetsReceived = 0, errors = 0;
    StatusType_t result = STATUS_OK;

    /* Initialize flashdestination variable */
    uint32_t flashDestination = APP_ADDR_IN_FLASH;

    while (sessionDone == false && result == STATUS_OK)
    {
        packetsReceived = 0;
        fileDone = false;
        pBufferPtr = pBuffer;
        while (fileDone == false && result == STATUS_OK)
        {
            switch (ReceivePacket(packetData, &packetLength, NAK_TIMEOUT))
            {
            case STATUS_OK:
                errors = 0;
                switch (packetLength)
                {
                case 2:
                    /* Abort by sender */
                    TransmitByte(ACK);
                    return STATUS_ABORT;
                case 0:
                    /* End of Transmission */
                    TransmitByte(ACK);
                    fileDone = true;
                    break;
                default:
                    /* Normal Packet */
                    if (packetData[PACKET_NUMBER_INDEX] != (uint8_t)(packetsReceived & 0xff))
                    {
                        TransmitByte(NAK);
                    }
                    else
                    {
                        if (packetsReceived == 0) // 起始帧、结束帧
                        {
                            /* File name packet */
                            if (packetData[PACKET_DATA_INDEX] != 0) // 起始帧
                            {
                                /* File name extraction */
                                for (i = 0, filePtr = (packetData + PACKET_DATA_INDEX); (i < FILE_NAME_LENGTH) && (*filePtr != 0);)
                                {
                                    g_fileName[i++] = *filePtr++;
                                }
                                g_fileName[i++] = '\0';
                                /* File size extraction */
                                for (i = 0, filePtr++; (i < FILE_SIZE_LENGTH) && (*filePtr != ' ');)
                                {
                                    fileSize[i++] = *filePtr++;
                                }
                                fileSize[i++] = '\0';

                                Str2Int(fileSize, pSize);

                                /* Test the size of the image to be sent */
                                /* Image size is greater than Flash size */
                                if (*pSize > FLASH_APP_SIZE)
                                {
                                    TransmitByte(CA);
                                    TransmitByte(CA);
                                    return STATUS_LIMIT;
                                }

                                EraseInflashForWrite(flashDestination, *pSize);
                                TransmitByte(ACK);
                                TransmitByte(CRC16);
                            }
                            else
                            {
                                TransmitByte(ACK);
                                fileDone = true;
                                sessionDone = true;
                                break;
                            }
                        }
                        else
                        {
                            memcpy(pBufferPtr, packetData + PACKET_DATA_INDEX, packetLength);
                            if (!WriteInflashData(flashDestination, packetLength, pBufferPtr))
                            {
                                TransmitByte(CA);
                                TransmitByte(CA);
                                return STATUS_DATA;
                            }
                            flashDestination += packetLength;
                            TransmitByte(ACK);
                        }
                        packetsReceived++;
                        sessionBegin = true;
                    }
                    break;
                }
                break;

            case STATUS_ABORT:
                TransmitByte(CA);
                TransmitByte(CA);
                return STATUS_ABORT;
            default:
                if (sessionBegin)
                {
                    errors++;
                }
                if (errors > MAX_ERRORS)
                {
                    TransmitByte(CA);
                    TransmitByte(CA);
                    return STATUS_ERROR;
                }
                TransmitByte(CRC16);

                break;
            }
            if (fileDone)
            {
                break;
            }
        }
        if (sessionDone)
        {
            break;
        }
    }
    return result;
}

void UpdateApp(void)
{
    uint8_t sizeBuffer[11] = "";
    uint32_t imageSize = 0;
    StatusType_t result;

    printf("Waiting for the file to be sent ... (press 'a' to abort)\n\r");
    result = YmodemReceive(g_packetBuffer, &imageSize);
    DelayNms(50);

    switch (result)
    {
    case STATUS_OK:
        printf("\n\n\rProgramming Completed Successfully!\n\r\r\n");
        printf("[ Name: %s", g_fileName);
        Int2Str(sizeBuffer, imageSize);
        printf(", imageSize: ");
        printf("%s", sizeBuffer);
        printf(" Bytes]\r\n");
        break;
    case STATUS_LIMIT:
        printf("\n\n\rThe image size is higher than the allowed space memory!\n\r");
        break;
    case STATUS_DATA:
        printf("\n\n\rVerification failed!\n\r");
        break;
    case STATUS_ABORT:
        printf("\r\n\nAborted by user. \n\r");
        break;
    default:
        printf("\n\rFailed to receive the file! \n\r");
        break;
    }
}
