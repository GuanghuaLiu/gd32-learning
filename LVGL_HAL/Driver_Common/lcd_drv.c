#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "gd32f30x.h"
#include "delay.h"
#include "lcd_drv.h"

#define EXMC_TYPE_PSRAM (1)
#define EXMC_TYPE_NORFLASH (0)
#define EXMC_TYPE_NANDFLASH (0)

static void GpioInit(void)
{
    rcu_periph_clock_enable(RCU_GPIOB);
    rcu_periph_clock_enable(RCU_GPIOD);
    rcu_periph_clock_enable(RCU_GPIOE);
    rcu_periph_clock_enable(RCU_GPIOF);
    rcu_periph_clock_enable(RCU_GPIOG);

    /* 通用地址引脚EXMC_A[25:0] */
    gpio_init(GPIOF, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0  | GPIO_PIN_1  | GPIO_PIN_2  | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5); // 依次为A0 ~ A5
    gpio_init(GPIOF, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15);                       // 依次为A6 ~ A9
    gpio_init(GPIOG, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0  | GPIO_PIN_1  | GPIO_PIN_2  | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5); // 依次为A10 ~ A15
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13);                                     // 依次为A16 ~ A18
//    gpio_init(GPIOE, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3  | GPIO_PIN_4  | GPIO_PIN_5  | GPIO_PIN_6 | GPIO_PIN_2);              // 依次为A19 ~ A23
    gpio_init(GPIOG, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_13 | GPIO_PIN_14);                                                   // 依次为A24 ~ A25

    /* 通用数据引脚EXMC_D[15:0] */
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_14 | GPIO_PIN_15 | GPIO_PIN_0 | GPIO_PIN_1);                                                                      // 依次为D0 ~ D3
    gpio_init(GPIOE, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7  | GPIO_PIN_8  | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15); // 依次为D4 ~ D12
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_8  | GPIO_PIN_9  | GPIO_PIN_10);                                                                                    // 依次为D13 ~ D15

/* NOR/PSRAM引脚 */
#if EXMC_TYPE_PSRAM || EXMC_TYPE_NORFLASH
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3);                             // EXMC_CLK引脚
    gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7);                             // EXMC_NADV
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7);                             // EXMC_NE0
    gpio_init(GPIOG, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_12); // EXMC_NE1 ~ NE3
#endif

/* PSRAM引脚 */
#if EXMC_TYPE_PSRAM
    gpio_init(GPIOE, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0 | GPIO_PIN_1); // EXMC_NBL0 ~ NBL1
#endif

/* NOR/PSRAM/NAND引脚 */
#if EXMC_TYPE_PSRAM || EXMC_TYPE_NORFLASH || EXMC_TYPE_NANDFLASH
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_4); // EXMC_NOE
    gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_5); // EXMC_NWE
    // gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_6); // EXMC_NWAIT
#endif
}

static void ExmcInit(void)
{
    rcu_periph_clock_enable(RCU_EXMC);
    exmc_norsram_deinit(EXMC_BANK0_NORSRAM_REGION1);

    exmc_norsram_timing_parameter_struct lcdReadTimingInitStruct;
    lcdReadTimingInitStruct.asyn_access_mode = EXMC_ACCESS_MODE_A;
    lcdReadTimingInitStruct.asyn_address_holdtime = 0;
    lcdReadTimingInitStruct.asyn_address_setuptime = 0;
    lcdReadTimingInitStruct.asyn_data_setuptime = 15;
    lcdReadTimingInitStruct.bus_latency = 0;
    lcdReadTimingInitStruct.syn_clk_division = EXMC_SYN_CLOCK_RATIO_DISABLE;
    lcdReadTimingInitStruct.syn_data_latency = 2;

    exmc_norsram_timing_parameter_struct lcdWriteTimingInitStruct;
    lcdWriteTimingInitStruct.asyn_access_mode = EXMC_ACCESS_MODE_A;
    lcdWriteTimingInitStruct.asyn_address_holdtime = 0;
    lcdWriteTimingInitStruct.asyn_address_setuptime = 0;
    lcdWriteTimingInitStruct.asyn_data_setuptime = 2;
    lcdWriteTimingInitStruct.bus_latency = 0;
    lcdWriteTimingInitStruct.syn_clk_division = EXMC_SYN_CLOCK_RATIO_DISABLE;
    lcdWriteTimingInitStruct.syn_data_latency = 2;

    exmc_norsram_parameter_struct sramInitStruct;
    sramInitStruct.norsram_region = EXMC_BANK0_NORSRAM_REGION1;
    sramInitStruct.write_mode = EXMC_ASYN_WRITE;
    sramInitStruct.extended_mode = ENABLE;
    sramInitStruct.asyn_wait = DISABLE;
    sramInitStruct.nwait_signal = DISABLE;
    sramInitStruct.memory_write = ENABLE;
    sramInitStruct.nwait_config = EXMC_NWAIT_CONFIG_BEFORE;
    sramInitStruct.wrap_burst_mode = DISABLE;
    sramInitStruct.nwait_polarity = EXMC_NWAIT_POLARITY_LOW;
    sramInitStruct.burst_mode = DISABLE;
    sramInitStruct.databus_width = EXMC_NOR_DATABUS_WIDTH_16B;
    sramInitStruct.memory_type = EXMC_MEMORY_TYPE_SRAM;
    sramInitStruct.address_data_mux = DISABLE;
	
    sramInitStruct.read_write_timing = &lcdReadTimingInitStruct;
    sramInitStruct.write_timing = &lcdWriteTimingInitStruct;
	
    exmc_norsram_init(&sramInitStruct);

    exmc_norsram_enable(EXMC_BANK0_NORSRAM_REGION1);
}

#define LCD_ID 					0x9488
#define LCD_PIXEL_WIDTH 		320
#define LCD_PIXEL_HEIGHT 		480
#define LCD_X_CMD 				0x2A
#define LCD_Y_CMD 				0x2B
#define LCD_WG_CMD 				0x2C

#define LcdSendCmd(cmdVal) 		(*(volatile uint16_t *)0X64000000) = cmdVal
#define LcdSendData(dataVal) 	(*(volatile uint16_t *)0X64000002) = dataVal
#define LcdGetData() 			(*(volatile uint16_t *)0X64000002)

#define BACK_LIGHT_ON() 		gpio_bit_set(GPIOA, GPIO_PIN_1)
#define BACK_LIGHT_OFF() 		gpio_bit_reset(GPIOA, GPIO_PIN_1)

static void BacklightInit(void)
{
    rcu_periph_clock_enable(RCU_GPIOA);
    gpio_init(GPIOA, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, GPIO_PIN_1);
}

static void LcdInit(void)
{
    uint16_t lcdId = 0;
    LcdSendCmd(0xd3);
    lcdId = LcdGetData();
    lcdId = LcdGetData();

    lcdId = LcdGetData();
    lcdId <<= 8;
    lcdId |= LcdGetData();

    printf("LCD ID: %#x.\r\n", lcdId);

    if (LCD_ID != lcdId)
    {
        printf("lcd init error!\r\n");
        return;
    }
    LcdSendCmd(0XF7);
    LcdSendData(0xA9);
    LcdSendData(0x51);
    LcdSendData(0x2C);
    LcdSendData(0x82);
    LcdSendCmd(0xC0);
    LcdSendData(0x11);
    LcdSendData(0x09);
    LcdSendCmd(0xC1);
    LcdSendData(0x41);
    LcdSendCmd(0XC5);
    LcdSendData(0x00);
    LcdSendData(0x0A);
    LcdSendData(0x80);
    LcdSendCmd(0xB1);
    LcdSendData(0xB0);
    LcdSendData(0x11);
    LcdSendCmd(0xB4);
    LcdSendData(0x02);
    LcdSendCmd(0xB6);
    LcdSendData(0x02);
    LcdSendData(0x22);
    LcdSendCmd(0xB7);
    LcdSendData(0xc6);
    LcdSendCmd(0xBE);
    LcdSendData(0x00);
    LcdSendData(0x04);
    LcdSendCmd(0xE9);
    LcdSendData(0x00);
    LcdSendCmd(0x36);
    LcdSendData(0x08);
    LcdSendCmd(0x3A);
    LcdSendData(0x55);
    LcdSendCmd(0xE0);
    LcdSendData(0x00);
    LcdSendData(0x07);
    LcdSendData(0x10);
    LcdSendData(0x09);
    LcdSendData(0x17);
    LcdSendData(0x0B);
    LcdSendData(0x41);
    LcdSendData(0x89);
    LcdSendData(0x4B);
    LcdSendData(0x0A);
    LcdSendData(0x0C);
    LcdSendData(0x0E);
    LcdSendData(0x18);
    LcdSendData(0x1B);
    LcdSendData(0x0F);
    LcdSendCmd(0XE1);
    LcdSendData(0x00);
    LcdSendData(0x17);
    LcdSendData(0x1A);
    LcdSendData(0x04);
    LcdSendData(0x0E);
    LcdSendData(0x06);
    LcdSendData(0x2F);
    LcdSendData(0x45);
    LcdSendData(0x43);
    LcdSendData(0x02);
    LcdSendData(0x0A);
    LcdSendData(0x09);
    LcdSendData(0x32);
    LcdSendData(0x36);
    LcdSendData(0x0F);
    LcdSendCmd(0x11);
    // DelayNms(120);
    LcdSendCmd(0x29);
}

void LcdDrvInit(void)
{
    GpioInit();
    ExmcInit();
    BacklightInit();
    LcdInit();
    BACK_LIGHT_ON();
}

static void LcdSetCursor(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey)
{
    LcdSendCmd(LCD_X_CMD);
    LcdSendData((sx & 0xff00) >> 8);
    LcdSendData(sx & 0x00ff);
    LcdSendData((ex & 0xff00) >> 8);
    LcdSendData(ex & 0x00ff);

    LcdSendCmd(LCD_Y_CMD);
    LcdSendData((sy & 0xff00) >> 8);
    LcdSendData(sy & 0x00ff);
    LcdSendData((ey & 0xff00) >> 8);
    LcdSendData(ey & 0x00ff);
}

/**
***********************************************************
* @brief 在指定区域内填充单个颜色
* @param (sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:
         (ex-sx+1)*(ey-sy+1)，color:要填充的颜色
* @return
***********************************************************
*/
void LcdFillPureColor(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t color)
{
    uint32_t pixelPoints = (ex - sx + 1) * (ey - sy + 1);
    LcdSetCursor(sx, sy, ex, ey);
    LcdSendCmd(LCD_WG_CMD);
    for (uint32_t i = 0; i < pixelPoints; i++)
    {
        LcdSendData(color);
    }
}

void LcdClearScreen(uint16_t color)
{
    LcdFillPureColor(0, 0, LCD_PIXEL_WIDTH - 1, LCD_PIXEL_HEIGHT - 1, color);
}

void LcdFillMultiColors(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t *colors)
{
    uint32_t pixelPoints = (ex - sx + 1) * (ey - sy + 1);
    LcdSetCursor(sx, sy, ex, ey);
    LcdSendCmd(LCD_WG_CMD);
    for (uint32_t i = 0; i < pixelPoints; i++)
    {
        LcdSendData(colors[i]);
    }
}

void LcdDrawPoint(uint16_t xPos, uint16_t yPos, uint16_t color)
{
    LcdSetCursor(xPos, yPos, xPos, yPos);
    LcdSendCmd(LCD_WG_CMD);
    LcdSendData(color);
}
