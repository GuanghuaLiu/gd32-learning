#ifndef _HMI_APP_H_
#define _HMI_APP_H_

void HmiInit(void);

/**
 * @brief 人机交互功能模块任务函数
 * @param  
 */
void HmiTask(void);

#endif
