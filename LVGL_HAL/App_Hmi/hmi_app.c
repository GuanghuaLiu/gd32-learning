#include <stdint.h>
#include <stdio.h>

#include "lvgl.h"
#include "lv_port_disp.h"
#include "lv_port_indev.h"
#include "led_drv.h"
#include "hmi_temphumi.h"
#include "hmi_time.h"

#define LED_NUM 		3
#define LED_STATE_OFF	0
#define LED_STATE_ON 	1
static uint8_t g_ledState[LED_NUM] = {LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF};

static void LedClickedEventCb(lv_event_t *event)
{
    lv_obj_t *ledCtrlBtn = lv_event_get_target(event);
    uint8_t *ledState = (uint8_t *)lv_event_get_user_data(event);

    uint8_t ledIndex = ledState - g_ledState;

    if (*ledState == LED_STATE_OFF)
    {        
        lv_obj_set_style_bg_color(ledCtrlBtn, lv_color_make(251, 83, 216), 0);
		*ledState = LED_STATE_ON;
		//TurnOnLed(ledIndex);
    }
    else
    {        
        lv_obj_set_style_bg_color(ledCtrlBtn, lv_color_make(239, 253, 120), 0);
		*ledState = LED_STATE_OFF;
		//TurnOffLed(ledIndex);
    }
    ToggleLed(ledIndex);
}

void HmiLedCtrlInit(void)
{
    // lv_obj_set_style_bg_color(lv_scr_act(), lv_color_make(0,0,0), 0);

    lv_obj_t *cont = lv_obj_create(lv_scr_act());
    
    lv_obj_set_size(cont, LV_HOR_RES, LV_VER_RES);
	lv_obj_set_style_bg_color(cont, lv_color_make(0,0,0), 0);
    lv_obj_set_style_radius(cont, 0, 0);
    lv_obj_set_style_border_width(cont, 0, 0);

    lv_obj_set_flex_flow(cont, LV_FLEX_FLOW_COLUMN_REVERSE);
    lv_obj_set_flex_align(cont, LV_FLEX_ALIGN_SPACE_EVENLY, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);

    static lv_style_t style;
    lv_style_init(&style);
	
    lv_style_set_width(&style, lv_pct(62));
    lv_style_set_height(&style, lv_pct(25));
    lv_style_set_radius(&style, 8);
    lv_style_set_bg_color(&style, lv_color_make(239, 253, 120));
    lv_style_set_bg_opa(&style, LV_OPA_100);
    lv_style_set_border_width(&style, 0);

    lv_obj_t *ledCtrlBtn;
    for (uint8_t i = 0; i < LED_NUM; i++)
    {
        ledCtrlBtn = lv_btn_create(cont);
        lv_obj_remove_style_all(ledCtrlBtn);
        lv_obj_add_style(ledCtrlBtn, &style, 0);
        //lv_obj_align(ledCtrlBtn, LV_ALIGN_BOTTOM_MID, 0, - 40 - 146 * i);

        lv_obj_add_event_cb(ledCtrlBtn, LedClickedEventCb, LV_EVENT_CLICKED, &g_ledState[i]);
        TurnOffLed(i);
    }
}

void HmiInit(void)
{
    lv_init();

    lv_port_disp_init();
    lv_port_indev_init();

    // HmiLedCtrlInit();
	// TempHumiScrInit();
	HmiSetTimeInit();
}

/**
***********************************************************
* @brief 人机交互任务处理函数
* @param
* @return
***********************************************************
*/
void HmiTask(void)
{
    lv_task_handler();
}
