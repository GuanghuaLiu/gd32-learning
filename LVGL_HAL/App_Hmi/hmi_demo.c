#include "lvgl.h"

void HmiDemo(void)
{
    lv_obj_t * btn1 = lv_btn_create(lv_scr_act());
    lv_obj_set_size(btn1, 100, 100);
    lv_obj_set_pos(btn1, 110, 190);

    lv_obj_t * btn2 = lv_btn_create(btn1);
    lv_obj_align(btn2, LV_ALIGN_RIGHT_MID, 20,0);
    // lv_obj_set_align(btn2, LV_ALIGN_TOP_RIGHT);
    lv_obj_set_style_bg_color(btn2, lv_color_make(0xff, 0, 0), 0);
    lv_obj_set_style_bg_opa(btn2, LV_OPA_60, 0);
    lv_obj_set_style_bg_color(btn2, lv_color_make(0, 0xff, 0), LV_STATE_PRESSED);

    lv_obj_t *btn3 = lv_btn_create(lv_scr_act());
    lv_obj_align_to(btn3, btn2, LV_ALIGN_OUT_RIGHT_MID, 0, 0);

    lv_obj_remove_local_style_prop(btn2, LV_STYLE_BG_OPA, 0);
    // lv_obj_remove_style_all(btn3);

    static lv_style_t btnStyle;
    lv_style_init(&btnStyle);
    lv_style_set_bg_color(&btnStyle, lv_color_make(0,0xff,0));

    lv_obj_add_style(btn3, &btnStyle, LV_STATE_PRESSED);
}

