#include <stdint.h>
#include "mb.h"
#include "modbus_slave.h"

static ModbusFuncCb_t g_modbusFuncCb;

eMBErrorCode eMBRegHoldingCB(UCHAR *pucRegBuffer, USHORT usAddress,
							 USHORT usNRegs, eMBRegisterMode eMode)
{
	eMBErrorCode eState;

	if (eMode == MB_REG_WRITE)
	{
		eState = g_modbusFuncCb.WriteHoldingRegisters(pucRegBuffer, usAddress, usNRegs);
	}
	else
	{
		eState = g_modbusFuncCb.ReadHoldingRegisters(pucRegBuffer, usAddress, usNRegs);
	}

	return eState;
}

void ModbusSlaveInit(ModbusSlaveInstance_t *modbusSlaveInstance)
{
	eMBInit(MB_RTU, modbusSlaveInstance->slaveAddr, 0, modbusSlaveInstance->baudRate, MB_PAR_NONE);
	g_modbusFuncCb = modbusSlaveInstance->funcCb;
	eMBEnable();
}
