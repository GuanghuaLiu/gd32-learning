#include <stdlib.h>
#include "sensor_drv.h"
#include "ntc_drv.h"
#include "rh_drv.h"

/**
 * @brief 获取传感器数据
 * @param sensorData 输出，传感器数据回写地址
 */
void GetSensorData(SensorData_t *sensorData)
{
	if (sensorData == NULL)
	{
		return;
	}
	sensorData->temperature = GetTempData();
	sensorData->humidity = GetHumiData();
}

/**
 * @brief 传感器驱动初始化
 * @param  
 */
void SensorDrvInit(void)
{
	TempDrvInit();
	HumiDrvInit();
}

/**
 * @brief 触发驱动转换传感器数据
 * @param  
 */
void SensorDrvProc(void)
{
	TempSensorProc();
	HumiSensorProc((uint8_t)GetTempData());
}
