/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include "gd32f30x.h"
/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"

/* ----------------------- static functions ---------------------------------*/
static void prvvTIMERExpiredISR(void);

/* ----------------------- Start implementation -----------------------------*/
typedef struct
{
	uint8_t irq;
	rcu_periph_enum rcuTimer;
	uint32_t timer;

} TimerInfo_t;

static TimerInfo_t g_timerInfo = {TIMER3_IRQn, RCU_TIMER3, TIMER3};

static void TimerInit(uint16_t usTim1Timerout50us)
{
	rcu_periph_clock_enable(g_timerInfo.rcuTimer);
	timer_deinit(g_timerInfo.timer);

	timer_parameter_struct timerStruct;
	timer_struct_para_init(&timerStruct);
	timerStruct.prescaler = 6000 - 1;
	timerStruct.alignedmode = TIMER_COUNTER_EDGE;
	timerStruct.counterdirection = TIMER_COUNTER_UP;
	timerStruct.period = usTim1Timerout50us - 1;
	timer_init(g_timerInfo.timer, &timerStruct);
	timer_interrupt_enable(g_timerInfo.timer, TIMER_INT_UP);
	nvic_irq_enable(g_timerInfo.irq, 0, 0);
}

BOOL xMBPortTimersInit(USHORT usTim1Timerout50us)
{
	TimerInit(usTim1Timerout50us);
	return TRUE;
}

inline void
vMBPortTimersEnable()
{
	/* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
	timer_counter_value_config(g_timerInfo.timer, 0);
	timer_interrupt_flag_clear(g_timerInfo.timer, TIMER_INT_FLAG_UP);
	timer_interrupt_enable(g_timerInfo.timer, TIMER_INT_UP);
	timer_enable(g_timerInfo.timer);
}

inline void
vMBPortTimersDisable()
{
	/* Disable any pending timers. */
	timer_counter_value_config(g_timerInfo.timer, 0);
	timer_interrupt_flag_clear(g_timerInfo.timer, TIMER_INT_FLAG_UP);
	timer_interrupt_disable(g_timerInfo.timer, TIMER_INT_UP);
	timer_disable(g_timerInfo.timer);
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
static void prvvTIMERExpiredISR(void)
{
	(void)pxMBPortCBTimerExpired();
}

void TIMER3_IRQHandler(void)
{
	if (timer_interrupt_flag_get(g_timerInfo.timer, TIMER_INT_FLAG_UP))
	{
		timer_interrupt_flag_clear(g_timerInfo.timer, TIMER_INT_FLAG_UP);
		timer_counter_value_config(g_timerInfo.timer, 0);
		prvvTIMERExpiredISR();
	}
}

