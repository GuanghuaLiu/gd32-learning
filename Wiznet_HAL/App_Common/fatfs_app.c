#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gd32f30x.h"
#include "ff.h"
#include "rtc_drv.h"
#include "sensor_drv.h"

#define FATFS_PATH_STR "0:"
#define FATFS_FILE_PATH_STR "0:�¶ȴ�����.txt"

static FATFS g_fatfs;
static bool g_fatfsInitFlag;
static FIL g_sensorFile;

void FatFsInit(void)
{
    uint8_t workBuffer[FF_MAX_SS];
    FRESULT result = f_mount(&g_fatfs, FATFS_PATH_STR, 1);

    if (result == FR_NO_FILESYSTEM)
    {
        if (f_mkfs(FATFS_PATH_STR, NULL, workBuffer, FF_MAX_SS) == FR_OK)
        {
            /* ��ʽ������ȡ������ */
            f_mount(NULL, FATFS_PATH_STR, 1);
            /* ���¹��� */
            result = f_mount(&g_fatfs, FATFS_PATH_STR, 1);
        }

    }
    if (result != FR_OK)
    {
        printf("��ʼ��ʧ�ܣ������룺%d.\n", result);
        g_fatfsInitFlag = false;
        return;
    }
    g_fatfsInitFlag = true;
}

void FatfsTask(void)
{
    if (!g_fatfsInitFlag)
    {
        return;
    }

    FRESULT result;
    result = f_open(&g_sensorFile, FATFS_FILE_PATH_STR, FA_WRITE | FA_OPEN_APPEND);

    if (result != FR_OK)
    {
        printf("��%s�ļ�ʧ�ܣ������룺%d.\n", FATFS_FILE_PATH_STR, result);
        return;
    }
    RtcTime_t rtcTime;
    GetRtcTime(&rtcTime);
    SensorData_t sensorData;
    GetSensorData(&sensorData);
    
    if (f_printf(&g_sensorFile, "%d-%d-%d %02d:%02d:%02d, the temperature is %.1f ��, and the humidity is %d%%.\n", 
    rtcTime.year, rtcTime.month, rtcTime.day, rtcTime.hour, rtcTime.minute, rtcTime.second, sensorData.temperature, sensorData.humidity) < 0)
    {
        printf("д����ʧ�ܣ�����");
    }
    
    result = f_close(&g_sensorFile);
    if (result != FR_OK)
    {
        printf("�ر��ļ�ʧ�ܣ������룺%d.\n", result);
        return;
    }
}

void PrintFileData(void)
{
    if (!g_fatfsInitFlag)
    {
        return;
    }
    FRESULT result = f_open(&g_sensorFile, FATFS_FILE_PATH_STR, FA_OPEN_EXISTING | FA_READ);
    if (result != FR_OK)
    {
        printf("��%s�ļ�ʧ�ܣ������룺%d.\n", FATFS_FILE_PATH_STR, result);
        return;
    }

    uint8_t readBuffer[100] = "\0";
    while (NULL != f_gets((TCHAR *)readBuffer, sizeof(readBuffer) / sizeof(readBuffer[0]), &g_sensorFile))
    {
        printf("%s", readBuffer);
        memset(readBuffer, '\0', sizeof(readBuffer));
    }

    result = f_close(&g_sensorFile);
    if (result != FR_OK)
    {
        printf("�ر��ļ�ʧ�ܣ������룺%d.\n", result);
        return;
    }
    result = f_unlink(FATFS_FILE_PATH_STR);
    if (result != FR_OK)
    {
        printf("�ر��ļ�ʧ�ܣ������룺%d.\n", result);
        return;
    }
}