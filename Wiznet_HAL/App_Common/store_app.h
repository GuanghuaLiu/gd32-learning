#ifndef _STORE_APP_H_
#define _STORE_APP_H_

#include <stdint.h>
#include <stdbool.h>

/* 保存至EEPROM，置位 */
#define PARAM_SAVED_IN_EEPROM		(0)
/* 保存至NORFLASH，置位 */
#define PARAM_SAVED_IN_NORFLASH		(0)
/* 保存至INFLASH，置位 */
#define PARAM_SAVED_IN_INFLASH		(1)

void InitSysParam(void);
bool SetModbusParam(uint8_t addr);

#endif
