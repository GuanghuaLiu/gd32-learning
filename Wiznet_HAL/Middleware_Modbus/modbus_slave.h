#ifndef _MODBUS_SLAVE_H_
#define _MODBUS_SLAVE_H_

#include <stdint.h>
#include "mb.h"

typedef struct
{
	eMBErrorCode (*WriteHoldingRegisters)(uint8_t *pBuffer, uint16_t startAddr, uint16_t numReg);
	eMBErrorCode (*ReadHoldingRegisters)(uint8_t *pBuffer, uint16_t startAddr, uint16_t numReg);
} ModbusFuncCb_t;

typedef struct
{
	uint8_t slaveAddr;
	uint32_t baudRate;
	ModbusFuncCb_t funcCb;
} ModbusSlaveInstance_t;

void ModbusSlaveInit(ModbusSlaveInstance_t *modbusSlaveInstance);

#endif
