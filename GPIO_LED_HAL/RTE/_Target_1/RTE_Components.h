
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'GPIO_LED_REG' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "gd32f30x.h"

/* GigaDevice::Device:GD32F30x_StdPeripherals:GPIO@2.2.0 */
#define RTE_DEVICE_STDPERIPHERALS_GPIO
/* GigaDevice::Device:GD32F30x_StdPeripherals:MISC@2.2.0 */
#define RTE_DEVICE_STDPERIPHERALS_MISC
/* GigaDevice::Device:GD32F30x_StdPeripherals:PMU@2.2.0 */
#define RTE_DEVICE_STDPERIPHERALS_PMU
/* GigaDevice::Device:GD32F30x_StdPeripherals:RCU@2.2.0 */
#define RTE_DEVICE_STDPERIPHERALS_RCU


#endif /* RTE_COMPONENTS_H */
