#include <stdint.h>
#include "gd32f30x.h"
#include "delay.h"

int main(void)
{
    // 初始化DWT延时
    InitDwtDelay();
    // 时钟树，开启PA口时钟树
    rcu_periph_clock_enable(RCU_GPIOA);
    // 端口控制寄存器，打开PA8，设置为推挽输出，最大速率2MHz
    // gpio_deinit(GPIOA);
    gpio_init(GPIOA, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, GPIO_PIN_8);

    while (1)
    {
        // 端口输出控制寄存器, 输出高电平
        // GPIOA_OCTL |= 0x01 << 8;
        gpio_bit_set(GPIOA, GPIO_PIN_8);
        DelayNms(5000);
        // 端口输出控制寄存器, 输出低电平
        // GPIOA_OCTL &= ~(0x01 << 8);
        gpio_bit_reset(GPIOA, GPIO_PIN_8);
        DelayNms(5000);
    }
}
