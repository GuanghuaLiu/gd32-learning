#include <stdint.h>
#include "gd32f30x.h"
#include "led_drv.h"

static void TimerInit(uint32_t periodUs)
{
    /* 使能定时器的时钟 */
    rcu_periph_clock_enable(RCU_TIMER0);
    /* 复位定时器 */
    timer_deinit(TIMER0);

    /* 初始化定时器参数, 设置自动重装值, 分频系数, 计数方式等 */
    timer_parameter_struct paramInitStruct;
    timer_struct_para_init(&paramInitStruct);
    paramInitStruct.prescaler = 60000 - 1;
    paramInitStruct.alignedmode = TIMER_COUNTER_EDGE;
    paramInitStruct.counterdirection = TIMER_COUNTER_UP;
    paramInitStruct.period = periodUs - 1;
    timer_init(TIMER0, &paramInitStruct);

    /* 使能定时器更新中断 */
    timer_interrupt_enable(TIMER0, TIMER_INT_UP);
    /* 使能定时器中断和优先级 */
    nvic_irq_enable(TIMER0_UP_IRQn, 0, 0);
    /* 使能定时器 */
    timer_enable(TIMER0);
}

void TimingDrvInit(void)
{
    TimerInit(1000);
}

void TIMER0_UP_IRQHandler(void)
{
    if (timer_interrupt_flag_get(TIMER0, TIMER_INT_FLAG_UP) == SET)
    {
        timer_interrupt_flag_clear(TIMER0, TIMER_INT_FLAG_UP);
        ToggleLed(LED1);
    }
}
