#include "queue.h"

/**
 * @brief 环形队列初始化
 * @param queue 队列变量地址
 * @param pBuf 队列缓冲区地址
 * @param size 队列缓冲区大小
 */
void QueueInit(QueueType_t *queue, uint8_t *pBuf, uint32_t size)
{
    queue->pBuffer = pBuf;
    queue->size = size;
    queue->head = 0;
    queue->tail = 0;
}

/**
 * @brief 压入数据到队列中
 * @param queue 队列变量指针
 * @param data 待压入队列的数据
 * @return 压入是否成功
 */
QueueState_t QueuePush(QueueType_t *queue, uint8_t data)
{
    uint32_t index = (queue->tail + 1) % queue->size;

    if (index == queue->head)
    {
        return QUEUE_OVERLOAD;
    }

    queue->pBuffer[queue->tail] = data;
    queue->tail = index;
    return QUEUE_OK;
}

/**
 * @brief 从队列中弹出数据
 * @param queue 队列变量指针
 * @param pdata 待弹出队列的数据缓存地址
 * @return 弹出队列是否成功
 */
QueueState_t QueuePop(QueueType_t *queue, uint8_t *pdata)
{
    if (queue->head == queue->tail)
    {
        return QUEUE_EMPTY;
    }

    *pdata = queue->pBuffer[queue->head];
    queue->head = (queue->head + 1) % queue->size;
    return QUEUE_OK;
}

/**
 * @brief 向队列中压入一组数据
 * @param queue 队列变量指针
 * @param pArr 待压入队列的数组地址
 * @param len 带压入队列的元素个数
 * @return 实际压入到队列的元素个数
 */
uint32_t QueuePushArray(QueueType_t *queue, uint8_t *pArr, uint8_t len)
{
    uint32_t i = 0;
    for (i = 0; i < len; i++)
    {
        if (QueuePush(queue, pArr[i]) == QUEUE_OVERLOAD)
        {
            break;
        }        
    }
    return i;
}

/**
 * @brief 从队列中弹出一组数据
 * @param queue 队列变量指针
 * @param pArr 待弹出的数组地址
 * @param len 带弹出的元素个数
 * @return 实际从队列弹出的元素个数
 */
uint32_t QueuePopArray(QueueType_t *queue, uint8_t *pArr, uint8_t len)
{
    uint32_t i = 0;
    for (i = 0; i < len; i++)
    {
        if (QueuePop(queue, &pArr[i]) == QUEUE_EMPTY)
        {
            break;
        }        
    }
    return i;
}

/**
 * @brief 队列中的数据个数
 * @param queue 队列变量指针
 * @return 实际队列的元素个数
 */
uint32_t QueueCount(QueueType_t *queue)
{
    if (queue->head <= queue->tail)
    {
        return queue->tail - queue->head;
    }
    return queue->size + queue->tail - queue->head;
}
