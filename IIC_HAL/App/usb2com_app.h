#ifndef _USB2COM_APP_H_
#define _USB2COM_APP_H_

#include <stdint.h>

/**
 * @brief USB转串口接收任务处理函数
 * @param
 */
void Usb2ComTask(void);

/**
 * @brief Usb转串口应用层初始化函数
 * @param
 */
void Usb2ComAppInit(void);

#endif
