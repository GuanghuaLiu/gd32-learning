#include <stdint.h>
#include "gd32f30x.h"
#include "led_drv.h"
#include "delay.h"

typedef struct
{
    rcu_periph_enum rcuGpio;
    uint32_t gpio;
    uint32_t gpioPin;

    rcu_periph_enum rcuTimer;
    uint32_t timer;
    uint16_t timerCh;
    uint8_t irq;
} PwmDrvInfo_t;

static PwmDrvInfo_t g_pwmDrvInfo = {RCU_GPIOA, GPIOA, GPIO_PIN_8, RCU_TIMER0, TIMER0, TIMER_CH_0, TIMER0_Channel_IRQn};

static void PwmGpioInit(void)
{
    rcu_periph_clock_enable(g_pwmDrvInfo.rcuGpio);
    gpio_init(g_pwmDrvInfo.gpio, GPIO_MODE_AF_PP, GPIO_OSPEED_10MHZ, g_pwmDrvInfo.gpioPin);
}
static void PwmTimerInit(void)
{
    rcu_periph_clock_enable(g_pwmDrvInfo.rcuTimer);
    timer_deinit(g_pwmDrvInfo.timer);

    timer_parameter_struct paramInitStruct;
    timer_struct_para_init(&paramInitStruct);
    paramInitStruct.prescaler = 120 - 1;
    paramInitStruct.period = 500 - 1;
    timer_init(g_pwmDrvInfo.timer, &paramInitStruct);

    timer_oc_parameter_struct ocParamInitStruct;
    timer_channel_output_struct_para_init(&ocParamInitStruct);
    ocParamInitStruct.outputstate = TIMER_CCX_ENABLE;
    ocParamInitStruct.ocpolarity = TIMER_OC_POLARITY_HIGH;
    timer_channel_output_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, &ocParamInitStruct);

    timer_channel_output_mode_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, TIMER_OC_MODE_PWM0);
    timer_channel_output_pulse_value_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, 300 - 1);
    timer_primary_output_config(g_pwmDrvInfo.timer, ENABLE);

    // timer_interrupt_enable(g_pwmDrvInfo.timer, TIMER_INT_CH0);   //输出比较不需要中断
    // nvic_irq_enable(g_pwmDrvInfo.irq, 0, 0);
    timer_enable(g_pwmDrvInfo.timer);
}

void PwmDrvInit(void)
{
    PwmGpioInit();
    PwmTimerInit();
}

void PwmDrvTest(void)
{
    for (uint32_t i = 0; i < 250; i += 10)
    {
        timer_channel_output_pulse_value_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, i);
        ToggleLed(LED1);
		DelayNms(50);
    }

    for (uint32_t i = 250; i > 0; i -= 10)
    {
        timer_channel_output_pulse_value_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, i);
        ToggleLed(LED1);
		DelayNms(50);
    }
}

// void TIMER0_Channel_IRQHandler(void)
// {
//     if (timer_interrupt_flag_get(g_pwmDrvInfo.timer, TIMER_INT_FLAG_CH0))
//     {
//         timer_interrupt_flag_clear(g_pwmDrvInfo.timer, TIMER_INT_FLAG_CH0);

//         for (uint32_t i = 0; i < 500; i += 10)
//         {
//             timer_channel_output_pulse_value_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, i);
//             ToggleLed(LED1);
//         }

//         for (uint32_t i = 500; i > 0; i -= 10)
//         {
//             timer_channel_output_pulse_value_config(g_pwmDrvInfo.timer, g_pwmDrvInfo.timerCh, i);
//             ToggleLed(LED1);
//         }

//     }
// }
