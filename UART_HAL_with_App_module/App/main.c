#include "led_drv.h"
#include "usb2com_drv.h"
#include "usb2com_app.h"
#include "delay.h"

static void DriverInit(void)
{
    LedDrvInit();
    Usb2ComDrvInit();
	DwtDelayInit();
}

static void AppInit(void)
{
    Usb2ComAppInit();
}

int main(void)
{ 
	DriverInit();
	AppInit();

    while (1)
    {
        Usb2ComTask();
		DelayNms(500);
    }
}
