#include <stdbool.h>
#include <stdint.h>
#include "led_drv.h"
#include "usb2com_drv.h"

/**
***********************************************************************
包格式：帧头0   帧头1   数据长度  功能字  LED编号  亮/灭   异或校验数据
        0x55   0xAA    0x03     0x06    0x00    0x01    0xFB
***********************************************************************
*/
#define FRAME_HEAD_0 0x55   //帧头0
#define FRAME_HEAD_1 0xAA   //帧头1
#define CTRL_DATA_LEN 0x03  //数据长度
#define FUNC_WORD_IDX 0x03  //功能字下标（索引标号）
#define LED_CTRL_CODE 0x06  //功能字

#define PACKET_DATA_LEN (CTRL_DATA_LEN + 4)     // 数据包长度

#define MAX_BUF_SIZE 20
static uint8_t g_uartBuf[MAX_BUF_SIZE];
static bool g_rcvFlag = false;

/**
 * @brief 串口数据处理函数
 * @param data
 */
static void ProcUartData(uint8_t data)
{
    static uint8_t index = 0;   // 变量的状态需要考虑
    g_uartBuf[index++] = data;

    switch (index)
    {
    case 1:
        if (g_uartBuf[0] != FRAME_HEAD_0)
        {
            index = 0;
        }
        break;
    case 2:
        if (g_uartBuf[1] != FRAME_HEAD_1)
        {
            index = 0;
        }
        break;
    case PACKET_DATA_LEN:
        g_rcvFlag = true;
        index = 0;
        break;
    default:
        break;
    }
}

/**
 * @brief 对数据进行异或运算
 * @param pBuf, 存储数组的首地址
 * @param len, 要计算的元素的个数
 * @return 异或运算结果
 */
static uint8_t CalXorSum(uint8_t *pBuf, uint8_t len)
{
    uint8_t xorSum = 0;
    for (uint8_t i = 0; i < len; i++)
    {
        xorSum ^= pBuf[i];
    }
    return xorSum;
}

typedef struct
{
    uint8_t ledNo;
    uint8_t ledState;
} CtrlLedInfo_t;

/**
 * @brief LED控制处理函数
 * @param ctrlData 结构体指针，传入LED的编号和状态
 */
static void CtrlLed(CtrlLedInfo_t *ctrlData)
{
    ctrlData->ledState != 0 ? TurnOnLed(ctrlData->ledNo) : TurnOffLed(ctrlData->ledNo);
}

/**
 * @brief USB转串口接收任务处理函数
 * @param
 */
void Usb2ComTask(void)
{
    if (!g_rcvFlag)
    {
        return;
    }
    g_rcvFlag = false;

    if (CalXorSum(g_uartBuf, PACKET_DATA_LEN - 1) != g_uartBuf[PACKET_DATA_LEN - 1])
    {
        return;
    }

    if (g_uartBuf[FUNC_WORD_IDX] == LED_CTRL_CODE)
    {
        CtrlLed((CtrlLedInfo_t *)&g_uartBuf[FUNC_WORD_IDX + 1]);
    }
}

/**
 * @brief Usb转串口应用层初始化函数
 * @param
 */
void Usb2ComAppInit(void)
{
    RegUsb2ComCb(ProcUartData);
}
