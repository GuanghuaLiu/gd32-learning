#ifndef _RH_DRV_H_
#define _RH_DRV_H_

#include <stdint.h>

void HumiDrvInit(void);

/**
 * @brief 触发驱动转换温度传感器数据
 * @param  
 */
void HumiSensorProc(uint8_t temp);

/**
 * @brief 获取温度数据
 * @param  
 * @return 温度数据，浮点数
 */
uint8_t GetHumiData(void);

#endif
