#ifndef _DELAY_H_
#define _DELAY_H_

#include <stdint.h>

/**
    @brief 初始化DWT延时
    @param
*/
void DwtDelayInit(void);

/**
    @brief 微秒级延时
    @param nUs N微秒，最大延时时间：(2^32/内核主频)*10^6 us
*/
void DelayNus(uint32_t nUs);

/**
    @brief 毫秒级延时
    @param nMs 延时N毫秒
*/
void DelayNms(uint32_t nMs);

#endif
