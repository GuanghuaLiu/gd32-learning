#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <stdint.h>

typedef struct
{
	uint32_t head;
	uint32_t tail;
	uint32_t size;
	uint8_t *pBuffer;
} QueueType_t;

typedef enum
{
	QUEUE_OK = 0, // 队列正常
	QUEUE_ERROR, // 队列错误
	QUEUE_OVERLOAD, // 队列已满
	QUEUE_EMPTY // 队列已空
} QueueState_t;

/**
    @brief 环形队列初始化
    @param queue 队列变量地址
    @param pBuf 队列缓冲区地址
    @param size 队列缓冲区大小
*/
void QueueInit(QueueType_t *queue, uint8_t *pBuf, uint32_t size);

/**
    @brief 压入数据到队列中
    @param queue 队列变量指针
    @param data 待压入队列的数据
    @return 压入是否成功
*/
QueueState_t QueuePush(QueueType_t *queue, uint8_t data);

/**
    @brief 从队列中弹出数据
    @param queue 队列变量指针
    @param pData 待弹出队列的数据缓存地址
    @return 弹出队列是否成功
*/
QueueState_t QueuePop(QueueType_t *queue, uint8_t *pdata);

/**
    @brief 向队列中压入一组数据
    @param queue 队列变量指针
    @param pArr 待压入队列的数组地址
    @param len 带压入队列的元素个数
    @return 实际压入到队列的元素个数
*/
uint32_t QueuePushArray(QueueType_t *queue, uint8_t *pArr, uint8_t len);

/**
    @brief 从队列中弹出一组数据
    @param queue 队列变量指针
    @param pArr 待压入弹出的数组地址
    @param len 带压入弹出的元素个数
    @return 实际从队列弹出的元素个数
*/
uint32_t QueuePopArray(QueueType_t *queue, uint8_t *pArr, uint8_t len);

/**
    @brief 队列中的数据个数
    @param queue 队列变量指针
    @return 实际队列的元素个数
*/
uint32_t QueueCount(QueueType_t *queue);

#endif
