#ifndef _SENSOR_DRV_H_
#define _SENSOR_DRV_H_

#include <stdint.h>

typedef struct
{
	uint8_t humidity;
	float temperature;
} SensorData_t;

/**
    @brief 获取传感器数据
    @param sensorData 输出，传感器数据回写地址
*/
void GetSensorData(SensorData_t *sensorData);

/**
    @brief 传感器驱动初始化
    @param
*/
void SensorDrvInit(void);

/**
    @brief 触发驱动转换传感器数据
    @param
*/
void SensorDrvProc(void);

#endif
