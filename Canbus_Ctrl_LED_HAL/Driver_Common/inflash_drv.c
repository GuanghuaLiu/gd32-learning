#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "gd32f30x.h"

#define INFLASH_PAGE_SIZE 2048
#define INFLASH_END_ADDR 0X0807FFFF

/**
    @brief 指定地址开始读出指定个数的数据
    @param readAdder 读取数据的地址
    @param len 读取数据的个数
    @param pBuffer 指定数据保存的地址
    @return bool 是否读取成功
*/
bool ReadInflashData(uint32_t readAddr, uint32_t len, uint8_t *pBuffer)
{
	if(readAddr + len > INFLASH_END_ADDR)
	{
		return false;
	}

	uint32_t addr = readAddr;
	for(uint32_t i = 0; i < len; i++)
	{
		pBuffer[i] = *(uint8_t *)addr;
		addr++;
		//pBuffer++;
	}
	return true;
}

/**
    @brief 擦除从eraseAddr开始到eraseAddr + len所对应的页
    @param eraseAddr 所需擦除页的地址
    @param len 数据的个数
    @return bool 是否擦除成功
*/
bool EraseInflashForWrite(uint32_t eraseAddr, uint32_t len)
{
	if(len == 0 || eraseAddr + len > INFLASH_END_ADDR)
	{
		return false;
	}

	uint8_t pageNum = 0;
	uint32_t addrOffset = eraseAddr % INFLASH_PAGE_SIZE;

	fmc_state_enum state = FMC_READY;
	fmc_unlock();

	if(len > INFLASH_PAGE_SIZE - addrOffset)
	{

		fmc_flag_clear(FMC_FLAG_BANK0_PGERR | FMC_FLAG_BANK0_WPERR | FMC_FLAG_BANK0_END);
		state = fmc_page_erase(eraseAddr);
		if(state != FMC_READY)
		{
			goto erase_err;
		}

		eraseAddr += INFLASH_PAGE_SIZE - addrOffset;
		len -= INFLASH_PAGE_SIZE - addrOffset;

		pageNum = len / INFLASH_PAGE_SIZE;

		while(pageNum--)
		{
			fmc_flag_clear(FMC_FLAG_BANK0_PGERR | FMC_FLAG_BANK0_WPERR | FMC_FLAG_BANK0_END);
			state = fmc_page_erase(eraseAddr);
			if(state != FMC_READY)
			{
				goto erase_err;
			}
			eraseAddr += INFLASH_PAGE_SIZE;
			len -= INFLASH_PAGE_SIZE;
		}
		if(len % INFLASH_PAGE_SIZE)
		{
			fmc_flag_clear(FMC_FLAG_BANK0_PGERR | FMC_FLAG_BANK0_WPERR | FMC_FLAG_BANK0_END);
			state = fmc_page_erase(eraseAddr);
			if(state != FMC_READY)
			{
				goto erase_err;
			}
		}
	}
	else
	{
		fmc_flag_clear(FMC_FLAG_BANK0_PGERR | FMC_FLAG_BANK0_WPERR | FMC_FLAG_BANK0_END);
		state = fmc_page_erase(eraseAddr);
		if(state != FMC_READY)
		{
			goto erase_err;
		}
	}
	fmc_lock();
	return true;

erase_err:
	fmc_lock();
	return false;
}

/**
    @brief 指定地址开始写入指定个数的数据，调用前需要擦除flash
    @param writeAddr 写入地址
    @param pBuffer 数组首地址
    @param len 要写入的数据个数
    @return bool 是否写入成功
*/
bool WriteInflashData(uint32_t writeAddr, uint32_t len, uint8_t *pBuffer)
{
	if(pBuffer == NULL || writeAddr + len > INFLASH_END_ADDR || len == 0)
	{
		return false;
	}
	if(writeAddr % 2)
	{
		return false;
	}

	fmc_state_enum fmcState = FMC_READY;

	fmc_unlock();

	for(uint16_t i = 0; i < len / 2; i++)
	{
		fmc_flag_clear(FMC_FLAG_BANK0_PGERR | FMC_FLAG_BANK0_WPERR | FMC_FLAG_BANK0_END);
		fmcState = fmc_halfword_program(writeAddr, *(uint16_t *)pBuffer);
		if(fmcState != FMC_READY)
		{
			goto write_err;
		}
		writeAddr += 2;
		pBuffer += 2;
	}
	if(len % 2)
	{
		fmc_flag_clear(FMC_FLAG_BANK0_PGERR | FMC_FLAG_BANK0_WPERR | FMC_FLAG_BANK0_END);
		fmcState = fmc_halfword_program(writeAddr, *(uint16_t *)pBuffer | 0xff00);
		if(fmcState != FMC_READY)
		{
			goto write_err;
		}
	}
	fmc_lock();
	return true;

write_err:
	fmc_lock();
	return false;
}

#define INFLASH_BUF_SIZE 2049
#define INFLASH_ADDR 0X0807F000

void InflashTest(void)
{
	uint8_t writeBuf[INFLASH_BUF_SIZE];
	uint8_t readBuf[INFLASH_BUF_SIZE];

	for(uint16_t i = 0; i < INFLASH_BUF_SIZE; i++)
	{
		writeBuf[i] = i;
	}
	for(uint16_t i = 0; i < INFLASH_BUF_SIZE; i++)
	{
		printf("0x%02x ", writeBuf[i]);
	}

	printf("\n开始写入...\n");
	if(!EraseInflashForWrite(INFLASH_ADDR, INFLASH_BUF_SIZE))
	{
		printf("\nInflash擦除失败，请排查错误\n");
	}
	if(!WriteInflashData(INFLASH_ADDR, INFLASH_BUF_SIZE, writeBuf))
	{
		printf("\nInflash写操作失败，请排查错误\n");
	}

	printf("\n开始读取...\n");
	if(!ReadInflashData(INFLASH_ADDR, INFLASH_BUF_SIZE, readBuf))
	{
		printf("\nInflash读操作失败，请排查错误\n");
	}

	for(uint16_t i = 0; i < INFLASH_BUF_SIZE; i++)
	{
		if(readBuf[i] != writeBuf[i])
		{
			printf("0x%02X ", readBuf[i]);
			printf("Flash测试故障，请排查！\n");
			return;
		}
		printf("0x%02X ", readBuf[i]);

	}
	printf("\nFlash测试通过！\n");

}
