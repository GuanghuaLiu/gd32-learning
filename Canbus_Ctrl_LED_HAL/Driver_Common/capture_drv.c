#include <stdint.h>
#include <stdio.h>
#include "gd32f30x.h"
#include "led_drv.h"
#include "delay.h"

typedef struct
{
	rcu_periph_enum rcuGpio;
	uint32_t gpio;
	uint32_t gpioPin;

	rcu_periph_enum rcuTimer;
	uint32_t timer;
	uint16_t timerCh;
	uint8_t irq;
} CaptureDrvInfo_t;

static CaptureDrvInfo_t g_captureDrvInfo = {RCU_GPIOA, GPIOA, GPIO_PIN_0, RCU_TIMER1, TIMER1, TIMER_CH_0, TIMER1_IRQn};

static void CaptureGpioInit(void)
{
	rcu_periph_clock_enable(g_captureDrvInfo.rcuGpio);
	gpio_init(g_captureDrvInfo.gpio, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, g_captureDrvInfo.gpioPin);
}
static void CaptureTimerInit(void)
{
	rcu_periph_clock_enable(g_captureDrvInfo.rcuTimer);
	timer_deinit(g_captureDrvInfo.timer);

	timer_parameter_struct paramInitStruct;
	timer_struct_para_init(&paramInitStruct);
	paramInitStruct.prescaler = 120 - 1;
	paramInitStruct.period = 500 - 1;
	timer_init(g_captureDrvInfo.timer, &paramInitStruct);

	timer_ic_parameter_struct icParamInitStruct;
	timer_channel_input_struct_para_init(&icParamInitStruct);
	timer_input_capture_config(g_captureDrvInfo.timer, g_captureDrvInfo.timerCh, &icParamInitStruct);

	timer_interrupt_enable(g_captureDrvInfo.timer, TIMER_INT_CH0); //输出比较不需要中断
	nvic_irq_enable(g_captureDrvInfo.irq, 0, 0);
	timer_enable(g_captureDrvInfo.timer);
}

void CaptureDrvInit(void)
{
	CaptureGpioInit();
	CaptureTimerInit();
}

static uint16_t g_capVal;

void TIMER1_IRQHandler(void)
{
	if(timer_interrupt_flag_get(g_captureDrvInfo.timer, TIMER_INT_FLAG_CH0))
	{
		timer_interrupt_flag_clear(g_captureDrvInfo.timer, TIMER_INT_FLAG_CH0);

		g_capVal = timer_channel_capture_value_register_read(g_captureDrvInfo.timer, g_captureDrvInfo.timerCh);

		timer_counter_value_config(g_captureDrvInfo.timer, 0);
	}
}
void CaptureDrvTest(void)
{
	printf("The period is %d us.\n", g_capVal + 1);
}
