#include <stdint.h>
#include "gd32f30x.h"

typedef struct
{
	rcu_periph_enum rcu;
	uint32_t gpio;
	uint32_t pin;
} Led_GPIO_t;

static Led_GPIO_t g_ledList[] =
{
	{RCU_GPIOA, GPIOA, GPIO_PIN_8},
	{RCU_GPIOE, GPIOE, GPIO_PIN_6},
	{RCU_GPIOF, GPIOF, GPIO_PIN_6}
};

#define LED_NUM_MAX (sizeof(g_ledList) / sizeof(g_ledList[0]))

/**
    @brief LED硬件初始化
    @param
*/
void LedDrvInit(void)
{
	for(uint8_t i = 0; i < LED_NUM_MAX; i++)
	{
		rcu_periph_clock_enable(g_ledList[i].rcu);
		gpio_init(g_ledList[i].gpio, GPIO_MODE_OUT_PP, GPIO_OSPEED_2MHZ, g_ledList[i].pin);
		gpio_bit_reset(g_ledList[i].gpio, g_ledList[i].pin);
	}
}

/**
    @brief 点亮LED
    @param ledNo LED标号
*/
void TurnOnLed(uint8_t ledNo)
{
	if(ledNo > LED_NUM_MAX)
	{
		return;
	}

	gpio_bit_set(g_ledList[ledNo].gpio, g_ledList[ledNo].pin);
}

/**
    @brief 熄灭LED
    @param ledNo LED标号
*/
void TurnOffLed(uint8_t ledNo)
{
	if(ledNo > LED_NUM_MAX)
	{
		return;
	}

	gpio_bit_reset(g_ledList[ledNo].gpio, g_ledList[ledNo].pin);
}

/**
    @brief LED状态反转
    @param ledNo LED标号
*/
void ToggleLed(uint8_t ledNo)
{
	if(ledNo > LED_NUM_MAX)
	{
		return;
	}

	FlagStatus bitState = gpio_input_bit_get(g_ledList[ledNo].gpio, g_ledList[ledNo].pin);
	// bitState = !bitState;
	// gpio_bit_write(GPIOA, GPIO_PIN_8, (FlagStatus)(1 - bitState));
	gpio_bit_write(g_ledList[ledNo].gpio, g_ledList[ledNo].pin, (FlagStatus)!bitState);
}
