#ifndef _NTC_DRV_H_
#define _NTC_DRV_H_

void TempDrvInit(void);

/**
    @brief 触发驱动转换温度传感器数据
    @param
*/
void TempSensorProc(void);

/**
    @brief 获取温度数据
    @param
    @return 温度数据，浮点数
*/
float GetTempData(void);

#endif
