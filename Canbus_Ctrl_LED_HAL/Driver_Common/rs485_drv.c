#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "gd32f30x.h"

typedef struct
{
	rcu_periph_enum rcuGpio;
	rcu_periph_enum rcuUart;
	rcu_periph_enum rcuFnGpio;
	uint32_t gpio;
	uint32_t rxPin;
	uint32_t txPin;
	uint32_t uart;
	uint32_t fnGpio;
	uint32_t fnPin;
} UartHwInfo_t;

static UartHwInfo_t g_uartHwInfo = {RCU_GPIOA, RCU_USART1, RCU_GPIOC, GPIOA, GPIO_PIN_3, GPIO_PIN_2, USART1, GPIOC, GPIO_PIN_5};

#define SWITCH_RS485_TO_TX() gpio_bit_set(g_uartHwInfo.fnGpio, g_uartHwInfo.fnPin)
#define SWITCH_RS485_TO_RX() gpio_bit_reset(g_uartHwInfo.fnGpio, g_uartHwInfo.fnPin)

static void GpioInit(void)
{
	rcu_periph_clock_enable(g_uartHwInfo.rcuGpio);
	gpio_init(g_uartHwInfo.gpio, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, g_uartHwInfo.rxPin);
	gpio_init(g_uartHwInfo.gpio, GPIO_MODE_AF_PP, GPIO_OSPEED_10MHZ, g_uartHwInfo.txPin);
}

static void UartInit(uint32_t baudRate)
{
	rcu_periph_clock_enable(g_uartHwInfo.rcuUart);

	usart_deinit(g_uartHwInfo.uart);

	usart_word_length_set(g_uartHwInfo.uart, USART_WL_8BIT);

	usart_parity_config(g_uartHwInfo.uart, USART_PM_NONE);

	usart_stop_bit_set(g_uartHwInfo.uart, USART_STB_1BIT);

	usart_baudrate_set(g_uartHwInfo.uart, baudRate);

	usart_transmit_config(g_uartHwInfo.uart, USART_TRANSMIT_ENABLE);

	usart_receive_config(g_uartHwInfo.uart, USART_RECEIVE_ENABLE);

	usart_enable(g_uartHwInfo.uart);
}

static void SwitchInit(void)
{
	rcu_periph_clock_enable(g_uartHwInfo.rcuFnGpio);
	gpio_init(g_uartHwInfo.fnGpio, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, g_uartHwInfo.fnPin);
}

void RS485DrvInit(void)
{
	GpioInit();
	UartInit(9600);
	SwitchInit();
}

static bool ReceiveByte(uint8_t *byte)
{
	if(RESET != usart_flag_get(g_uartHwInfo.uart, USART_FLAG_RBNE))
	{
		*byte = (uint8_t)usart_data_receive(g_uartHwInfo.uart);
		return true;
	}
	return false;
}

bool ReceiveByteTimeout(uint8_t *byte, uint32_t timeout)
{
	while(timeout-- > 0)
	{
		if(ReceiveByte(byte))
		{
			return true;
		}
	}
	return false;
}

bool GetKeyPressed(uint8_t *key)
{
	return ReceiveByte(key);
}

void TransmitByte(uint8_t byte)
{
	SWITCH_RS485_TO_TX();
	usart_data_transmit(g_uartHwInfo.uart, (uint8_t)byte);
	while(usart_flag_get(g_uartHwInfo.uart, USART_FLAG_TC) == RESET);
	SWITCH_RS485_TO_RX();
}

int fputc(int c, FILE *stream)
{
	SWITCH_RS485_TO_TX();
	usart_data_transmit(g_uartHwInfo.uart, (uint8_t)c);
	while(usart_flag_get(g_uartHwInfo.uart, USART_FLAG_TC) == RESET);
	SWITCH_RS485_TO_RX();
	return c;
}
