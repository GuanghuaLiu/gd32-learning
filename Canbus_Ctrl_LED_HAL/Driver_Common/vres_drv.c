#include <stdint.h>
#include <stdio.h>
#include "gd32f30x.h"
#include "delay.h"

static void GpioInit(void)
{
	rcu_periph_clock_enable(RCU_GPIOC);
	gpio_init(GPIOC, GPIO_MODE_AIN, GPIO_OSPEED_10MHZ, GPIO_PIN_2);
}

static void AdcInit(void)
{
	/* 使能ADC时钟 */
	rcu_periph_clock_enable(RCU_ADC0);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV6);
	/* 1. 确保ADC_CTL0寄存器的DISRC和SM位以及ADC_CTL1寄存器的CTN位为0；*/
	adc_deinit(ADC0);
	/* 2. 用模拟通道编号来配置RSQ0，配置 分辨率、独立模式、连续模式、数据对齐方式、通道个数；*/
	adc_resolution_config(ADC0, ADC_RESOLUTION_12B);
	adc_mode_config(ADC_MODE_FREE);
	adc_special_function_config(ADC0, ADC_CONTINUOUS_MODE, ENABLE);
	adc_data_alignment_config(ADC0, ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC0, ADC_REGULAR_CHANNEL, 1);
	/* 3. 配置ADC_SAMPTx寄存器；*/
	adc_regular_channel_config(ADC0, 0, ADC_CHANNEL_12, ADC_SAMPLETIME_239POINT5);
	/* 4. 如果有需要，可以配置ADC_CTL1寄存器的ETERC和ETSRC位；*/
	adc_external_trigger_source_config(ADC0, ADC_REGULAR_CHANNEL, ADC0_1_2_EXTTRIG_REGULAR_NONE);
	adc_external_trigger_config(ADC0, ADC_REGULAR_CHANNEL, ENABLE);

	adc_oversample_mode_config(ADC0, ADC_OVERSAMPLING_ALL_CONVERT, ADC_OVERSAMPLING_SHIFT_5B, ADC_OVERSAMPLING_RATIO_MUL32);
	adc_oversample_mode_enable(ADC0);
	/* 时间校准 */
	/* 1. 确保ADCON=1；*/
	adc_enable(ADC0);
	/* 2. 延迟14个CK_ADC以等待ADC稳定；*/
	DelayNus(20);
	/* 3. 设置RSTCLB (可选的)；*/
	/* 4. 设置CLB=1；*/
	/* 5. 等待直到CLB=0。*/
	adc_calibration_enable(ADC0);

	/* 5. 设置SWRCST位，或者为常规序列产生一个外部触发信号；*/
	adc_software_trigger_enable(ADC0, ADC_REGULAR_CHANNEL);
}

void VresDrvInit(void)
{
	GpioInit();
	AdcInit();
}

uint16_t GetAdcVal(void)
{
	/* 6. 等到EOC置1；*/
	while(!adc_flag_get(ADC0, ADC_FLAG_EOC))
		;
	/* 7. 从ADC_RDATA寄存器中读ADC转换结果；*/
	uint16_t adcVal = adc_regular_data_read(ADC0);
	/* 8. 写0清除EOC标志位。*/
	adc_flag_clear(ADC0, ADC_FLAG_EOC);
	return adcVal;
}

void VresDrvTest(void)
{
	uint16_t adcVal = GetAdcVal();
	float voltage = (float)adcVal / 4095 * 3.3f;
	printf("adcVal = %d, voltage = %.1f V.\n", adcVal, voltage);
}
