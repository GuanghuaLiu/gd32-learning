#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include "gd32f30x.h"
#include "delay.h"

#define GET_I2C_SDA() gpio_input_bit_get(GPIOB, GPIO_PIN_7)
#define SET_I2C_SCL() gpio_bit_set(GPIOB, GPIO_PIN_6)
#define CLEAR_I2C_SCL() gpio_bit_reset(GPIOB, GPIO_PIN_6)
#define SET_I2C_SDA() gpio_bit_set(GPIOB, GPIO_PIN_7)
#define CLEAR_I2C_SDA() gpio_bit_reset(GPIOB, GPIO_PIN_7)

static void GpioInit(void)
{
	rcu_periph_clock_enable(RCU_GPIOB);

	gpio_init(GPIOB, GPIO_MODE_OUT_OD, GPIO_OSPEED_10MHZ, GPIO_PIN_6 | GPIO_PIN_7);
}

void EepromDrvInit(void)
{
	GpioInit();
}

static void I2CStart(void)
{
	SET_I2C_SDA();
	SET_I2C_SCL();
	DelayNus(4);
	CLEAR_I2C_SDA();
	DelayNus(4);
	CLEAR_I2C_SCL();
	DelayNus(4);
}

static void I2CStop(void)
{
	CLEAR_I2C_SDA();
	DelayNus(4);
	SET_I2C_SCL();
	DelayNus(4);
	SET_I2C_SDA();
}

static bool I2CWaitAck(void)
{
	SET_I2C_SDA();
	DelayNus(4);
	SET_I2C_SCL();
	DelayNus(4);

	while(GET_I2C_SDA())
	{
		static uint8_t errTimes = 0;
		errTimes++;
		if(errTimes > 200)
		{
			errTimes = 0;
			I2CStop();
			return false;
		}
	}
	CLEAR_I2C_SCL();
	DelayNus(4);

	return true;
}

static void I2CAck(void)
{
	CLEAR_I2C_SDA();
	DelayNus(4);
	SET_I2C_SCL();
	DelayNus(4);
	CLEAR_I2C_SCL();
	DelayNus(4);

	SET_I2C_SDA();
}

static void I2CNack(void)
{
	SET_I2C_SDA();
	DelayNus(4);
	SET_I2C_SCL();
	DelayNus(4);
	CLEAR_I2C_SCL();
	DelayNus(4);

	// SET_I2C_SDA();
}

static void I2CWriteByte(uint8_t byte)
{
	for(uint8_t i = 0; i < 8; i++)
	{
		if(byte & 0x80)
		{
			SET_I2C_SDA();
		}
		else
		{
			CLEAR_I2C_SDA();
		}
		byte <<= 1;

		DelayNus(4);
		SET_I2C_SCL();
		DelayNus(4);
		CLEAR_I2C_SCL();
		DelayNus(4);
	}
}

static uint8_t I2CReadByte(void)
{
	uint8_t byte = 0;
	SET_I2C_SDA();
	// DelayNus(4);
	for(uint8_t i = 0; i < 8; i++)
	{
		SET_I2C_SCL();
		DelayNus(4);

		byte <<= 1;
		byte |= (uint8_t)GET_I2C_SDA();

		CLEAR_I2C_SCL();
		DelayNus(4);
	}
	return byte;
}

#define EEPROM_DEV_ADDR 0xA0 // 24xx02的设备地址
#define EEPROM_PAGE_SIZE 8	 // 24xx02的页面大小
#define EEPROM_SIZE 256		 // 24xx02总容量
#define EEPROM_I2C_WR 0		 // 写控制bit
#define EEPROM_I2C_RD 1		 // 读控制bit

bool WriteEepromData(uint8_t writeAddr, uint8_t *pBuffer, uint16_t numToWrite)
{
	if(pBuffer == NULL || (writeAddr + numToWrite) > EEPROM_SIZE)
	{
		return false;
	}

	uint8_t dataAddr = writeAddr;
	uint8_t j = 0;
	for(uint16_t i = 0; i < numToWrite; i++)
	{
		if(i == 0 || (dataAddr & (EEPROM_PAGE_SIZE - 1)) == 0)
		{
			I2CStop();
			for(j = 0; j < 100; j++)
			{
				I2CStart();
				I2CWriteByte(EEPROM_DEV_ADDR | EEPROM_I2C_WR);
				if(I2CWaitAck())
				{
					break;
				}
			}
			if(j == 100)
			{
				goto i2c_err;
			}

			I2CWriteByte(dataAddr);
			if(!I2CWaitAck())
			{
				goto i2c_err;
			}
		}
		I2CWriteByte(pBuffer[i]);
		if(!I2CWaitAck())
		{
			goto i2c_err;
		}
		dataAddr++;
	}
	I2CStop();
	return true;

i2c_err:
	I2CStop();
	return false;
}

bool ReadEepromData(uint8_t readAddr, uint8_t *pBuffer, uint16_t numToRead)
{
	if(pBuffer == NULL || readAddr + numToRead > EEPROM_SIZE)
	{
		return false;
	}

	I2CStart();
	I2CWriteByte(EEPROM_DEV_ADDR | EEPROM_I2C_WR);
	if(!I2CWaitAck())
	{
		goto i2c_err;
	}

	I2CWriteByte(readAddr);
	if(!I2CWaitAck())
	{
		goto i2c_err;
	}

	I2CStart();
	I2CWriteByte(EEPROM_DEV_ADDR | EEPROM_I2C_RD);
	if(!I2CWaitAck())
	{
		goto i2c_err;
	}

	for(uint16_t i = numToRead; i > 1; i--)
	{
		*pBuffer++ = I2CReadByte();
		I2CAck();
	}
	*pBuffer = I2CReadByte();
	I2CNack();

	I2CStop();
	return true;
i2c_err:
	I2CStop();
	return false;
}

#define BUFFER_SIZE 256
void EepromDrvTest(void)
{
	uint8_t bufferWrite[BUFFER_SIZE];
	uint8_t bufferRead[BUFFER_SIZE];

	printf("AT24C02 writing data：\n");
	for(uint16_t i = 0; i < BUFFER_SIZE; i++)
	{
		bufferWrite[i] = i + 1;
		printf("0x%02X ", bufferWrite[i]);
	}
	printf("\n开始写入\n");

	if(!WriteEepromData(0, bufferWrite, BUFFER_SIZE))
	{
		printf("AT24C02写数据故障，请排查！\n");
		return;
	}

	printf("AT24C02 reading...\n");
	if(!ReadEepromData(0, bufferRead, BUFFER_SIZE))
	{
		printf("AT24C02读数据故障，请排查！\n");
		return;
	}
	for(uint16_t i = 0; i < BUFFER_SIZE; i++)
	{
		if(bufferRead[i] != bufferWrite[i])
		{
			printf("0x%02X ", bufferRead[i]);
			printf("AT24C02测试故障，请排查！\n");
			return;
		}
		printf("0x%02X ", bufferRead[i]);
	}
	printf("\n24C02测试通过！\n");
}
