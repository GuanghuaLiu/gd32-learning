#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "gd32f30x.h"
#include "delay.h"

static uint8_t g_humiData;
static uint16_t g_adcVal;
#define HUMI_LEVEL_NUM 18
static const uint16_t g_rhAdcTable[][HUMI_LEVEL_NUM] =
{
	// 10%RH	15%RH	20%RH	25%RH	30%RH	35%RH	40%RH	45%RH	50%RH	55%RH	60%RH	65%RH	70%RH	75%RH	80%RH	85%RH	90%RH	95%RH
	/*  0℃ */ {155, 157, 159, 165, 177, 204, 257, 354, 516, 803, 1188, 1633, 2100, 2579, 2919, 3204, 3434, 3605},
	/*  5℃ */ {156, 157, 161, 168, 186, 223, 296, 429, 650, 985, 1407, 1874, 2329, 2795, 3112, 3355, 3570, 3707},
	/* 10℃ */ {156, 159, 164, 175, 199, 250, 341, 508, 775, 1153, 1633, 2126, 2540, 2945, 3235, 3456, 3618, 3742},
	/* 15℃ */ {157, 161, 168, 183, 217, 284, 410, 619, 941, 1762, 1835, 2329, 2749, 3112, 3369, 3554, 3676, 3785},
	/* 20℃ */ {159, 165, 176, 197, 236, 320, 477, 750, 1121, 1604, 2075, 2540, 2919, 3235, 3441, 3610, 3735, 3828},
	/* 25℃ */ {161, 168, 183, 210, 264, 375, 579, 901, 1337, 1854, 2298, 2726, 3054, 3342, 3539, 3676, 3779, 3859},
	/* 30℃ */ {163, 173, 191, 230, 300, 451, 696, 1061, 1536, 2075, 2502, 2893, 3204, 3426, 3594, 3718, 3806, 3878},
	/* 35℃ */ {166, 178, 202, 250, 344, 508, 803, 1226, 1727, 2237, 2704, 3054, 3301, 3500, 3651, 3753, 3833, 3898},
	/* 40℃ */ {169, 184, 214, 284, 397, 619, 985, 1429, 1936, 2430, 2819, 3142, 3369, 3546, 3684, 3779, 3852, 3911},
	/* 45℃ */ {175, 196, 236, 315, 477, 750, 1153, 1604, 2100, 2540, 2945, 3235, 3441, 3602, 3727, 3815, 3870, 3924},
	/* 50℃ */ {179, 204, 253, 354, 545, 865, 1328, 1835, 2298, 2726, 3054, 3328, 3516, 3659, 3761, 3842, 3894, 3940},
	/* 55℃ */ {186, 217, 278, 401, 634, 941, 1523, 2026, 2502, 2843, 3173, 3398, 3562, 3692, 3788, 3861, 3908, 3950},
	/* 60℃ */ {191, 229, 300, 457, 738, 1034, 1711, 2208, 2661, 2998, 3274, 3456, 3610, 3727, 3806, 3870, 3918, 3957},
};

#define TEMP_INTERVAL_VAL 5
#define HUMI_INTERVAL_VAL 5
#define HUMI_VAL_MIN 10
#define HUMI_VAL_MAX 95
#define TEMP_VAL_MAX 60

// typedef struct
// {
//	rcu_periph_enum rcuPeriphGpio;
//	uint32_t periphGpio;
//	uint16_t periphPin;
//	rcu_periph_enum rcuPowerGpio;
//	uint32_t powerGpio;
//	uint16_t powerPin;
// };

static void GpioInit(void)
{
	rcu_periph_clock_enable(RCU_GPIOC);
	gpio_init(GPIOC, GPIO_MODE_AIN, GPIO_OSPEED_10MHZ, GPIO_PIN_4);

	rcu_periph_clock_enable(RCU_GPIOB);
	gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_0);
	gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_1);

	gpio_bit_write(GPIOB, GPIO_PIN_0, SET);
	gpio_bit_write(GPIOB, GPIO_PIN_1, RESET);
}
static void AdcInit(void)
{
	/* 使能ADC时钟 */
	rcu_periph_clock_enable(RCU_ADC1);
	rcu_adc_clock_config(RCU_CKADC_CKAPB2_DIV6);
	/* 1. 确保ADC_CTL0寄存器的DISRC和SM位以及ADC_CTL1寄存器的CTN位为0；*/
	adc_deinit(ADC1);
	/* 2. 用模拟通道编号来配置RSQ0，配置 分辨率、独立模式、连续模式、数据对齐方式、通道个数；*/
	adc_resolution_config(ADC1, ADC_RESOLUTION_12B);
	adc_mode_config(ADC_MODE_FREE);
	adc_special_function_config(ADC1, ADC_CONTINUOUS_MODE, DISABLE);
	adc_data_alignment_config(ADC1, ADC_DATAALIGN_RIGHT);
	adc_channel_length_config(ADC1, ADC_REGULAR_CHANNEL, 1);
	/* 3. 配置ADC_SAMPTx寄存器；*/
	adc_regular_channel_config(ADC1, 0, ADC_CHANNEL_14, ADC_SAMPLETIME_71POINT5);
	/* 4. 如果有需要，可以配置ADC_CTL1寄存器的ETERC和ETSRC位；*/
	adc_external_trigger_source_config(ADC1, ADC_REGULAR_CHANNEL, ADC0_1_2_EXTTRIG_REGULAR_NONE);
	adc_external_trigger_config(ADC1, ADC_REGULAR_CHANNEL, ENABLE);
	/* ADC硬件滤波 */
//	adc_oversample_mode_config(ADC1, ADC_OVERSAMPLING_ALL_CONVERT, ADC_OVERSAMPLING_SHIFT_4B, ADC_OVERSAMPLING_RATIO_MUL16);
//	adc_oversample_mode_enable(ADC1);
	/* 时间校准 */
	/* 1. 确保ADCON=1；*/
	adc_enable(ADC1);
	/* 2. 延迟14个CK_ADC以等待ADC稳定；*/
	DelayNus(20);
	/* 3. 设置RSTCLB (可选的)；*/
	/* 4. 设置CLB=1；*/
	/* 5. 等待直到CLB=0。*/
	adc_calibration_enable(ADC1);
}
static void TimerInit(uint32_t periodUs)
{
	/* 使能定时器的时钟 */
	rcu_periph_clock_enable(RCU_TIMER4);
	/* 复位定时器 */
	timer_deinit(TIMER4);

	/* 初始化定时器参数, 设置自动重装值, 分频系数, 计数方式等 */
	timer_parameter_struct paramInitStruct;
	timer_struct_para_init(&paramInitStruct);
	paramInitStruct.prescaler = 120 - 1; // 时钟频率为120MHz，经过分频后频率为1MHz，即1μs
	paramInitStruct.alignedmode = TIMER_COUNTER_EDGE;
	paramInitStruct.counterdirection = TIMER_COUNTER_UP;
	paramInitStruct.period = periodUs - 1;
	timer_init(TIMER4, &paramInitStruct);

	/* 使能定时器更新中断 */
	timer_interrupt_enable(TIMER4, TIMER_INT_UP);
	/* 使能定时器中断和优先级 */
	nvic_irq_enable(TIMER4_IRQn, 0, 0);
	/* 使能定时器 */
	timer_enable(TIMER4);
}

void HumiDrvInit(void)
{
	GpioInit();
	AdcInit();
	TimerInit(1000);
}

void ExchgeAcPower(void)
{
	gpio_bit_write(GPIOB, GPIO_PIN_0, (FlagStatus)!gpio_output_bit_get(GPIOB, GPIO_PIN_0));
	gpio_bit_write(GPIOB, GPIO_PIN_1, (FlagStatus)!gpio_output_bit_get(GPIOB, GPIO_PIN_1));
}

/**
    @brief 获取湿度传感器ADC值
    @param
    @return 湿度传感器ADC数值
*/
static uint16_t GetAdcVal(void)
{
	/* 6. 等到EOC置1；*/
	while(!adc_flag_get(ADC1, ADC_FLAG_EOC))
		;
	/* 7. 从ADC_RDATA寄存器中读ADC转换结果；*/
	uint16_t adcVal = adc_regular_data_read(ADC1);
	/* 8. 写0清除EOC标志位。*/
	adc_flag_clear(ADC1, ADC_FLAG_EOC);
	return adcVal;
}

void TIMER4_IRQHandler(void)
{
	static uint16_t s_timingNum = 0;
	if(timer_interrupt_flag_get(TIMER4, TIMER_INT_FLAG_UP))
	{
		timer_interrupt_flag_clear(TIMER4, TIMER_INT_FLAG_UP);

		s_timingNum++;
//	if (s_timingNum == 1)
//	{
//	/* 5. 设置SWRCST位，或者为常规序列产生一个外部触发信号；*/
//	adc_software_trigger_enable(ADC1, ADC_REGULAR_CHANNEL);
//	g_adcVal = GetAdcVal();
//	}
//	if (s_timingNum == 2)
//	{
//	ExchgeAcPower();
//	}
//	if (s_timingNum == 4)
//	{
//	ExchgeAcPower();
//	s_timingNum = 0;
//	}
		if(s_timingNum % 2 == 0)
		{
			ExchgeAcPower();
		}
		if(s_timingNum % 400 == 1)
		{
			adc_software_trigger_enable(ADC1, ADC_REGULAR_CHANNEL);
			g_adcVal = GetAdcVal();
		}
	}
}

static void FindAdcBufByTemp(uint16_t *arr, uint8_t temp)
{
	uint8_t tempRow = temp / TEMP_INTERVAL_VAL;
	if(!(temp % TEMP_INTERVAL_VAL))
	{
		memcpy(arr, &g_rhAdcTable[tempRow][0], HUMI_LEVEL_NUM * sizeof(uint16_t));
		return;
	}

	for(uint8_t i = 0; i < HUMI_LEVEL_NUM; i++)
	{
		arr[i] = g_rhAdcTable[tempRow][i] + (g_rhAdcTable[tempRow + 1][i] - g_rhAdcTable[tempRow][i]) * (temp % TEMP_INTERVAL_VAL) / TEMP_INTERVAL_VAL;
	}
}

/**
    @brief 递增式二分查找接近值
    @param arr 目标数组
    @param len 数组元素个数
    @param key 目标值
    @return 元素下标
*/
static int8_t AscBinaryNear(uint16_t *arr, uint8_t len, uint32_t key)
{
	int8_t left = 0;
	int8_t right = len - 1;
	int8_t middle = 0;
	int8_t index = 0;
	while(left <= right)
	{
		middle = left + (right - left) / 2;
		if(arr[middle] < key)
		{
			left = middle + 1;
		}
		else
		{
			right = middle - 1;
			index = middle;
		}
	}
	return index;
}

static uint8_t Adc2Humi(uint16_t *arr, uint16_t adcVal)
{
	int8_t index = AscBinaryNear(arr, HUMI_LEVEL_NUM, adcVal);

	if(index == 0)
	{
		return HUMI_VAL_MIN;
	}

	uint8_t humi = HUMI_INTERVAL_VAL * (index - 1) + HUMI_VAL_MIN +
	    HUMI_INTERVAL_VAL * (adcVal - arr[index - 1]) / (arr[index] - arr[index - 1]);

	return humi;
}

/**
    @brief 触发驱动转换湿度传感器数据
    @param
*/
void HumiSensorProc(uint8_t temp)
{
	if(temp > TEMP_VAL_MAX)
	{
		temp = TEMP_VAL_MAX;
	}

	uint16_t humiAdcBuf[HUMI_LEVEL_NUM] = {0};
	FindAdcBufByTemp(humiAdcBuf, temp);

	g_humiData = Adc2Humi(humiAdcBuf, g_adcVal);
}

/**
    @brief 获取湿度数据
    @param
    @return 湿度数据，正整数
*/
uint8_t GetHumiData(void)
{
	return g_humiData;
}


