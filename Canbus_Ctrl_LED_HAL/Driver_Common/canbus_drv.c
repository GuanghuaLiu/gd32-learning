#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "gd32f30x.h"

#define USE_FILTER_BY_LIST 0

static void (*g_pDataProc)(uint8_t *data, uint8_t dataLength);

void RegDataProcCb(void (*pFun)(uint8_t *data, uint8_t dataLength))
{
	g_pDataProc = pFun;
}

static void GpioConfig(void)
{
	rcu_periph_clock_enable(RCU_GPIOA);
	gpio_init(GPIOA, GPIO_MODE_IPU, GPIO_OSPEED_50MHZ, GPIO_PIN_11);
	gpio_init(GPIOA, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12);
}

static void ParamConfig(void)
{
	rcu_periph_clock_enable(RCU_CAN0);

	/* initialize CAN register */
	can_deinit(CAN0);

	/* initialize CAN */
	can_parameter_struct paramInitStruct;
	can_struct_para_init(CAN_INIT_STRUCT, &paramInitStruct);
	paramInitStruct.working_mode = CAN_NORMAL_MODE;
	paramInitStruct.resync_jump_width = CAN_BT_SJW_1TQ;
	paramInitStruct.time_segment_1 = CAN_BT_BS1_7TQ;
	paramInitStruct.time_segment_2 = CAN_BT_BS2_2TQ;
	paramInitStruct.time_triggered = DISABLE;
	paramInitStruct.auto_bus_off_recovery = ENABLE;
	paramInitStruct.auto_wake_up = DISABLE;
	paramInitStruct.auto_retrans = ENABLE;
	paramInitStruct.rec_fifo_overwrite = DISABLE;
	paramInitStruct.trans_fifo_order = DISABLE;
	/* baudrate 1Mbps */
	paramInitStruct.prescaler = 6;
	can_init(CAN0, &paramInitStruct);

	/* initialize filter */
	can_filter_parameter_struct filterInitStruct;
	can_struct_para_init(CAN_FILTER_STRUCT, &filterInitStruct);

#if USE_FILTER_BY_LIST
	filterInitStruct.filter_mask_low = 0X5A5 << 5;
	filterInitStruct.filter_list_low = 0X5A6 << 5;
	filterInitStruct.filter_mask_high = 0X5A7 << 5;
	filterInitStruct.filter_list_high = 0X5A8 << 5;

	filterInitStruct.filter_fifo_number = CAN_FIFO0;
	filterInitStruct.filter_number = 0;
	filterInitStruct.filter_mode = CAN_FILTERMODE_LIST;
	filterInitStruct.filter_bits = CAN_FILTERBITS_16BIT;
	filterInitStruct.filter_enable = ENABLE;

#else
	filterInitStruct.filter_mask_low = 0X7FE << 5;
	filterInitStruct.filter_list_low = 0X5A5 << 5;
	filterInitStruct.filter_mask_high = 0X7FE << 5;
	filterInitStruct.filter_list_high = 0X5A5 << 5;

	filterInitStruct.filter_fifo_number = CAN_FIFO0;
	filterInitStruct.filter_number = 0;
	filterInitStruct.filter_mode = CAN_FILTERMODE_MASK;
	filterInitStruct.filter_bits = CAN_FILTERBITS_16BIT;
	filterInitStruct.filter_enable = ENABLE;

#endif

	can_filter_init(&filterInitStruct);

	/* enable CAN receive FIFO0 not empty interrupt */
	can_interrupt_enable(CAN0, CAN_INT_RFNE0);
	/* configure the nested vectored interrupt controller */
	nvic_irq_enable(USBD_LP_CAN0_RX0_IRQn, 0, 0);
}

void CanbusDrvInit(void)
{
	GpioConfig();
	ParamConfig();
}

static uint8_t g_transmitBuffer[8] = {0, 1, 2, 3, 4, 5, 6, 7};
static uint8_t g_recBuffer[8];
static volatile bool g_recFlag = false;

void USBD_LP_CAN0_RX0_IRQHandler(void)
{
	can_receive_message_struct recMessage;
	can_struct_para_init(CAN_RX_MESSAGE_STRUCT, &recMessage);

	can_message_receive(CAN0, CAN_FIFO0, &recMessage);

	memset(g_recBuffer, 0, sizeof(g_recBuffer));

	memcpy(g_recBuffer, recMessage.rx_data, recMessage.rx_dlen);

	g_pDataProc(recMessage.rx_data, recMessage.rx_dlen);
	g_recFlag = true;
}

/**
    @brief canbus发送一帧数据
    @param canbusId canbus设备id
    @param dataLength 数据包长度
    @param transmitBuffer 数据包地址
    @return
*/
bool CanbusSendMessage(uint32_t canbusId, uint8_t *transmitBuffer, uint8_t dataLength)
{
	if((dataLength > 8) || (transmitBuffer == NULL))
	{
		return false;
	}

	uint8_t txMail = 0;
	uint16_t timeout = 0xffff;
	can_trasnmit_message_struct transmitMessage;
	can_struct_para_init(CAN_TX_MESSAGE_STRUCT, &transmitMessage);
	transmitMessage.tx_sfid = canbusId;
	transmitMessage.tx_ff = CAN_FF_STANDARD;
	transmitMessage.tx_ft = CAN_FT_DATA;
	transmitMessage.tx_dlen = dataLength;

	memcpy(transmitMessage.tx_data, transmitBuffer, dataLength);

	txMail = can_message_transmit(CAN0, &transmitMessage);

	while((can_transmit_states(CAN0, txMail) != CAN_TRANSMIT_OK) && (timeout != 0))
	{
		timeout--;
	}
	if(0 == timeout)
	{
		return false;
	}
	return true;
}

#define CAN_TRANSMIT_NUM (uint8_t)(8)

void CanbusDrvTest(void)
{
	printf("***************************************\n");
	printf("***********Canbus测试开始**************\n");
	printf("***************************************\n");

	if(!CanbusSendMessage(0x1, g_transmitBuffer, CAN_TRANSMIT_NUM))
	{
		printf("发送数据测试失败\n");
		return;
	}

	while(!g_recFlag)
		;
	g_recFlag = false;

	for(uint8_t i = 0; i < CAN_TRANSMIT_NUM; i++)
	{
		if(g_recBuffer[i] != g_transmitBuffer[i])
		{
			printf("0x%02x ", g_recBuffer[i]);
			printf("\n接收数据测试失败\n");
			return;
		}
		printf("0x%02x ", g_recBuffer[i]);
	}
	printf("\ncanbus测试成功\n");

	printf("***********************\n");
	printf("*****canbus测试结束****\n");
	printf("***********************\n");
}
