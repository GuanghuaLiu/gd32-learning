#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include "gd32f30x.h"

#define SET_SPI_NSS() gpio_bit_set(GPIOE, GPIO_PIN_2)
#define CLR_SPI_NSS() gpio_bit_reset(GPIOE, GPIO_PIN_2)

#define SW_MODE (1)
#define HW_MODE (0)

#if SW_MODE
#define CLR_SPI_SCK() gpio_bit_reset(GPIOB, GPIO_PIN_13)
#define SET_SPI_SCK() gpio_bit_set(GPIOB, GPIO_PIN_13)

#define SET_SPI_MOSI() gpio_bit_set(GPIOB, GPIO_PIN_15)
#define CLR_SPI_MOSI() gpio_bit_reset(GPIOB, GPIO_PIN_15)

#define READ_SPI_MISO() gpio_input_bit_get(GPIOB, GPIO_PIN_14)
#endif

static void GpioInit(void)
{
	rcu_periph_clock_enable(RCU_GPIOE);
	gpio_init(GPIOE, GPIO_MODE_OUT_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_2);

#if SW_MODE
	rcu_periph_clock_enable(RCU_GPIOB);
	gpio_init(GPIOB, GPIO_MODE_OUT_PP, GPIO_OSPEED_10MHZ, GPIO_PIN_13 | GPIO_PIN_15);
	gpio_init(GPIOB, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, GPIO_PIN_14);
#elif HW_MODE
	rcu_periph_clock_enable(RCU_GPIOB);
	gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_13 | GPIO_PIN_15);
	gpio_init(GPIOB, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, GPIO_PIN_14);
#endif
}

#if HW_MODE
static void SPIHwInit(void)
{
	rcu_periph_clock_enable(RCU_SPI1);
	spi_i2s_deinit(SPI1);

	spi_parameter_struct spiInitStruct;
	spi_struct_para_init(&spiInitStruct);
	spiInitStruct.device_mode = SPI_MASTER;
	spiInitStruct.trans_mode = SPI_TRANSMODE_FULLDUPLEX;
	spiInitStruct.frame_size = SPI_FRAMESIZE_8BIT;
	spiInitStruct.nss = SPI_NSS_SOFT;
	spiInitStruct.endian = SPI_ENDIAN_MSB;
	spiInitStruct.clock_polarity_phase = SPI_CK_PL_LOW_PH_1EDGE;
	spiInitStruct.prescale = SPI_PSC_2;
	spi_init(SPI1, &spiInitStruct);
	spi_enable(SPI1);
}
#endif

static void SPIInit(void)
{
	GpioInit();
#if HW_MODE
	SPIHwInit();
#endif

	SET_SPI_NSS();
#if SW_MODE
	CLR_SPI_SCK();
#endif
}

static void SPIStart(void)
{
	CLR_SPI_NSS();
}

static void SPIStop(void)
{
	SET_SPI_NSS();
}

static uint8_t SPIReadWriteByte(uint8_t sendByte)
{
	uint8_t receiveByte;
#if SW_MODE
	for(uint8_t i = 0; i < 8; i++)
	{
		if(sendByte & 0x80)
		{
			SET_SPI_MOSI();
		}
		else
		{
			CLR_SPI_MOSI();
		}
		sendByte <<= 1;

		SET_SPI_SCK();
		receiveByte <<= 1;
		receiveByte |= READ_SPI_MISO();
		CLR_SPI_SCK();
	}
#elif HW_MODE
	while(!spi_i2s_flag_get(SPI1, SPI_FLAG_TBE));
	spi_i2s_data_transmit(SPI1, sendByte);
	while(!spi_i2s_flag_get(SPI1, SPI_FLAG_RBNE))
	{
		static uint32_t timeout = 100000;
		timeout--;
		if(!timeout)
		{
			timeout = 100000;
			break;
		}

	}
	receiveByte = (uint8_t)spi_i2s_data_receive(SPI1);

#endif
	return receiveByte;
}

/* Erase, Program Instructions */
#define NORFLASH_WRITE_ENABLE 0X06
#define NORFLASH_WRITE_ENABLE_FOR_VOLATILE_STATUS_REGISTER 0X50
#define NORFLASH_WRITE_DISABLE 0X04
#define NORFLASH_READ_STATUS_REGISTER_1 0X05
#define NORFLASH_READ_STATUS_REGISTER_2 0X35
#define NORFLASH_WRITE_STATUS_REGISTER 0X01
#define NORFLASH_PAGE_PROGRAM 0X02
#define NORFLASH_SECTOR_ERASE_4KB 0X20
#define NORFLASH_BLOCK_ERASE_32KB 0X52
#define NORFLASH_BLOCK_ERASE_64KB 0XD8
#define NORFLASH_CHIP_ERASE 0X60
#define NORFLASH_ERASE_PROGRAM_SUSPEND 0X75
#define NORFLASH_ERASE_PROGRAM_RESUME 0X7A
#define NORFLASH_POWER_DOWN 0XB9
#define NORFLASH_CONTINUOUS_READ_MODE_RESET 0XFF

/* Read Instructions */
#define NORFLASH_READ_DATA 0X03
#define NORFLASH_FAST_READ 0X0B
#define NORFLASH_SET_BURST_WITH_WRAP 0X77

/* ID, Security Instructions */
#define NORFLASH_RELEASE_POWER_DOWN_DEVICE_ID 0XAB
#define NORFLASH_MANUFACTURER_DEVICE_ID 0X90
#define NORFLASH_JEDEC_ID 0X9F
#define NORFLASH_READ_UNIQUE_ID 0X4B
#define NORFLASH_READ_SFDP_REGISTER 0X5A
#define NORFLASH_ERASE_SECURITY_REGISTERS 0X44
#define NORFLASH_PROGRAM_SECURITY_REGISTERS 0X42
#define NORFLASH_READ_SECURITY_REGISTERS 0X48

/* Sizes of norflash */
#define NORFLASH_PAGE_SIZE 256
#define NORFLASH_SECTOR_SIZE 4096
#define NORFLASH_BLOCK_SIZE (16 * (NORFLASH_SECTOR_SIZE))
#define NORFLASH_SIZE 256 * (NORFLASH_BLOCK_SIZE)

#define DUMMY_BYTE 0XFF

void NorflashDrvInit(void)
{
	SPIInit();
}

/**
    @brief 获取Norflash的ID
    @param mId 生产商ID
    @param dId 设备ID
*/
void ReadNorflashID(uint8_t *mId, uint16_t *dId)
{
	SPIStart();
	SPIReadWriteByte(NORFLASH_JEDEC_ID);
	*mId = SPIReadWriteByte(DUMMY_BYTE);
	*dId = SPIReadWriteByte(DUMMY_BYTE);
	*dId <<= 8;
	*dId |= SPIReadWriteByte(DUMMY_BYTE);
	SPIStop();
}

/**
    @brief 指定地址开始读出指定个数的数据
    @param readAdder 读取数据的地址
    @param len 读取数据的个数
    @param pBuffer 指定数据保存的地址
*/
void ReadNorflashData(uint32_t readAdder, uint32_t len, uint8_t *pBuffer)
{
	SPIStart();
	SPIReadWriteByte(NORFLASH_READ_DATA);
	SPIReadWriteByte((readAdder & 0XFF0000) >> 16);
	SPIReadWriteByte((readAdder & 0X00FF00) >> 8);
	SPIReadWriteByte((readAdder & 0X0000FF) >> 0);

	for(uint32_t i = 0; i < len; i++)
	{
		pBuffer[i] = SPIReadWriteByte(DUMMY_BYTE);
	}

	SPIStop();
}

/**
    使能Norflash写操作, 在每次执行页编程 (PP)、四页编程 (QPP)、扇区擦除 (SE)、块擦除 (BE)、
    片擦除 (CE)、写状态寄存器 (WRSR) 和擦除/编程安全寄存器命令之前，必须设置写使能锁存器 (WEL) 位。
*/
static void EnableNorflashWrite(void)
{
	SPIStart();
	SPIReadWriteByte(NORFLASH_WRITE_ENABLE);
	SPIStop();
}

/**
    等待Norflash写操作完成, 当写操作位BUSY = 1时，需要判断是否超时
    超时需要强制推出判断等待死循环
*/
static void WaitNorflashWriteEnd(void)
{
	uint32_t timeOut = 100000;
	SPIStart();
	SPIReadWriteByte(NORFLASH_READ_STATUS_REGISTER_1);

	while(SPIReadWriteByte(DUMMY_BYTE) & 0x01)
	{
		timeOut--;
		if(!timeOut)
		{
			break;
		}
	}
	SPIStop();
}

/**
    @brief 按页对Norflash编程
    @param writeAdder 写入地址
    @param len 要写入的数据个数
    @param pBuffer 数组首地址
*/
static void ProgramNorflashDataPage(uint32_t writeAdder, uint32_t len, uint8_t *pBuffer)
{
	EnableNorflashWrite();
	SPIStart();
	SPIReadWriteByte(NORFLASH_PAGE_PROGRAM);
	SPIReadWriteByte((writeAdder & 0XFF0000) >> 16);
	SPIReadWriteByte((writeAdder & 0X00FF00) >> 8);
	SPIReadWriteByte((writeAdder & 0X0000FF) >> 0);

	for(uint32_t i = 0; i < len; i++)
	{
		SPIReadWriteByte(*pBuffer++);
	}
	SPIStop();
	WaitNorflashWriteEnd();
}

/**
    @brief 指定地址开始写入指定个数的数据，调用前需要擦除flash
    @param writeAddr 写入地址
    @param pBuffer 数组首地址
    @param len 要写入的数据个数
*/
void WriteNorflashData(uint32_t writeAddr, uint32_t len, uint8_t *pBuffer)
{
	uint32_t pageNum = 0;
	uint8_t addrOffset = writeAddr % NORFLASH_PAGE_SIZE;
	/* 判断是否跨页 */
	if(NORFLASH_PAGE_SIZE - addrOffset < len)
	{
		/*1. 补完首页 */
		ProgramNorflashDataPage(writeAddr, NORFLASH_PAGE_SIZE - addrOffset, pBuffer);
		pBuffer += NORFLASH_PAGE_SIZE - addrOffset;
		writeAddr += NORFLASH_PAGE_SIZE - addrOffset;
		len -= NORFLASH_PAGE_SIZE - addrOffset;
		pageNum = len / NORFLASH_PAGE_SIZE;
		/*2. 写剩下的全页*/
		while(pageNum--)
		{
			ProgramNorflashDataPage(writeAddr, NORFLASH_PAGE_SIZE, pBuffer);
			writeAddr += NORFLASH_PAGE_SIZE;
			pBuffer += NORFLASH_PAGE_SIZE;
			len -= NORFLASH_PAGE_SIZE;
		}
		/*3. 写剩下不足一页的数据*/
		ProgramNorflashDataPage(writeAddr, len, pBuffer);
	}
	else
	{
		ProgramNorflashDataPage(writeAddr, len, pBuffer);
	}
}

/**
    @brief 擦除扇区
    @param eraseAddr 所需擦除扇区的地址
*/
static void EraseNorflashSector(uint32_t eraseAddr)
{
	EnableNorflashWrite();
	SPIStart();
	SPIReadWriteByte(NORFLASH_SECTOR_ERASE_4KB);
	SPIReadWriteByte((eraseAddr & 0XFF0000) >> 16);
	SPIReadWriteByte((eraseAddr & 0X00FF00) >> 8);
	SPIReadWriteByte((eraseAddr & 0X0000FF) >> 0);
	SPIStop();
	WaitNorflashWriteEnd();
}

/**
    @brief 擦除从eraseAddr开始到eraseAddr + len所对应的扇区
    @param eraseAddr 所需擦除扇区的地址
    @param len 数据的个数
*/
void EraseNorflashSectorForWrite(uint32_t eraseAddr, uint32_t len)
{
	uint8_t sectorNum = 0;
	uint32_t addrOffset = eraseAddr % NORFLASH_SECTOR_SIZE;

	if(len > NORFLASH_SECTOR_SIZE - addrOffset)
	{
		EraseNorflashSector(eraseAddr);
		eraseAddr += NORFLASH_SECTOR_SIZE - addrOffset;
		len -= NORFLASH_SECTOR_SIZE - addrOffset;
		sectorNum = len / NORFLASH_SECTOR_SIZE;

		while(sectorNum--)
		{
			EraseNorflashSector(eraseAddr);
			eraseAddr += NORFLASH_SECTOR_SIZE;
			len -= NORFLASH_SECTOR_SIZE;
		}
		EraseNorflashSector(eraseAddr);
	}
	else
	{
		EraseNorflashSector(eraseAddr);
	}
}

#define BUFFER_SIZE 4321
#define FLASH_ADDRESS 0x000002
void NorflashDrvTest(void)
{
	uint8_t mid = 0;
	uint16_t did = 0;
	uint8_t bufferWrite[BUFFER_SIZE];
	uint8_t bufferRead[BUFFER_SIZE];

	printf("***************************\n");
	printf("********GD25Qxx测试开始********\n");
	printf("***************************\n");

	ReadNorflashID(&mid, &did);
	printf("Norflash MID: %#X, DID: %#X\n", mid, did);

	for(uint16_t i = 0; i < BUFFER_SIZE; i++)
	{
		bufferWrite[i] = i + 1;
		printf("0x%02X ", bufferWrite[i]);
	}
	printf("\n开始写入\n");

	EraseNorflashSectorForWrite(FLASH_ADDRESS, BUFFER_SIZE);
	WriteNorflashData(FLASH_ADDRESS, BUFFER_SIZE, bufferWrite);

	printf("开始读取...\n");
	ReadNorflashData(FLASH_ADDRESS, BUFFER_SIZE, bufferRead);

	for(uint16_t i = 0; i < BUFFER_SIZE; i++)
	{
		if(bufferRead[i] != bufferWrite[i])
		{
			printf("0x%02X ", bufferRead[i]);
			printf("测试故障，请排查！\n");
			return;
		}
		printf("0x%02X ", bufferRead[i]);
	}
	printf("\n测试通过！\n");
}
