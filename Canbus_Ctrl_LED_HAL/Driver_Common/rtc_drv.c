#include <stdint.h>
#include <time.h>
#include "rtc_drv.h"
#include "gd32f30x.h"

#define MASK_CODE 0X5A5A

void RtcDrvInit(void)
{
	if(bkp_read_data(BKP_DATA_0) != MASK_CODE)
	{
		/* 1. RTC复位 */
		/* 使能电源以及备份接口时钟 */
		rcu_periph_clock_enable(RCU_PMU);
		rcu_periph_clock_enable(RCU_BKPI);
		/* 使能对备份域寄存器和RTC的访问。 */
		pmu_backup_write_enable();
		/* 复位备份域寄存器以及RTC寄存器 */
		bkp_deinit();

		/* 2. RTC读取 */
		/* 使能晶振LXTAL，并等待其稳定*/
		rcu_osci_on(RCU_LXTAL);
		rcu_osci_stab_wait(RCU_LXTAL);
		/* 设置RTC时钟源为LXTAL */
		rcu_rtc_clock_config(RCU_RTCSRC_LXTAL);
		/* 使能RTC时钟 */
		rcu_periph_clock_enable(RCU_RTC);
		/* 等待APB1接口时钟与RTC时钟同步 */
		rtc_register_sync_wait();

		/* 3. RTC配置 */
		/* 等待上次对RTC寄存器写操作完成 */
		rtc_lwoff_wait();
		/* 设置分频值 */
		rtc_prescaler_set(32768 - 1);
		/* 等待上次对RTC寄存器写操作完成 */
		rtc_lwoff_wait();

		/*设置北京时间：2024年3月2日11点49分50秒*/
		rtc_counter_set(1713450200);

		bkp_write_data(BKP_DATA_0, MASK_CODE);
		return;
	}

	/* 1. RTC复位 */
	/* 使能电源以及备份接口时钟 */
	rcu_periph_clock_enable(RCU_PMU);
	rcu_periph_clock_enable(RCU_BKPI);
	/* 使能对备份域寄存器和RTC的访问。 */
	pmu_backup_write_enable();

	/* 2. RTC读取 */
	/* 等待APB1接口时钟与RTC时钟同步 */
	rtc_register_sync_wait();

	/* 3. RTC配置 */
	/* 等待上次对RTC寄存器写操作完成 */
	rtc_lwoff_wait();
}

/**
    @brief 设置RTC模块时间
    @param time 时间结构体
*/
void SetRtcTime(RtcTime_t *time)
{
	time_t timeStamp = 0;
	struct tm tmInfo = {0};
	tmInfo.tm_year = time->year - 1900;
	tmInfo.tm_mon = time->month - 1;
	tmInfo.tm_mday = time->day;
	tmInfo.tm_hour = time->hour - 8;
	tmInfo.tm_min = time->minute;
	tmInfo.tm_sec = time->second;
	timeStamp = mktime(&tmInfo);

	rtc_lwoff_wait();
	rtc_counter_set(timeStamp);
}

/**
    @brief 获取RTC模块时间
    @param time 时间结构体
*/
void GetRtcTime(RtcTime_t *time)
{
	uint32_t timeStamp = rtc_counter_get() + 8 * 60 * 60;
	struct tm *tmInfo = localtime(&timeStamp);

	time->year = tmInfo->tm_year + 1900;
	time->month = tmInfo->tm_mon + 1;
	time->day = tmInfo->tm_mday;
	time->hour = tmInfo->tm_hour;
	time->minute = tmInfo->tm_min;
	time->second = tmInfo->tm_sec;
}
