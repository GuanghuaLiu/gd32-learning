#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "gd32f30x.h"

#define BANK0_REGION3_ADDR      ((uint32_t)(0x6C000000))
#define EX_SRAM_BUFFER_SIZE     ((uint32_t)(1 * 1024 * 1024))
#define EX_SRAM_BEGIN_ADDR      (BANK0_REGION3_ADDR)
#define EX_SRAM_END_ADDR        (SRAM_START_ADDR + SRAM_BUFFER_SIZE - 1)

#define EXMC_TYPE_PSRAM         (1)
#define EXMC_TYPE_NORFLASH      (0)
#define EXMC_TYPE_NANDFLASH     (0)

static void ExmcSramGpioInit()
{
	rcu_periph_clock_enable(RCU_GPIOB);
	rcu_periph_clock_enable(RCU_GPIOD);
	rcu_periph_clock_enable(RCU_GPIOE);
	rcu_periph_clock_enable(RCU_GPIOF);
	rcu_periph_clock_enable(RCU_GPIOG);

	/* 通用地址引脚EXMC_A[25:0] */
	gpio_init(GPIOF, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5); // 依次为A0 ~ A5
	gpio_init(GPIOF, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15); // 依次为A6 ~ A9
	gpio_init(GPIOG, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5); // 依次为A10 ~ A15
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13); // 依次为A16 ~ A18
	// gpio_init(GPIOE, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_2);              // 依次为A19 ~ A23
	// gpio_init(GPIOG, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_13 | GPIO_PIN_14);                                                   // 依次为A24 ~ A25

	/* 通用数据引脚EXMC_D[25:0] */
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_14 | GPIO_PIN_15 | GPIO_PIN_0 | GPIO_PIN_1); // 依次为D0 ~ D3
	gpio_init(GPIOE, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15); // 依次为D4 ~ D12
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10); // 依次为D13 ~ D15

	/* NOR/PSRAM引脚 */
#if EXMC_TYPE_PSRAM || EXMC_TYPE_NORFLASH
	// gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_3);                             // EXMC_CLK引脚
	// gpio_init(GPIOB, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7);                             // EXMC_NADV
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_7); // EXMC_NE0
	gpio_init(GPIOG, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_12); // EXMC_NE1 ~ NE3
#endif

	/* PSRAM引脚 */
#if EXMC_TYPE_PSRAM
	gpio_init(GPIOE, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_0 | GPIO_PIN_1); // EXMC_NBL0 ~ NBL1
#endif

	/* NOR/PSRAM/NAND引脚 */
#if EXMC_TYPE_PSRAM || EXMC_TYPE_NORFLASH || EXMC_TYPE_NANDFLASH
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_4); // EXMC_NOE
	gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_5); // EXMC_NWE
	// gpio_init(GPIOD, GPIO_MODE_AF_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_6); // EXMC_NWAIT
#endif
}

static void ExmcSramExmcInit()
{
	rcu_periph_clock_enable(RCU_EXMC);
	exmc_norsram_deinit(EXMC_BANK0_NORSRAM_REGION3);

	exmc_norsram_timing_parameter_struct sramTimingInitStruct;
	sramTimingInitStruct.asyn_access_mode = EXMC_ACCESS_MODE_A;
	sramTimingInitStruct.asyn_address_holdtime = 0;
	sramTimingInitStruct.asyn_address_setuptime = 0;
	sramTimingInitStruct.asyn_data_setuptime = 4;
	sramTimingInitStruct.bus_latency = 0;
	sramTimingInitStruct.syn_clk_division = EXMC_SYN_CLOCK_RATIO_DISABLE;
	sramTimingInitStruct.syn_data_latency = 2;

	exmc_norsram_parameter_struct sramInitStruct;
	sramInitStruct.norsram_region = EXMC_BANK0_NORSRAM_REGION3;
	sramInitStruct.write_mode = EXMC_ASYN_WRITE;
	sramInitStruct.extended_mode = DISABLE;
	sramInitStruct.asyn_wait = DISABLE;
	sramInitStruct.nwait_signal = DISABLE;
	sramInitStruct.memory_write = ENABLE;
	sramInitStruct.nwait_config = EXMC_NWAIT_CONFIG_BEFORE;
	sramInitStruct.wrap_burst_mode = DISABLE;
	sramInitStruct.nwait_polarity = EXMC_NWAIT_POLARITY_LOW;
	sramInitStruct.burst_mode = DISABLE;
	sramInitStruct.databus_width = EXMC_NOR_DATABUS_WIDTH_16B;
	sramInitStruct.memory_type = EXMC_MEMORY_TYPE_SRAM;
	sramInitStruct.address_data_mux = DISABLE;
	sramInitStruct.read_write_timing = &sramTimingInitStruct;
	exmc_norsram_init(&sramInitStruct);

	exmc_norsram_enable(EXMC_BANK0_NORSRAM_REGION3);
}

void ExsramDrvInit(void)
{
	ExmcSramGpioInit();
	ExmcSramExmcInit();
}

#define MAX_BUFFER_SIZE 255
//static uint8_t g_sramBuffer[MAX_BUFFER_SIZE] __attribute__((section(".ARM.__at_0x6C000000")));
//static uint8_t g_sramBuffer[MAX_BUFFER_SIZE] __attribute__((at(EX_SRAM_BEGIN_ADDR)));
static uint8_t g_sramBuffer[MAX_BUFFER_SIZE];

void ExmcSramDrvTest(void)
{
	printf("***********************************\n");
	printf("********外部SRAM测试程序开始********\n");
	printf("***********************************\n");

	printf("*******Exmc Sram is writting*******\n");

	for(uint8_t i = 0; i < MAX_BUFFER_SIZE; i++)
	{
		g_sramBuffer[i] = i;
		printf("0x%02x ", g_sramBuffer[i]);
		if(0 == i % 16)
		{
			printf("\n\r");
		}
	}

	printf("\n*******Exmc Sram is reading*******\n");
	for(uint8_t i = 0; i < MAX_BUFFER_SIZE; i++)
	{
		if(i != g_sramBuffer[i])
		{
			printf("0x%02x ", g_sramBuffer[i]);
			printf("\n*******外部SRAM读错误*******\n");
			return;
		}
		printf("0x%02x ", g_sramBuffer[i]);
		if(0 == i % 16)
		{
			printf("\n\r");
		}
	}
	printf("\n**************测试成功*************\n");
	printf("***********************************\n");
	printf("********外部SRAM测试程序结束********\n");
	printf("***********************************\n");
}
