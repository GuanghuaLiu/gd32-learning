#ifndef _RTC_DRV_H_
#define _RTC_DRV_H_

#include <stdint.h>

typedef struct
{
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
} RtcTime_t;

/**
    @brief 设置RTC模块时间
    @param time 时间结构体
*/
void SetRtcTime(RtcTime_t *time);

/**
    @brief 获取RTC模块时间
    @param time 时间结构体
*/
void GetRtcTime(RtcTime_t *time);

void RtcDrvInit(void);

#endif
