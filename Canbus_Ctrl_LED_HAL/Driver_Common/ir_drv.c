#include <stdint.h>
#include <stdbool.h>
#include "gd32f30x.h"

typedef struct
{
	rcu_periph_enum rcuGpio;
	uint32_t gpio;
	uint32_t gpioPin;

	rcu_periph_enum rcuTimer;
	uint32_t timer;
	uint16_t timerCh;
	uint8_t irq;
} IrHwDrvInfo_t;

static IrHwDrvInfo_t g_irDrvInfo = {RCU_GPIOC, GPIOC, GPIO_PIN_6, RCU_TIMER7, TIMER7, TIMER_CH_0, TIMER7_Channel_IRQn};

static void GpioInit(void)
{
	rcu_periph_clock_enable(g_irDrvInfo.rcuGpio);
	gpio_init(g_irDrvInfo.gpio, GPIO_MODE_IPU, GPIO_OSPEED_10MHZ, g_irDrvInfo.gpioPin);
}
static void TimerInit(void)
{
	rcu_periph_clock_enable(g_irDrvInfo.rcuTimer);
	timer_deinit(g_irDrvInfo.timer);

	timer_parameter_struct timerInitStruct;
	timer_struct_para_init(&timerInitStruct);
	timerInitStruct.prescaler = 120 - 1;
	timer_init(g_irDrvInfo.timer, &timerInitStruct);

	timer_ic_parameter_struct icInitStruct;
	timer_channel_input_struct_para_init(&icInitStruct);
	icInitStruct.icpolarity = TIMER_IC_POLARITY_FALLING;
	timer_input_capture_config(g_irDrvInfo.timer, g_irDrvInfo.timerCh, &icInitStruct);

	timer_interrupt_enable(g_irDrvInfo.timer, TIMER_INT_CH0);
	nvic_irq_enable(g_irDrvInfo.irq, 0, 0);
	timer_enable(g_irDrvInfo.timer);
}

void IrDrvInit(void)
{
	GpioInit();
	TimerInit();
}

#define TICK_HEAD_MAX 20000
#define TICK_HEAD_MIN 10000
#define TICK_0_MAX 1800
#define TICK_0_MIN 900
#define TICK_1_MAX 2700
#define TICK_1_MIN 1800

static uint8_t g_irCode[4];
static bool g_irCodeFlag = false;

/**
***********************************************************
    @brief 解析按键码值
    @param tickNum 捕获计数值，单位us
    @return
***********************************************************
*/
static void ParseIrFrame(uint16_t tickNum)
{
	static bool s_headFlag = false;
	static uint8_t s_index = 0;

	if(tickNum < TICK_HEAD_MAX && tickNum > TICK_HEAD_MIN)
	{
		s_headFlag = true;
		return;
	}

	if(!s_headFlag)
	{
		return;
	}

	if(tickNum < TICK_1_MAX && tickNum > TICK_1_MIN)
	{
		g_irCode[s_index / 8] >>= 1;
		g_irCode[s_index / 8] |= 0x80;
		s_index++;
	}
	if(tickNum < TICK_0_MAX && tickNum > TICK_0_MIN)
	{
		g_irCode[s_index / 8] >>= 1;
		s_index++;
	}
	if(s_index == 32)
	{
		if((g_irCode[2] & g_irCode[3]) == 0)
		{
			g_irCodeFlag = true;
		}
		else
		{
			g_irCodeFlag = false;
		}

		s_headFlag = false;
		s_index = 0;
	}
}

void TIMER7_Channel_IRQHandler(void)
{
	uint16_t icVal = 0;
	if(timer_interrupt_flag_get(g_irDrvInfo.timer, TIMER_INT_FLAG_CH0) == SET)
	{
		timer_interrupt_flag_clear(g_irDrvInfo.timer, TIMER_INT_FLAG_CH0);
		icVal = (uint16_t)timer_channel_capture_value_register_read(g_irDrvInfo.timer, g_irDrvInfo.timerCh);
		timer_counter_value_config(g_irDrvInfo.timer, 0);

		ParseIrFrame(icVal);
	}
}

/**
***********************************************************
    @brief 获取遥控按键码值
    @param code 输出，按键码值
    @return 返回是否成功获取到按键码值
***********************************************************
*/
bool GetIrCode(uint8_t *code)
{
	if(!g_irCodeFlag)
	{
		return false;
	}
	*code = g_irCode[2];
	g_irCodeFlag = false;
	return true;
}
