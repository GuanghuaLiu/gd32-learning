#include <stdint.h>
#include "gd32f30x.h"

/**
    @brief 初始化DWT延时
    @param
*/
void DwtDelayInit(void)
{
	// 关闭DEMCR寄存器的TRCENA位，禁能DWT
	CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk;
	// 打开DEMCR寄存器的TRCENA位，使能DWT
	CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

	// 关闭计数功能
	DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;
	// 打开计数功能
	DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;

	// 计数清零
	DWT->CYCCNT = 0;
}

/**
    @brief 微秒级延时
    @param nUs N微秒，最大延时时间：(2^32/内核主频)*10^6 us
*/
void DelayNus(uint32_t nUs)
{
	uint32_t tickStart = DWT->CYCCNT;

	nUs *= (rcu_clock_freq_get(CK_AHB) / 1000000);

	while((DWT->CYCCNT - tickStart) < nUs);
}

/**
    @brief 毫秒级延时
    @param nMs 延时N毫秒
*/
void DelayNms(uint32_t nMs)
{
	for(uint32_t i = 0; i < nMs; i++)
	{
		DelayNus(1000);
	}

}
