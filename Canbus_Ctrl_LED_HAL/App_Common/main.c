#include <stdint.h>
#include <stdio.h>
#include "systick.h"
#include "delay.h"
#include "usb2com_drv.h"
#include "canbus_drv.h"
#include "led_drv.h"
#include "canbus_app.h"

typedef struct
{
	uint8_t run;
	uint16_t timLoad;
	uint16_t timRload;
	void (*pTaskFunctionCb)(void);
} TaskComponents_t;

static TaskComponents_t g_taskComps[] = {
	{0, 10, 10, CanbusTask},
};

#define MAX_TASK_MUM (sizeof(g_taskComps) / sizeof(g_taskComps[0]))

void TaskHandler(void)
{
	for (uint8_t i = 0; i < MAX_TASK_MUM; i++)
	{
		if (g_taskComps[i].run)
		{
			g_taskComps[i].run = 0;
			g_taskComps[i].pTaskFunctionCb();
		}
	}
}

void TaskScheduleCb(void)
{
	for (uint8_t i = 0; i < MAX_TASK_MUM; i++)
	{
		if (g_taskComps[i].timLoad)
		{
			g_taskComps[i].timLoad--;
			if (g_taskComps[i].timLoad == 0)
			{
				g_taskComps[i].run = 1;
				g_taskComps[i].timLoad = g_taskComps[i].timRload;
			}
			
		}
		
	}
}

static void DriverInit(void)
{
	DwtDelayInit();
	SystickInit();
	Usb2ComDrvInit();
	CanbusDrvInit();
	LedDrvInit();
}

static void AppInit(void)
{
	CanbusAppInit();
	TaskScheduleCbReg(TaskScheduleCb);
}

int main(void)
{
	DriverInit();
	AppInit();

	while (1)
	{
		TaskHandler();
	}
}
