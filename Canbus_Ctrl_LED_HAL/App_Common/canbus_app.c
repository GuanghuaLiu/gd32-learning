#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "canbus_drv.h"
#include "led_drv.h"

/**
***********************************************************************
* 包格式：功能字  LED编号    亮/灭
*         0x06   0x00    0x01/0x00
***********************************************************************
*/
#define FUNC_CODE_INDEX ((uint8_t)0x00)
#define FUNC_CODE       ((uint8_t)0x06)
#define LED_INDEX       ((uint8_t)0x01)

#define MAX_BUFFER_SIZE 8
static uint8_t g_recBuffer[MAX_BUFFER_SIZE];
static volatile bool g_recFlag = false;

static void DataProc(uint8_t *data, uint8_t dataLength)
{
    if (data == NULL || dataLength > 8)
    {
        return;
    }
    memcpy(g_recBuffer, data, dataLength);
    g_recFlag = true;
}

typedef struct
{
    uint8_t ledIndex;
    uint8_t ledStatus;
} LedCtrlInfo_t;

static void CtrlLed(LedCtrlInfo_t *ledInfo)
{
    ledInfo->ledStatus ? TurnOnLed(ledInfo->ledIndex) : TurnOffLed(ledInfo->ledIndex);
}

void CanbusTask(void)
{
    if (!g_recFlag)
    {
        return;
    }
    g_recFlag = false;

    if (g_recBuffer[FUNC_CODE_INDEX] == FUNC_CODE)
    {
        CtrlLed((LedCtrlInfo_t *)&g_recBuffer[LED_INDEX]);
    }
}

void CanbusAppInit(void)
{
    RegDataProcCb(DataProc);
}
